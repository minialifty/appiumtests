﻿using System;
using NUnit.Framework;
using OpenCvSharp;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.iOS;

namespace appiumtests
{
    class Tests
    {
        TestDevice device = TestDevice.iPhone6Plus;
        
        ScreenshotActions screenshotActions;
        Detector detector;
        Touch touchInput;
        Confirmer confirmer;

        /// <summary>
        /// Starts appium session
        /// </summary>
        /// <param name="app">If theViewer session or browser</param>
        /// <param name="reinstallApp">Only for iOS</param>
        public void SetUp(bool app, bool reinstallApp = false)
        {
            Driver.SetUpDriver(device, app, reinstallApp);
            touchInput = Driver.GetTouchInput();
            screenshotActions = Driver.GetScreenshotActions();
            detector = new Detector(screenshotActions);
            //confirmer = new Confirmer(detector, screenshotActions, touchInput, Driver.android);
        }
        
        [Test]
        public void Base()
        {

        }
    }
}
