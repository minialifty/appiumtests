﻿using System;
using NUnit.Framework;
using OpenCvSharp;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.MultiTouch;
using OpenQA.Selenium.Appium.iOS;

namespace appiumtests
{
    static class DriverTouch
    {
        static int screenshotRatio = 1;

        static int errorCount = 0;
        static int consecutiveErrorLimit = 4;
        static int retryWait = 10;

        public static void Tap(Point point, AndroidDriver<AndroidElement> driver)
        {
            try
            {
                TouchAction touchAction = new TouchAction(driver);
                touchAction.Press(point.X, point.Y).Wait(20).Release().Perform();
                Console.WriteLine("[Android] DiverTouch - Performed single tap at point:  " + point.X + "," + point.Y);
                errorCount = 0;
            }
            catch (Exception e)
            {
                Console.WriteLine("[Android] DiverTouch - Performing single tap  failed ( consecutive: " + errorCount + "). Error: " + e.Message);
                if ((driver.CurrentActivity.Equals("co.theconstructutils.viewerplugin.ViewerActivity") || driver.CurrentActivity.Equals(".permission.ui.GrantPermissionsActivity")) && errorCount < consecutiveErrorLimit)
                {
                    System.Threading.Thread.Sleep(retryWait);
                    Tap(point, driver);
                }
                else
                {
                    Assert.Fail("Performing single tap failed!");
                }
                errorCount++;
            }
        }

        public static void Tap(Point point, IOSDriver<IOSElement> idriver)
        {
            try
            {
                TouchAction touchAction = new TouchAction(idriver);
                touchAction.Press(point.X, point.Y).Wait(20).Release().Perform();
                Console.WriteLine("[iOS] DriverTouch - Performed single tap at point: " + point.X + "," + point.Y);
                errorCount = 0;
            }
            catch (Exception e)
            {
                errorCount++;
                Console.WriteLine("[iOS] DriverTouch: Performing single tap failed ( consecutive: " + errorCount + "). Error: " + e.Message);
                if (errorCount < consecutiveErrorLimit)
                {
                    System.Threading.Thread.Sleep(retryWait);
                    Tap(point, idriver);
                }
                else
                {
                    Assert.Fail("Performing single tap failed!");
                }
            }
        }

        public static void DoubleTap(Point point, AndroidDriver<AndroidElement> driver)
        {
            try
            {
                TouchAction touchAction = new TouchAction(driver);
                touchAction.Tap(point.X, point.Y, 2).Perform();
                errorCount = 0;
                Console.WriteLine("[Android] DriverTouch - Performed double tap at point: [" + point.X + ", " + point.Y + "]");
            }
            catch (Exception e)
            {
                Console.WriteLine("[Android] DriverTouch - Performing double tap failed ( consecutive: " + errorCount + "). Error: " + e.Message);
                if (driver.Context.Equals("NATIVE_APP") && driver.CurrentActivity.Equals("co.theconstructutils.viewerplugin.ViewerActivity") && errorCount < consecutiveErrorLimit)
                {
                    System.Threading.Thread.Sleep(retryWait);
                    DoubleTap(point, driver);
                }
                else
                {
                    Assert.Fail("Performing double tap failed!");
                }
                errorCount++;

            }
        }

        public static void DoubleTap(Point point, IOSDriver<IOSElement> idriver)
        {
            try
            {
                TouchAction touchAction = new TouchAction(idriver);
                touchAction.Tap(point.X, point.Y, 2).Perform();
                Console.WriteLine("[iOS] DriverTouch - Performed double tap at point: [" + point.X + ", " + point.Y + "]");
                errorCount = 0;
            }
            catch (Exception e)
            {
                Console.WriteLine("[iOS] DriverTouch - Performing double tap failed ( consecutive: " + errorCount + "). Error: " + e.Message);
                if (errorCount < consecutiveErrorLimit)
                {
                    System.Threading.Thread.Sleep(retryWait);
                    DoubleTap(point, idriver);
                }
                else
                {
                    Assert.Fail("Performing double tap failed!");
                }
                errorCount++;
            }

        }

        public static void SlowDoubleTap(Point point, AndroidDriver<AndroidElement> driver)
        {
            try
            {
                TouchAction touchAction = new TouchAction(driver);
                touchAction.Press(point.X, point.Y).Wait(10).Release().Wait(700).Tap(point.X, point.Y).Press(point.X, point.Y).Wait(10).Release().Perform();
                errorCount = 0;
                Console.WriteLine("[Android] DriverTouch - Performed slow double tap at point: [" + point.X + ", " + point.Y + "]");
            }
            catch (Exception e)
            {
                Console.WriteLine("[Android] DriverTouch - Performing slow double tap failed ( consecutive: " + errorCount + "). Error: " + e.Message);
                if (errorCount < consecutiveErrorLimit)
                {
                    System.Threading.Thread.Sleep(retryWait);
                    SlowDoubleTap(point, driver);
                }
                else
                {
                    Assert.Fail("Performing slow double tap failed!");
                }
                errorCount++;
            }
        }

        public static void SlowDoubleTap(Point point, IOSDriver<IOSElement> idriver)
        {
            try
            {
                TouchAction touchAction = new TouchAction(idriver);
                touchAction.Press(point.X, point.Y).Wait(10).Release().Wait(700).Tap(point.X, point.Y).Press(point.X, point.Y).Wait(10).Release().Perform();
                errorCount = 0;
                Console.WriteLine("[iOS] DriverTouch - Performed slow double tap at point: [" + point.X + ", " + point.Y + "]");
            }
            catch (Exception e)
            {
                Console.WriteLine("[iOS] DriverTouch - Performing slow double tap failed ( consecutive: " + errorCount + "). Error: " + e.Message);
                if (errorCount < consecutiveErrorLimit)
                {
                    System.Threading.Thread.Sleep(retryWait);
                    SlowDoubleTap(point, idriver);
                }
                else
                {
                    Assert.Fail("Performing slow double tap failed!");
                }
                errorCount++;
            }

        }

        public static void UnlockSwipe(AndroidDriver<AndroidElement> driver)
        {
            TouchAction touchAction = new TouchAction(driver);
            touchAction.Press(720, 2350).MoveTo(720, 1340).Release().Perform();
        }

        public static void SetScreenshotRatio(int screenshotRatio)
        {
            DriverTouch.screenshotRatio = screenshotRatio;
        }
    }
}
