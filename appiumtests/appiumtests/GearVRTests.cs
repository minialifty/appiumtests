﻿using System;
using System.IO;
using NUnit.Framework;
using OpenCvSharp;
using OpenCvSharp.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.Service;

namespace appiumtests
{
    class GearVRTests
    {
        string rootPath;
        Phone phone;
        public bool saveSuccessScreens = true;
        public int warpSteps = 10;

        string magicLink = "https://fp23h.app.goo.gl/WUgf1VZZZ3XyT6oB8";
        string magicLink8k = "https://fp23h.app.goo.gl/worfQ7bG7EaB3jTm7";
        string activity = "com.unity3d.player.UnityPlayerActivity";

        int serverRetryCount = 0;
        int serverRetryLimit = 3;

        AppiumLocalService server;
        AndroidDriver<AndroidElement> driver;
        AppiumOptions cap;

        ScreenshotActions screenshotActions;
        Detector detector;
        Touch touchInput;
        Confirmer confirmer;

        int errorCount = 0;
        int consecutiveErrorLimit = 3;

        bool initialised = false;

        public void InitializeParams()
        {
            var target = TestContext.Parameters.Get("Device");
            phone = ParseTargtDevice(target);
            rootPath = TestContext.Parameters.Get("Rootpath");
            var warpSteps = TestContext.Parameters.Get("WarpSteps");
            Console.WriteLine("Targeted device: " + phone + " Root path: " + rootPath);
            if (!int.TryParse(warpSteps, out this.warpSteps) || warpSteps.Equals(""))
            {
                this.warpSteps = 10;
                Console.WriteLine("Warp steps set to default: " + warpSteps);
            }
            else
            {
                Console.WriteLine("Warp steps set to: " + warpSteps);
            }
            Assert.AreNotEqual(Phone.non_recogised, phone, "Target device was not recogised");
            Assert.IsFalse((!Directory.Exists(rootPath) || !Directory.Exists(rootPath + "AppiumTestsOutput/") || !Directory.Exists(rootPath + "AppiumTests/")), "Project path setup is incorrect");
            initialised = true;
        }

        public bool FindOrStartAppiumServer()
        {
            if (server != null && server.IsRunning)
            {
                serverRetryCount = 0;
                return true;
            }
            try
            {
                Console.WriteLine("Building new server service");
                //AppiumServiceBuilder serverBuilder = new AppiumServiceBuilder().WithIPAddress("127.0.0.1").UsingPort(4723).WithAppiumJS(new FileInfo("C:/Users/theC/node_modules/appium/build/lib/main.js"));
                AppiumServiceBuilder serverBuilder = new AppiumServiceBuilder().WithIPAddress("127.0.0.1").UsingAnyFreePort().WithAppiumJS(new FileInfo("C:/Users/theC/node_modules/appium/build/lib/main.js"));
                server = serverBuilder.Build();
                server.Start();
                Console.WriteLine("Server started on: " + server.ServiceUrl);
                if (server.IsRunning)
                {
                    serverRetryCount = 0;
                    return true;
                }
                else
                {
                    Console.WriteLine("Server not running...");
                    if (serverRetryCount >= serverRetryLimit)
                    {
                        Console.WriteLine("Failed to start server in " + serverRetryCount + " retries");
                        return false;
                    }
                    serverRetryCount++;
                    FindOrStartAppiumServer();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error staring appium server! " + e.Message);
                if (serverRetryCount >= serverRetryLimit)
                {
                    Console.WriteLine("Failed to start server in " + serverRetryCount + " retries");
                    return false;
                }
                serverRetryCount++;
                FindOrStartAppiumServer();
            }
            return false;
        }


        [SetUp]
        public void SetUp()
        {
            if (!initialised) InitializeParams();
            cap = new AppiumOptions();
            cap.AddAdditionalCapability("automationName", "uiautomator2");
            cap.AddAdditionalCapability("platformName", "Android");
            cap.AddAdditionalCapability("appPackage", "co.theConstruct.theViewer.GVR");
            cap.AddAdditionalCapability("appActivity", "co.theconstructutils.viewerplugin.ViewerActivity");
            cap.AddAdditionalCapability("newCommandTimeout", "60");
            cap.AddAdditionalCapability("autoLaunch", false);
            cap.AddAdditionalCapability("skipUnlock", true);
            cap.AddAdditionalCapability("noReset", true);

            switch ((int)phone)
            {
                case 0:
                    cap.AddAdditionalCapability("deviceName", "Galaxy S6");
                    cap.AddAdditionalCapability("udid", "06157df6b8718307");
                    cap.AddAdditionalCapability("platformVersion", "7.0");
                    break;
                case 1:
                    cap.AddAdditionalCapability("deviceName", "Samsung Galaxy S7 edge");
                    cap.AddAdditionalCapability("udid", "e4381d5e");
                    cap.AddAdditionalCapability("platformVersion", "8.0");
                    break;
            }
            FindOrStartAppiumServer();
            driver = new AndroidDriver<AndroidElement>(server, cap);
            string sessionID = driver.SessionId.ToString();
            Console.WriteLine("Session id: " + sessionID);

            screenshotActions = new ScreenshotActions(driver, rootPath);
            detector = new Detector(screenshotActions);
            touchInput = new Touch(driver);
            confirmer = new Confirmer(detector, screenshotActions, touchInput, true, rootPath, saveSuccessScreens);
        }

        [TearDown]
        public void Close()
        {
            Console.WriteLine("Closing app");
            CloseApp();
            PressKeyCode(AndroidKeyCode.Keycode_HOME);
            PressKeyCode(AndroidKeyCode.Keycode_APP_SWITCH);
            System.Threading.Thread.Sleep(1000);
            try
            {
                AndroidElement closeApps = driver.FindElementByAccessibilityId("Close all");
                closeApps.Click();
            }
            catch (Exception e)
            {
                Console.WriteLine("Closing all apps failed!");
                PressKeyCode(AndroidKeyCode.Keycode_APP_SWITCH);
            }
            QuitDriver();
        }

        public void BrowserSetUp()
        {
            cap = new AppiumOptions();
            cap.AddAdditionalCapability("automationName", "uiautomator2");
            cap.AddAdditionalCapability("platformName", "Android");
            cap.AddAdditionalCapability("newCommandTimeout", "60");
            cap.AddAdditionalCapability("autoLaunch", false);
            cap.AddAdditionalCapability("skipUnlock", true);;
            cap.AddAdditionalCapability("browserName", "Chrome");
            switch ((int)phone)
            {
                case 0:
                    cap.AddAdditionalCapability("deviceName", "Galaxy S6");
                    cap.AddAdditionalCapability("udid", "06157df6b8718307");
                    cap.AddAdditionalCapability("platformVersion", "7.0");
                    break;
                case 1:
                    cap.AddAdditionalCapability("deviceName", "Samsung Galaxy S7 edge");
                    cap.AddAdditionalCapability("udid", "e4381d5e");
                    cap.AddAdditionalCapability("platformVersion", "8.0");
                    break;
            }
            FindOrStartAppiumServer();
            driver = new AndroidDriver<AndroidElement>(server, cap);
            string sessionID = driver.SessionId.ToString();
            Console.WriteLine("Session id: " + sessionID);
        }

        public void TakeScreenshots()
        {
            UnlockPhone();
            driver.ResetApp();
            System.Threading.Thread.Sleep(500);
            screenshotActions.screenshotPrefix = "GVR_Take";
            screenshotActions.SetScreenshotRatio(touchInput);
            GrantAllows();
            //bool found = detector.SeekTemplateOld(Template.GVR_SkipButton, 30, 0, false, true, 0.1f);
            //Assert.IsTrue(found);
            Mat histgramTemplate = screenshotActions.LoadHistogram(Histogram.GVR_LoginScreen);

            bool compared = false;
            for (int i = 0; i < 30; i++)
            {
                compared = screenshotActions.HistogramCompare(histgramTemplate, 0.03f);
                if (compared)
                {
                    Console.WriteLine("Found try " + i);
                    break;
                }
            }
            for (int i = 0; i < 5; i++)
            {
                screenshotActions.LoadNewScreenshot();
            }
        }

        [Test]
        public void MagicLink_CubeTest()
        {
            UnlockPhone();
            driver.ResetApp();
            System.Threading.Thread.Sleep(500);
            screenshotActions.screenshotPrefix = "GVR_Take";
            screenshotActions.SetScreenshotRatio(touchInput);
            GrantAllows();
            System.Threading.Thread.Sleep(500);
            Close();
            BrowserSetUp();
            driver.Url = magicLink;
            System.Threading.Thread.Sleep(500);
            bool found = SeekActivity(activity, 20, 500);
            Assert.IsTrue(found, "Not expected activity");
            Console.WriteLine("Found theViewer activity.");
            Close();
            SetUp();
            driver.LaunchApp();
            System.Threading.Thread.Sleep(20000);
            bool confirmed = false;
            for (int i = 0; i < 15; i++)
            {
                confirmed = ConfirmVRModeTestTour(false);
                if(confirmed)
                {
                    break;
                }
            }
            Assert.IsTrue(confirmed, "Test Tour not confirmed");
            Point tapPoint = new Point(screenshotActions.screenshot.Width / 4, screenshotActions.screenshot.Height / 2);
            touchInput.DoubleTapAtPoint(tapPoint);
            System.Threading.Thread.Sleep(500);
            for (int i = 0; i < 5; i++)
            {
                confirmed = ConfirmVRModeTestTour(true);
                if (confirmed)
                {
                    break;
                }
            }
            Assert.IsTrue(confirmed);
        }

        [Test]
        public void MagicLink_SphericTest()
        {
            UnlockPhone();
            driver.ResetApp();
            System.Threading.Thread.Sleep(500);
            screenshotActions.screenshotPrefix = "GVR_Take";
            screenshotActions.SetScreenshotRatio(touchInput);
            GrantAllows();
            System.Threading.Thread.Sleep(500);
            Close();
            BrowserSetUp();
            driver.Url = magicLink8k;
            System.Threading.Thread.Sleep(500);
            bool found = SeekActivity(activity, 20, 500);
            Assert.IsTrue(found, "Not expected activity");
            Console.WriteLine("Found theViewer activity.");
            Close();
            SetUp();
            driver.LaunchApp();
            System.Threading.Thread.Sleep(20000);
            bool confirmed = false;
            for (int i = 0; i < 15; i++)
            {
                confirmed = ConfirmTour8k(false);
                if (confirmed)
                {
                    break;
                }
            }
            Assert.IsTrue(confirmed, "Test Tour not confirmed");
            Point tapPoint = new Point(screenshotActions.screenshot.Width / 4, screenshotActions.screenshot.Height / 2);
            touchInput.DoubleTapAtPoint(tapPoint);
            System.Threading.Thread.Sleep(500);
            for (int i = 0; i < 5; i++)
            {
                confirmed = ConfirmTour8k(true);
                if (confirmed)
                {
                    break;
                }
            }
            Assert.IsTrue(confirmed);
        }

        bool GrantAllows()
        {
            bool found = detector.SeekTemplate(Element.AllowButton, 10, 100, true, saveSuccessScreens);
            if (found)
            {
                for (int i = 0; i < 5; i++)
                {
                    touchInput.TapAtPoint(detector.centerPoint);
                }
            }
            return found;
        }

        void UnlockPhone()
        {
            if (driver.IsLocked())
            {
                driver.Unlock();
                System.Threading.Thread.Sleep(30);
                Console.WriteLine("Device unlock context: " + driver.CurrentActivity);
                touchInput.UnlockSwipe();
                if (phone == Phone.SmGalaxyS7edge)
                {
                    System.Threading.Thread.Sleep(30);
                    UnlockCode();
                }
            }
            else
            {
                Console.WriteLine("Device already unlocked - context: " + driver.CurrentActivity);
            }
        }

        void UnlockCode()
        {
            for (int i = 1; i < 7; i++)
            {
                AndroidElement codeKey = driver.FindElementByAccessibilityId(i.ToString());
                codeKey.Click();
                System.Threading.Thread.Sleep(30);
            }
            AndroidElement ok = driver.FindElementByAccessibilityId("OK");
            ok.Click();
        }

        // <summary>
        /// Checks if drivers current activity equals activity passed in parameter
        /// </summary>
        /// <param name="activity"></param>
        /// <param name="retries"></param>
        /// <param name="wait"></param>
        /// <returns></returns>
        bool SeekActivity(string activity, int retries, int wait)
        {
            for (int i = 0; i < retries; i++)
            {
                if (driver.CurrentActivity.Equals(activity))
                {
                    return true;
                }
                System.Threading.Thread.Sleep(wait);
            }
            Console.WriteLine("Not expected activity (" + activity + ")" + driver.CurrentActivity);
            return false;
        }

        void CloseApp()
        {
            try
            {
                driver.CloseApp();
                errorCount = 0;
            }
            catch(Exception e)
            {
                Console.WriteLine("Consecutive Error: " + errorCount + " Error message: " + e.Message);
                if (errorCount < consecutiveErrorLimit)
                {
                    CloseApp();
                }
                else
                {
                    Assert.Fail("Could not close the app");
                }
            }
            errorCount++;
        }

        void QuitDriver()
        {
            try
            {
                driver.Quit();
                errorCount = 0;
            }
            catch (Exception e)
            {
                Console.WriteLine("Consecutive Error: " + errorCount + " Error message: " + e.Message);
                if (errorCount < consecutiveErrorLimit)
                {
                    QuitDriver();
                }
                else
                {
                    Assert.Fail("Could not close the driver");
                }
                errorCount++;
            }
        }

        void PressKeyCode(int keyCode)
        {
            try
            {
                driver.PressKeyCode(keyCode);
                errorCount = 0;
            }
            catch (Exception e)
            {
                Console.WriteLine("Consecutive Error: " + errorCount + " Error message: " + e.Message);
                if (errorCount < consecutiveErrorLimit)
                {
                    PressKeyCode(keyCode);
                }
                else
                {
                    Assert.Fail("Could not press key code: " + keyCode);
                }
                errorCount++;
            }
        }

        public bool ConfirmVRModeTestTour(bool HUDpresent)
        {
            Console.WriteLine("GVR: Attempting to confirm Test Tour in VRMode...");
            bool compared;
            if(HUDpresent)
            {
                compared = screenshotActions.HistogramCompareLR(Histogram.GVR_TestTourLandingHUD);
            }
            else
            {
                compared = screenshotActions.HistogramCompareLR(Histogram.GVR_TestTourLanding);
            }
            if (compared)
            {
                Console.WriteLine("GVR: Test Tour VR landing pano Confirmed by histogram");
                return true;
            }
            return false;
        }

        public bool ConfirmTour8k(bool HUDpresent)
        {
            Console.WriteLine("GVR: Attempting to confirm Tour 8k in VRMode...");
            bool compared;
            if (HUDpresent)
            {
                compared = screenshotActions.HistogramCompareLR(Histogram.GVR_Tour8kHUD);
            }
            else
            {
                compared = screenshotActions.HistogramCompareLR(Histogram.GVR_Tour8k);
            }
            if (compared)
            {
                Console.WriteLine("GVR: Tour 8k landing pano confirmed by histogram");
                return true;
            }
            return false;
        }

        Phone ParseTargtDevice(string text)
        {
            Phone phone;
            if (!Enum.TryParse<Phone>(text, out phone))
            {
                phone = Phone.non_recogised;
            }
            return phone;
        }
    }

    
}
