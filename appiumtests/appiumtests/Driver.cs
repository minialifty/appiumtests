﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.iOS;

namespace appiumtests
{
    class Driver
    {
        static AndroidDriver<AndroidElement> driver;
        static IOSDriver<IOSElement> idriver;
        public static bool android;

        static Uri androidServer = new Uri("http://127.0.0.1:4723/wd/hub");
        static Uri iOSServer = new Uri("http://192.168.1.116:4723/wd/hub");

        static int errorCount = 0;
        static int consecutiveErrorLimit = 3;

        Driver(TestDevice device, bool app, bool freshStart)
        {
            SetUpDriver(device, app, freshStart);
        }

        /// <summary>
        /// Starts appium session
        /// </summary>
        /// <param name="device">Tested device</param>
        /// <param name="app">theViewer app or browser (Chrome/Safari)</param>
        /// <param name="freshStart">reinstall on iOS, app reset on Android</param>
        public static void SetUpDriver(TestDevice device, bool app, bool freshStart)
        {
            if(IsAndroidDevice(device))
            {
                SetUpAndroid(device, app, freshStart);
            }
            else
            {
                SetUpIOS(device, app, freshStart);
            }
        }

        static void SetUpIOS(TestDevice device, bool app, bool reinstallApp)
        {
            if(device == TestDevice.iPhone6 || device == TestDevice.iPhone6Plus)
            {
                AppiumOptions options = SetCapsiOS(device, app, reinstallApp);
                idriver = new IOSDriver<IOSElement>(iOSServer, options);
            }
            else
            {
                Console.WriteLine("Driver - Unsupported iOS device: " + device);
                Assert.Fail("Unsupported iOS device: " + device);
            }
        }

        static void SetUpAndroid( TestDevice device, bool app, bool resetApp)
        {
            if (device == TestDevice.SmGalaxyS6 || device == TestDevice.SmGalaxyS7edge)
            {
                AppiumOptions options = SetCapsAndroid(device, app);
                driver = new AndroidDriver<AndroidElement>(androidServer, options);
                AppiumDriver<AppiumWebElement> driv = new AndroidDriver<AppiumWebElement>(options);
            }
            else
            {
                Console.WriteLine("Driver - Unsupported Android device: " + device);
                Assert.Fail("Unsupported Android device: " + device);
            }

        }

        static AppiumOptions SetCapsiOS(TestDevice device, bool app, bool reinstallApp)
        {
            Console.WriteLine("iOS: Setting up caps...[" + device + ", app: " + app + " reinstallApp: " + reinstallApp + "]");
            AppiumOptions options = new AppiumOptions();
            options.AddAdditionalCapability("automationName", "XCUITest");
            options.AddAdditionalCapability("platformName", "iOS");
            options.AddAdditionalCapability("xcodeOrgId", "9B259JMCA9");
            options.AddAdditionalCapability("xcodeSigningId", "iPhone Developer");
            options.AddAdditionalCapability("shouldUseSingletonTestManager", false);
            options.AddAdditionalCapability("useNewWDA", true);
            options.AddAdditionalCapability("wdaLaunchTimeout", 999999999);
            options.AddAdditionalCapability("wdaConnectionTimeout", 999999999);
            options.AddAdditionalCapability("wdaStartupRetries", 3);
            options.AddAdditionalCapability("newCommandTimeout", 80);
            switch ((int)device)
            {
                case 2:
                    options.AddAdditionalCapability("udid", "bdc8a593bef30818df6ef3c410c9f5f332d18909");
                    options.AddAdditionalCapability("deviceName", "dariuszthecip6id2");
                    options.AddAdditionalCapability("platformVersion", "12.2");
                    break;
                case 3:
                    options.AddAdditionalCapability("udid", "e06ca7466009f4661082b490a8a997fc27245159");
                    options.AddAdditionalCapability("deviceName", "dariusz@thec-ip6+");
                    options.AddAdditionalCapability("platformVersion", "12.2");
                    break;
            }
            //if session will start with theViewer app or browser
            if (app)
            {
                options.AddAdditionalCapability("bundleId", "co.theConstruct.theViewer.CBD");
            }
            else
            {
                options.AddAdditionalCapability("browserName", "Safari");
            }
            //should app be reinstalled
            if(reinstallApp)
            {
                options.AddAdditionalCapability("noReset", "false");
                options.AddAdditionalCapability("app", "/Users/lukasz/Library/Developer/Xcode/DerivedData/Unity-iPhone-gftlbihiohmvlcbwrdiylqpkcnnl/Build/Products/Release-iphoneos/CBD.app");
            }
            else
            {
                options.AddAdditionalCapability("noReset", "true");
            }
            return options;
        }
        
        static AppiumOptions SetCapsAndroid(TestDevice device, bool app)
        {
            Console.WriteLine("Android: Setting up caps...[" + device + ", app: " + app + "]");
            AppiumOptions options = new AppiumOptions();
            options.AddAdditionalCapability("automationName", "uiautomator2");
            options.AddAdditionalCapability("platformName", "Android");
            options.AddAdditionalCapability("newCommandTimeout", "60");
            options.AddAdditionalCapability("autoLaunch", false);
            options.AddAdditionalCapability("skipUnlock", true);
            options.AddAdditionalCapability("noReset", "true");
            switch ((int)device)
            {
                case 0:
                    options.AddAdditionalCapability("deviceName", "Galaxy S6");
                    options.AddAdditionalCapability("udid", "06157df6b8718307");
                    options.AddAdditionalCapability("platformVersion", "7.0");
                    break;
                case 1:
                    options.AddAdditionalCapability("deviceName", "Samsung Galaxy S7 edge");
                    options.AddAdditionalCapability("udid", "e4381d5e");
                    options.AddAdditionalCapability("platformVersion", "8.0");
                    break;
            }

            if(app)
            {
                options.AddAdditionalCapability("appPackage", "co.theConstruct.theViewer.CBD");
                options.AddAdditionalCapability("appActivity", "co.theconstructutils.viewerplugin.ViewerActivity");
            }
            else
            {
                options.AddAdditionalCapability("browserName", "Chrome");
            }
            return options;
        }

        public static bool IsAndroidDevice(TestDevice device)
        {
            if(device == TestDevice.SmGalaxyS6 || device == TestDevice.SmGalaxyS7edge)
            {
                android = true;
                return true;
            }
            if(device == TestDevice.iPhone6 || device == TestDevice.iPhone6Plus)
            {
                android = false;
                return false;
            }
            Assert.Fail("Unsupported device:" + device);
            return false;
        }

        public static Touch GetTouchInput()
        {
            if(android)
            {
                return new Touch(driver);
            }
            else
            {
                return new Touch(idriver);
            }
        }

        public static ScreenshotActions GetScreenshotActions()
        {
            if(android)
            {
                return new ScreenshotActions(driver, "outputPath");
            }
            else
            {
                return new ScreenshotActions(idriver, "outputPath");
            }
        }

        public static string Context()
        {
            string context = "";
            try
            {
                context = android ? driver.Context : idriver.Context;
                errorCount = 0;
            }
            catch (Exception e)
            {
                Console.WriteLine("Getting Context - Consecutive Error: " + errorCount + " Error message: " + e.Message);
                if (errorCount < consecutiveErrorLimit)
                {
                    context = Context();
                }
                else
                {
                    Assert.Fail("Could not fetch driver context");
                }
            }
            return context;
        }

        public static void Close()
        {
            try
            {
                Console.WriteLine("Attempting to close app");
                if(android)
                {
                    driver.CloseApp();
                    driver.Quit();
                }
                else
                {
                    idriver.CloseApp();
                    idriver.Quit();
                }
                errorCount = 0;
            }
            catch (Exception e)
            {
                Console.WriteLine("Consecutive Error: " + errorCount + " Error message: " + e.Message);
                if (errorCount < consecutiveErrorLimit)
                {
                    Close();
                }
                else
                {
                    Assert.Fail("Could not close the driver");
                }
            }

        }

        void UnlockPhone(bool useCode)
        {
            if (driver.IsLocked())
            {
                driver.Unlock();
                System.Threading.Thread.Sleep(30);
                Console.WriteLine("[Android] Driver - Device unlock context: " + driver.CurrentActivity);
                DriverTouch.UnlockSwipe(driver);
                if (useCode)
                {
                    System.Threading.Thread.Sleep(30);
                    UnlockCode();
                }
            }
            else
            {
                Console.WriteLine("[Android] Driver - Device already unlocked - context: " + driver.CurrentActivity);
            }
        }

        void UnlockCode()
        {
            Console.WriteLine("[Android] Driver - Entering unlock code...");
            for (int i = 1; i < 7; i++)
            {
                AndroidElement codeKey = driver.FindElementByAccessibilityId(i.ToString());
                codeKey.Click();
                System.Threading.Thread.Sleep(30);
            }
            AndroidElement ok = driver.FindElementByAccessibilityId("OK");
            ok.Click();
            Console.WriteLine("[Android] Driver - Entering unlock code done");
        }

    }
}
