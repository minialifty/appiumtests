﻿using System;
using OpenCvSharp;

namespace appiumtests
{
    class Detector
    {
        public ScreenshotActions loader;
        public Point centerPoint;

        public Detector(ScreenshotActions loader)
        {
            this.loader = loader;
        }

        public bool SeekTemplate(Element element, int retries, int waitDuration, bool overrideScrennshots, bool saveSuccessScreen, bool maxprecision = false, float precision = 0.001f)
        {
            loader.LoadElement(element);
            Console.WriteLine("Loaded template: " + element);
            for (int i = 0; i < retries; i++)
            {
                if (overrideScrennshots)
                {
                    loader.LoadNewTempScreenshot();
                }
                else
                {
                    loader.LoadNewScreenshot();
                }
                if (DetectTemplateOnScreenshot(saveSuccessScreen,maxprecision, precision))
                {
                    return true;
                }
                System.Threading.Thread.Sleep(waitDuration);
            }
            //Save last try's screenshot if failed to find template
            loader.SaveResultScreenshot(loader.screenshot, false);
            return false;
        }

        public bool DetectTemplateOnScreenshot(bool saveSuccessScreen, bool maxprecision = false, float precision = 0.001f)
        {
            Mat screenshot = loader.screenshot;
            Mat template = loader.template;

            double matchLimit = 0.2;
            if (maxprecision)
            {
                matchLimit = precision;
            }
            Mat result = new Mat();
            int result_cols = screenshot.Cols - template.Cols + 1;
            int result_rows = screenshot.Rows - template.Rows + 1;
            result.Create(MatType.CV_32FC1, result_cols, result_rows);
            Cv2.MatchTemplate(screenshot, template, result, TemplateMatchModes.SqDiffNormed);
            double minVal = 0; double maxVal = 0; Point minLoc = new Point(); Point maxLoc = new Point();
            Point matchLoc;
            Cv2.MinMaxLoc(result, out minVal, out maxVal, out minLoc, out maxLoc);
            matchLoc = minLoc;
            Console.WriteLine("Min val: [" + minVal + "] minLoc: [" + minLoc + "] maxVal: [" + maxVal + "] maxLoc: [" + maxLoc + "]");
            if (minVal <= matchLimit)
            {
                Point centralMatchPoint = new Point(matchLoc.X + template.Cols / 2, matchLoc.Y + template.Rows / 2);
                centerPoint = centralMatchPoint;
                if(saveSuccessScreen)
                {
                    /// Mark found template and save screenshot
                    Cv2.Rectangle(screenshot, matchLoc, new Point(matchLoc.X + template.Cols, matchLoc.Y + template.Rows), new Scalar(), 2, LineTypes.Link8, 0);
                    Cv2.Circle(screenshot, centerPoint, 10, Scalar.Red, 4, LineTypes.Link8, 0);
                    loader.SaveResultScreenshot(screenshot, true);
                }
                Console.WriteLine("Found match! Central point " + loader.currentTemplate + ": " + centralMatchPoint);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Runs detecting key.
        /// </summary>
        /// <param name="key">Key to detect</param>
        /// <param name="retries">Number of retries</param>
        /// <param name="waitDuration">Wait in ms between detection runs</param>
        /// <param name="saveSuccessScreen">Should success screenshot be saved (Saving is done in detecting function). Fail is always saved at the end</param>
        /// <returns></returns>
        public bool SeekKey(Key key, int retries, int waitDuration, bool saveSuccessScreen)
        {
            loader.LoadKey(key);
            for (int i = 0; i < retries; i++)
            {
                if (DetectKeyOnScreenshot(true, saveSuccessScreen))
                {
                    return true;
                }
            }
            loader.SaveResultScreenshot(loader.screenshot, false);
            return false;
        }
        
        /// <summary>
        /// Use outside Detector should be for debug only
        /// </summary>
        /// <param name="maxprecision"></param>
        /// <param name="saveSuccessScreen">If should save success screenshot</param>
        /// <returns></returns>
        public bool DetectKeyOnScreenshot(bool maxprecision, bool saveSuccessScreen)
        {
            Mat screenshot = loader.screenshot;
            Mat keyTemplate = loader.keyTemplate;
            double matchLimit = 0.2;
            if (maxprecision)
            {
                matchLimit = 0.001;
            }
            Mat result = new Mat();
            int result_cols = screenshot.Cols - keyTemplate.Cols + 1;
            int result_rows = screenshot.Rows - keyTemplate.Rows + 1;
            result.Create(MatType.CV_32FC1, result_cols, result_rows);
            Cv2.MatchTemplate(screenshot, keyTemplate, result, TemplateMatchModes.SqDiffNormed);
            double minVal = 0; double maxVal = 0; Point minLoc = new Point(); Point maxLoc = new Point();
            Point matchLoc;
            Cv2.MinMaxLoc(result, out minVal, out maxVal, out minLoc, out maxLoc);
            matchLoc = minLoc;
            //Console.WriteLine("Min val: [" + minVal + "] minLoc: [" + minLoc + "] maxVal: [" + maxVal + "] maxLoc: [" + maxLoc + "]");
            if (minVal <= matchLimit)
            {
                Point centralMatchPoint = new Point(matchLoc.X + keyTemplate.Cols / 2, matchLoc.Y + keyTemplate.Rows / 2);
                centerPoint = centralMatchPoint;
                if(saveSuccessScreen)
                {
                    /// Mark found template and save screenshot
                    Cv2.Rectangle(screenshot, matchLoc, new Point(matchLoc.X + keyTemplate.Cols, matchLoc.Y + keyTemplate.Rows), new Scalar(), 2, LineTypes.Link8, 0);
                    Cv2.Circle(screenshot, centerPoint, 10, Scalar.Red, 4, LineTypes.Link8, 0);
                    loader.SaveResultScreenshot(screenshot, true);
                }
                return true;
            }
            return false;
        }

        public bool SeekThumbnail(ThumbnailTemplate thumbnail, int retries, int waitDuration, bool saveSuccessScreen)
        {
            loader.LoadThumbnailTemplate(thumbnail);
            for (int i = 0; i < retries; i++)
            {
                if (DetectThumbnailOnScreenshot(saveSuccessScreen))
                {
                    return true;
                }
                System.Threading.Thread.Sleep(waitDuration);
            }
            loader.SaveResultScreenshot(loader.screenshot,"thumb_" + loader.thumbnailTemplate, false);
            return false;
        }

        public bool DetectThumbnailOnScreenshot(bool saveSuccessScreen) //should work different way than detecting template
        {
            Mat screenshot = loader.screenshot;
            Mat thumbnail = loader.thumbnailTemplate;
            Mat result = new Mat();
            int result_cols = screenshot.Cols - thumbnail.Cols + 1;
            int result_rows = screenshot.Rows - thumbnail.Rows + 1;
            result.Create(MatType.CV_32FC1, result_cols, result_rows);

            Cv2.MatchTemplate(screenshot, thumbnail, result, TemplateMatchModes.SqDiffNormed);

            double minVal = 0; double maxVal = 0; OpenCvSharp.Point minLoc = new OpenCvSharp.Point(); OpenCvSharp.Point maxLoc = new OpenCvSharp.Point();
            OpenCvSharp.Point matchLoc;
            Cv2.MinMaxLoc(result, out minVal, out maxVal, out minLoc, out maxLoc);
            matchLoc = minLoc;
            //Console.WriteLine("Min val: [" + minVal + "] minLoc: [" + minLoc + "] maxVal: [" + maxVal + "] maxLoc: [" + maxLoc + "]");
            if (minVal <= 0.2)
            {
                OpenCvSharp.Point centralMatchPoint = new OpenCvSharp.Point(matchLoc.X + thumbnail.Cols / 2, matchLoc.Y + thumbnail.Rows / 2);
                centerPoint = centralMatchPoint;
                if(saveSuccessScreen)
                {
                    /// Mark found thumbnail template and save screenshot
                    Cv2.Rectangle(screenshot, matchLoc, new OpenCvSharp.Point(matchLoc.X + thumbnail.Cols, matchLoc.Y + thumbnail.Rows), new Scalar(), 2, LineTypes.Link8, 0);
                    Cv2.Circle(screenshot, centerPoint, 10, Scalar.Red, 4, LineTypes.Link8, 0);
                    loader.SaveResultScreenshot(screenshot,"thumb_" + loader.thumbnailTemplate, true);
                }
                return true;
            }
            return false;
        }

        /*
        public bool DetectTemplateOnScreenshotExpectposition(bool maxprecision = false, float precision = 0.001f)
        {
            Mat screenshot = loader.screenshot;
            Mat template = loader.template;

            double matchLimit = 0.2;
            if (maxprecision)
            {
                matchLimit = precision;
            }
            Mat result = new Mat();
            int result_cols = screenshot.Cols - template.Cols + 1;
            int result_rows = screenshot.Rows - template.Rows + 1;
            result.Create(MatType.CV_32FC1, result_cols, result_rows);

            Cv2.MatchTemplate(screenshot, template, result, TemplateMatchModes.SqDiffNormed);

            double minVal = 0; double maxVal = 0; Point minLoc = new Point(); Point maxLoc = new Point();
            Point matchLoc;
            Cv2.MinMaxLoc(result, out minVal, out maxVal, out minLoc, out maxLoc);
            matchLoc = minLoc;
            //Console.WriteLine("Min val: [" + minVal + "] minLoc: [" + minLoc + "] maxVal: [" + maxVal + "] maxLoc: [" + maxLoc + "]");
            if (minVal <= matchLimit)
            {
                /// Mark found template and save screenshot
                //Cv2.Rectangle(screenshot, matchLoc, new Point(matchLoc.X + template.Cols, matchLoc.Y + template.Rows), new Scalar(), 2, LineTypes.Link8, 0);
                //Point centralMatchPoint = new Point(matchLoc.X + template.Cols / 2, matchLoc.Y + template.Rows / 2);
                //centerPoint = centralMatchPoint;
                //Cv2.Circle(screenshot, centerPoint, 10, Scalar.Red, 4, LineTypes.Link8, 0);
                //loader.SaveResultScreenshot(screenshot, true);
                //Console.WriteLine("Found match! Central point of found " + loader.currentTemplate + ": " + centralMatchPoint);
                return true;
            }
            return false;
        }
        */

        /// <summary>
        /// Seeks circles in screen (once). Sets found center point.
        /// </summary>
        /// <param name="saveSuccessScreen"> Should success screenshot be saved. Fail is always saved.</param>
        /// <returns></returns>
        public bool FindWarp(bool saveSuccessScreen)
        {
            loader.LoadNewTempScreenshot();
            Mat screenshotCut = loader.screenshot;
            Mat gray = screenshotCut.CvtColor(ColorConversionCodes.BGR2GRAY);
            CircleSegment[] circleSegments = Cv2.HoughCircles(gray, HoughMethods.Gradient, 10, 10, 300, 100, 5, 100);
            if (circleSegments.Length > 0)
            {
                //Console.WriteLine("Finding Warp - circle segments:" + circleSegments.Length);
                for (int i = 0; i < circleSegments.Length; i++)
                {
                    //Console.WriteLine("I: " + i + " radius: " + circleSegments[i].Radius);
                    float radius = circleSegments[i].Radius;
                    if (radius != 0)
                    {
                        Point center = new Point(circleSegments[i].Center.X, circleSegments[i].Center.Y);
                        centerPoint = center;
                        Console.WriteLine("Found warp: center [" + center + "] radius [" + radius);
                        if (saveSuccessScreen)
                        {
                            Cv2.Circle(screenshotCut, center, (int)radius, Scalar.Black);
                            loader.SaveResultScreenshot(screenshotCut, "warp", true);
                        }
                        return true;
                    }
                }
            }
            loader.SaveResultScreenshot(screenshotCut, "warp", false);
            return false;
        }

    }



}
