﻿using OpenCvSharp;
using OpenCvSharp.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesseract;
using NUnit.Framework;

namespace appiumtests
{
    class Confirmer
    {
        TesseractEngine engine;
        BitmapToPixConverter converterBitPix;

        Detector detector;
        ScreenshotActions screenshotActions;
        Touch touchInput;
        bool saveSuccessScreens;

        bool android;
        public bool greenCode = false;

        public Confirmer(Detector detector, ScreenshotActions screenshotActions, Touch touchInput, bool android, string rootPath, bool saveSuccessScreens)
        {
            this.android = android;
            this.detector = detector;
            this.screenshotActions = screenshotActions;
            this.touchInput = touchInput;
            this.saveSuccessScreens = saveSuccessScreens;

            Console.WriteLine("Confirmer constructor - path for tessdata: " + rootPath + "AppiumTests/tessdata");
            engine = new TesseractEngine(rootPath + "AppiumTests/tessdata", "eng");
            converterBitPix = new BitmapToPixConverter();
        }

        /// <summary>
        /// Finds HUD elements: HudLogo and VRTours header
        /// </summary>
        /// <returns></returns>
        public bool ConfirmHUD()
        {
            bool found = false;
            found = detector.SeekTemplate(Element.HUDLogo, 10, 1000, true, saveSuccessScreens);
            System.Threading.Thread.Sleep(100);
            Console.WriteLine("Confirming HUD - Found " + Element.HUDLogo + ": " + found);
            found = detector.SeekTemplate(Element.VRToursHeader, 3, 200, true, saveSuccessScreens);
            Console.WriteLine("Confirming HUD - Found " + Element.VRToursHeader + ": " + found);
            return found;
        }

        /// <summary>
        /// Seeks Skip tutorial button and taps it if found
        /// </summary>
        public void SkipTutorial()
        {
            bool found = detector.SeekTemplate(Element.SkipTutorialButton, 3, 200, true, saveSuccessScreens);
            if (found)
            {
                Console.WriteLine("Tapped SkipTutorial");
                touchInput.TapAtPoint(detector.centerPoint);
            }
            else
            {
                Console.WriteLine("No tutorial skip button found");
            }
        }

        /// <summary>
        /// Takes screenshot every second and filters it. Calculates box (by gyroButton point) where downloading label is expected and reads text. If no label found seeks HUD on screen. If three consecutive times label was not found but HUD was still in view - returns false (not downloading).
        /// If waiting hits timeoutCounter - returns status of downloading (true)
        /// </summary>
        /// <returns></returns>
        public bool WaitForDownload(Point gyroButtonPoint)
        {
            Tesseract.Rect labelBox = new Tesseract.Rect(gyroButtonPoint.X - screenshotActions.screenshot.Width / 2, gyroButtonPoint.Y - 100, screenshotActions.screenshot.Width / 2, 200);
            int timeoutCounter = 100;
            int successCounter = 0;
            int hudCounter = 0;
            bool downloading = true;
            for (int i = 0; i < timeoutCounter; i++)
            {
                //Get filtered grayscale screenshot
                screenshotActions.LoadNewTempScreenshot();
                Mat filter_screenshot = screenshotActions.FilterNonWhite(screenshotActions.screenshot);
                filter_screenshot = filter_screenshot.CvtColor(ColorConversionCodes.BGR2GRAY);
                Cv2.BitwiseNot(filter_screenshot, filter_screenshot);
                System.Drawing.Bitmap bitmap_screenshot = BitmapConverter.ToBitmap(filter_screenshot);
                //seek downloading label
                string foundText = SearchScreenshotForText(bitmap_screenshot, labelBox);
                downloading = FindDownloadingLabel(foundText);
                Console.WriteLine("Downloading label present: " + downloading);
                //If no downloading label found
                if (!downloading)
                {
                    if (!ConfirmHUDText(bitmap_screenshot))
                    {
                        Console.WriteLine("Searching for download label but no HUD found!");
                        screenshotActions.SaveResultScreenshot(filter_screenshot, "downloading_" + i + "_HUD_not_found", true);
                        successCounter = 0;
                        hudCounter++;
                    }
                    else
                    {
                        Console.WriteLine("Dowlnoading label not found, HUD confirmed!");
                        screenshotActions.SaveResultScreenshot(filter_screenshot, "downloading_" + i + "_no_label_" + successCounter, true);
                        successCounter++;
                        hudCounter = 0;
                    }
                }
                else
                {
                    hudCounter = 0;
                    successCounter = 0;
                }

                if (successCounter > 2)
                {
                    return downloading;
                }

                if(hudCounter > 4)
                {
                    Console.WriteLine("Hud not in view for too long");
                    return false;
                }

                System.Threading.Thread.Sleep(1000);
            }
            Console.WriteLine("Number of runs for downloading label: " + timeoutCounter);
            return downloading;
        }

        /// <summary>
        /// Reads text from full screenshot (is supposed to be already filtered)
        /// </summary>
        /// <param name="screenshot"></param>
        /// <returns></returns>
        string SearchScreenshotForText(System.Drawing.Bitmap screenshot)
        {
            Pix img = converterBitPix.Convert(screenshot);
            Page page = engine.Process(img, PageSegMode.SingleBlock);
            string text = page.GetText();
            page.Dispose();
            return text;
        }

        /// <summary>
        /// Reads text form requested area
        /// </summary>
        /// <param name="screenshot"></param>
        /// <param name="roi"></param>
        /// <returns></returns>
        string SearchScreenshotForText(System.Drawing.Bitmap screenshot, Tesseract.Rect roi)
        {
            Pix img = converterBitPix.Convert(screenshot);
            Page page = engine.Process(img, roi, PageSegMode.SingleBlock);
            string text = page.GetText();
            page.Dispose();
            return text;
        }

        /// <summary>
        /// Seeks downlading label in string
        /// </summary>
        /// <param name="foundText"></param>
        /// <returns></returns>
        bool FindDownloadingLabel(string foundText)
        {
            if (foundText.Contains("Downloading") || foundText.Contains("Downioading") || foundText.Contains("Download") || foundText.Contains("Down") || foundText.Contains("Dewnleading") || foundText.Contains("images"))
            {
                return true;
            }
            Console.WriteLine("Did not found dowloading label in:  " + foundText);
            return false;
        }

        /// <summary>
        /// Takes screenshot, filters it, iverses and then calls ConfirmHUDText requested retry times
        /// </summary>
        /// <param name="retries"></param>
        /// <param name="waitDuration"></param>
        /// <returns></returns>
        public bool ConfirmHUDWithText(int retries, int waitDuration, bool expectFail = false)
        {
            bool confirm = true;
            Mat filter_screenshot = new Mat();
            for (int i = 0; i < retries; i++)
            {
                screenshotActions.LoadNewTempScreenshot();
                filter_screenshot = screenshotActions.FilterNonWhite(screenshotActions.screenshot);
                filter_screenshot = filter_screenshot.CvtColor(ColorConversionCodes.BGR2GRAY);
                Cv2.BitwiseNot(filter_screenshot, filter_screenshot);
                System.Drawing.Bitmap bitmap_screenshot = BitmapConverter.ToBitmap(filter_screenshot);
                confirm = ConfirmHUDText(bitmap_screenshot);
                if (confirm)
                {
                    if(!expectFail)
                    {
                        screenshotActions.SaveResultScreenshot(filter_screenshot, "HUDConfirm", true);
                    }
                    else
                    {
                        screenshotActions.SaveResultScreenshot(filter_screenshot, "HUDHide", false);
                    }
                    return true;
                }
                System.Threading.Thread.Sleep(waitDuration);
            }
            if(!expectFail)
            {
                screenshotActions.SaveResultScreenshot(filter_screenshot, "HUDConfirm", false);
            }
            return false;
        }

        /// <summary>
        /// Reads text from upper half of screenshot and then lower half
        /// </summary>
        /// <param name="bitmap_screenshot"></param>
        /// <returns></returns>
        bool ConfirmHUDText(System.Drawing.Bitmap bitmap_screenshot)
        {
            //search upper half of screenshot for text
            string foundText = SearchScreenshotForText(bitmap_screenshot, new Tesseract.Rect(0, 0, bitmap_screenshot.Width, bitmap_screenshot.Height / 2));
            if (foundText.Contains("theVIEWER") || foundText.Contains("VIEWER") || foundText.Contains("V|EWER") || foundText.Contains("V'EWER") || foundText.Contains("VlEWER"));
            {
                if (foundText.Contains("VR Tours") || foundText.Contains("VRTours") || foundText.Contains("VR Tou rs"))
                {
                    //search bottom half of screenshot for text
                    foundText = SearchScreenshotForText(bitmap_screenshot, new Tesseract.Rect(0, bitmap_screenshot.Height / 2, bitmap_screenshot.Width, bitmap_screenshot.Height / 2));
                    if (foundText.Contains("Actions") || foundText.Contains("Actlons") || foundText.Contains("Achons")) ;
                    {
                        return true;
                    }
                }
            }
            Console.WriteLine("HUD elements was not found in: " + foundText);
            return false;
        }

        /// <summary>
        /// Seeks 10 screenshots (1s) for BlueVase template, if found checks id OrangeVase is also present
        /// </summary>
        /// <returns></returns>
        public bool ConfirmVestry70()
        {
            bool found = detector.SeekTemplate(Element.Vestry70BlueVase, 10, 1000, true, saveSuccessScreens);
            if (found)
            {
                Console.WriteLine("Detected 1st confirmation element for 70 Vestry tour");

                if (detector.SeekTemplate(Element.Vestry70OrangeVase, 10, 200, true, saveSuccessScreens))
                {
                    Console.WriteLine("Detected 2nd confirmation element for 70 Vestry tour");
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Seeks and taps provided keyboard keys
        /// </summary>
        /// <param name="magicCode"></param>
        /// <returns></returns>
        public bool FindAndTapCode(Key[] magicCode)
        {
            screenshotActions.LoadNewTempScreenshot();
            Mat temp_screenshot = screenshotActions.screenshot.Clone();
            bool found = false;
            for (int i = 0; i < magicCode.Length; i++)
            {
                Cv2.WaitKey();
                screenshotActions.screenshot = temp_screenshot.Clone();
                found = detector.SeekKey(magicCode[i], 3, 200, saveSuccessScreens);
                if (!found)
                {
                    Console.WriteLine("Failed to find key (" + magicCode[i] + ")");
                    return false;
                }
                touchInput.TapAtPoint(detector.centerPoint);
            }
            return true;
        }

        /// <summary>
        /// Checks if keyboard is present, if yes then checks for red and green elements in screenshot. Fails test if found red. Sets greenCode true if found green.
        /// </summary>
        /// <returns></returns>
        public bool CheckKeyboard(string code)
        {

            screenshotActions.LoadNewScreenshot();
            bool found = detector.SeekTemplate(Element.KeyboardPlayPress, 3, 200, true, true, true, 0.01f);
            if (found)
            {
                Console.WriteLine("Confirmed keyboard presence.");
                Point keyboardPlayButton = detector.centerPoint;
                //Console.WriteLine("Calculating possible input box position...");
                Mat inputField = screenshotActions.screenshot; //new OpenCvSharp.Rect(tapPoint.X - screenshot.Width / 2, tapPoint.Y - template.Height, screenshot.Width / 2, template.Height * 2)
                //Console.WriteLine("Calculated");
                Mat filter = screenshotActions.FilterRed(inputField);
                if (filter.CountNonZero() > 0)
                {
                    //Double Check
                    screenshotActions.LoadNewScreenshot();
                    found = detector.SeekTemplate(Element.KeyboardPlayPress, 3, 200, true, true, true, 0.01f);
                    if (found)
                    {
                        inputField = screenshotActions.screenshot;
                        filter = screenshotActions.FilterRed(inputField);
                        if (filter.CountNonZero() > 0)
                        {
                            if(ReadCode(filter, code))
                            {
                                Console.WriteLine("Magic code is incorrect!");
                                Assert.Fail("Provided magic code is incorrect!");
                            }
                        }
                    }
                }
                filter = screenshotActions.FilterGreen(inputField);
                if (filter.CountNonZero() > 0)
                {
                    if (ReadCode(filter, code))
                    {
                        Console.WriteLine("Magic code is correct!");
                        greenCode = true;
                    }
                    else
                    {
                        Console.WriteLine("Found green but could not read code. Not setting green code.");
                        greenCode = false;
                    }
                }
                else
                {
                    Console.WriteLine("Did not found red or green code");
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Tries to find Test Tour with timeout
        /// </summary>
        /// <returns></returns>
        public bool ConfirmEnterFirstMagicCode()
        {
            int timeout = 20;
            for (int i = 0; i < timeout; i++)
            {
                if (ConfirmFirstTestTour())
                {
                    Console.WriteLine("Confirmed entering first magic code!");
                    return true;
                }
                System.Threading.Thread.Sleep(1000);
            }
            Console.WriteLine("Couln't confirm entering first magic code!");
            return false;
        }

        /// <summary>
        /// Tries to find Test Tour 2 with timeout
        /// </summary>
        /// <returns></returns>
        public bool ConfirmEnterSecondMagicCode()
        {
            int timeout = 20;
            for (int i = 0; i < timeout; i++)
            {
                if(ConfirmSecondTestTour())
                {
                    Console.WriteLine("Confirmed entering second magic code!");
                    return true;
                }
                System.Threading.Thread.Sleep(1000);
            }
            Console.WriteLine("Couln't confirm entering second magic code!");
            return false;
        }

        public bool ConfirmEnterThirdMagicCode()
        {
            int timeout = 20;
            for (int i = 0; i < timeout; i++)
            {
                if (ConfirmThirdTestTour())
                {
                    Console.WriteLine("Confirmed entering third magic code!");
                    return true;
                }
                System.Threading.Thread.Sleep(1000);
            }
            Console.WriteLine("Couln't confirm entering third magic code!");
            return false;
        }

        public bool ConfirmFirstTestTour()
        {
            Console.WriteLine("Attempting to confirm Test Tour...");
            bool compared;
            if (android)
            {
                compared = screenshotActions.HistogramCompare(screenshotActions.LoadHistogram(Histogram.TestTourLanding));
            }
            else
            {
                compared = screenshotActions.HistogramCompare(screenshotActions.LoadHistogram(Histogram.iOSTestTourLanding));
            }
            if(compared)
            {
                Console.WriteLine("Test Tour landing pano Confirmed by histogram");
                return true;
            }
            return false;
        }

        public bool ConfirmFirstTestTourHUD()
        {
            Console.WriteLine("Attempting to confirm HUD screen Test Tour...");
            bool compared;
            if (android)
            {
                compared = screenshotActions.HistogramCompare(screenshotActions.LoadHistogram(Histogram.TestTourLandingHUD));
            }
            else
            {
                compared = screenshotActions.HistogramCompare(screenshotActions.LoadHistogram(Histogram.iOSTestTourLandingHUD));
            }
            if (compared)
            {
                Console.WriteLine("HUD screen Test Tour landing pano Confirmed by histogram");
                return true;
            }
            screenshotActions.SaveResultScreenshot(screenshotActions.screenshot, "HUD_pirate", false);
            return false;
        }

        public bool ConfirmSecondTestTour()
        {
            Console.WriteLine("Attempting to confirm Test Tour 2...");
            bool compared;
            if (android)
            {
                compared = screenshotActions.HistogramCompare(screenshotActions.LoadHistogram(Histogram.TestTour2Landing));
            }
            else
            {
                compared = screenshotActions.HistogramCompare(screenshotActions.LoadHistogram(Histogram.iOSTestTour2Landing));
            }
            if (compared)
            {
                Console.WriteLine("Test Tour 2 landing pano Confirmed by histogram");
                return true;
            }
            return false;
        }

        public bool ConfirmThirdTestTour()
        {
            Console.WriteLine("Attempting to confirm Test Tour 3...");
            bool compared = false;
            if (android)
            {
                compared = screenshotActions.HistogramCompare(screenshotActions.LoadHistogram(Histogram.TestTour3Landing));
            }
            else
            {
                compared = screenshotActions.HistogramCompare(screenshotActions.LoadHistogram(Histogram.iOSTestTour3Landing));
            }
            if (compared)
            {
                Console.WriteLine("Test Tour 2 landing pano Confirmed by histogram");
                return true;
            }
            return false;
        }

        public bool ConfirmThirdTestTourHUD()
        {
            Console.WriteLine("Attempting to confirm Test Tour 3 HUD...");
            bool compared = false;
            if (android)
            {
                compared = screenshotActions.HistogramCompare(screenshotActions.LoadHistogram(Histogram.TestTour3LandingHUD));
            }
            else
            {
                compared = screenshotActions.HistogramCompare(screenshotActions.LoadHistogram(Histogram.iOSTestTour3LandingHUD));
            }
            if (compared)
            {
                Console.WriteLine("Test Tour 2 landing pano Confirmed by histogram");
                return true;
            }
            return false;
        }

        public bool ConfirmVRModeTestTour(bool HUDpresent)
        {
            Console.WriteLine("Attempting to confirm Test Tour in VRMode...");
            bool compared;
            if (android)
            {
                if (HUDpresent)
                { compared = screenshotActions.HistogramCompareLR(Histogram.TTLandHUDVR); }
                else
                { compared = screenshotActions.HistogramCompareLR(Histogram.TestTourLandingVR); }
            }
            else
            {
                if (HUDpresent)
                { compared = screenshotActions.HistogramCompareLR(Histogram.iOSTTLandHUDVR); }
                else
                { compared = screenshotActions.HistogramCompareLR(Histogram.iOSTestTourLandingVR); }
            }
            if (compared)
            {
                Console.WriteLine("Test Tour VR landing pano Confirmed by histogram");
                return true;
            }
            return false;
        }

        public bool ConfirmVRModeTestTour3(bool HUDpresent)
        {
            Console.WriteLine("Attempting to confirm Test Tour 3 in VRMode...");
            bool compared;
            if (android)
            {
                if(HUDpresent)
                { compared = screenshotActions.HistogramCompareLR(Histogram.TT3LandHUDVR);    }
                else
                { compared = screenshotActions.HistogramCompareLR(Histogram.TestTour3LandingVR); }
            }
            else
            {
                if(HUDpresent)
                { compared = screenshotActions.HistogramCompareLR(Histogram.iOSTT3LandHUDVR); }
                else
                { compared = screenshotActions.HistogramCompareLR(Histogram.iOSTestTour3LandingVR);   }
            }
            if (compared)
            {
                Console.WriteLine("Test Tour 3 VR landing pano Confirmed by histogram");
                return true;
            }
            return false;
        }

        public bool ConfirmTour8k()
        {
            Console.WriteLine("Attempting to confirm Test Tour 3 in VRMode...");
            bool compared = false;
            if (android)
            {

            }
            else
            {
                screenshotActions.LoadNewScreenshot();
                //compared = screenshotActions.HistogramCompareLR(Histogram.iOSResizeTour);
            }
            if (compared)
            {
                Console.WriteLine("Tour8k landing pano Confirmed by histogram");
                return true;
            }
            return false;
        }

        public bool ConfirmRedBlueSpheric(bool HUDpresent, bool floorplan, int retry, int wait)
        {
            Console.WriteLine("Red blue spheric panoramic");
            Histogram histogram;
            bool confirmed = false;
            if (android)
            {
                if(floorplan)
                {
                    histogram = Histogram.RedSpheric;
                }
                else
                {
                    histogram = Histogram.RedSphericNoFloorplan;
                }  
            }
            else
            {
                if(floorplan)
                {
                    histogram = Histogram.iOSRedSpheric;
                }
                else
                {
                    histogram = Histogram.iOSRedSphericNoFloorplan;
                }
            }
            for (int i = 0; i < retry; i++)
            {
                confirmed = screenshotActions.HistogramCompareDoubleChannel(screenshotActions.LoadHistogram(histogram));
                if (confirmed)
                {
                    Console.WriteLine("Red blue spheric panoramic confirmed by histogram");
                    screenshotActions.SaveResultScreenshot(screenshotActions.screenshot, "hist_" + histogram, true);
                    return true;
                }
                Console.WriteLine("Attempt: " + i + " no confirm");
                System.Threading.Thread.Sleep(wait);
            }
            screenshotActions.SaveResultScreenshot(screenshotActions.screenshot, "hist_" + histogram, false);
            return false;
        }

        public bool ConfirmCyanYellowSpheric(bool HUDpresent, bool floorplan, int retry, int wait)
        {
            Console.WriteLine("Red blue spheric panoramic");
            Histogram histogram;
            bool confirmed = false;
            if (android)
            {
                if(floorplan)
                {
                    histogram = Histogram.CyanSpheric;
                }
                else
                {
                    histogram = Histogram.CyanSphericNoFloorplan;
                }
            }
            else
            {
                if (floorplan)
                {
                    histogram = Histogram.iOSCyanSpheric;
                }
                else
                {
                    histogram = Histogram.iOSCyanSphericNoFloorplan;
                }
            }
            for (int i = 0; i < retry; i++)
            {
                confirmed = screenshotActions.HistogramCompareDoubleChannel(screenshotActions.LoadHistogram(histogram));
                if (confirmed)
                {
                    Console.WriteLine("Cyan spheric panoramic confirmed by histogram");
                    screenshotActions.SaveResultScreenshot(screenshotActions.screenshot, "hist_" + histogram, true);
                    return true;
                }
                Console.WriteLine("Attempt: " + i + " no confirm");
                System.Threading.Thread.Sleep(wait);
            }
            screenshotActions.SaveResultScreenshot(screenshotActions.screenshot, "hist_" + histogram, false);
            return false;
        }

        /// <summary>
        /// Seeks 10 screenshots (1s) for Vestry70Armchair, if found then seeks Vestry70Lamp
        /// </summary>
        /// <returns></returns>
        public bool ConfirmVestry70Clone()
        {
            Console.WriteLine("Attempting to confirm Vestry70Clone...");
            bool found = detector.SeekTemplate(Element.Vestry70Armchair, 10, 1000, true,saveSuccessScreens, true, 0.05f);
            if (found)
            {
                Console.WriteLine("Detected 1st confirmation element for 70 Vestry Clone tour");
                if (detector.SeekTemplate(Element.Vestry70Lamp, 10, 200, true,saveSuccessScreens, true, 0.05f))
                {
                    Console.WriteLine("Detected 2nd confirmation element for 70 Vestry Clone tour");
                    return true;
                }
            }
            return false;
        }

        public bool ReadCode(Mat filter_screenshot, string code)
        {
            System.Drawing.Bitmap bitmap_screenshot = BitmapConverter.ToBitmap(filter_screenshot);
            string text = SearchScreenshotForText(bitmap_screenshot);
            if (text.Contains(code))
            {
                Console.WriteLine("Found code [" + code + "] on screen");
                return true;
            }
            return false;
        }

        


    }



}

