﻿using System;
using System.IO;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.iOS;
using OpenQA.Selenium.Appium.Service;

namespace appiumtests
{
    public class iOSMagicLinkTest
    {
        iPhone iphone;
        string rootPath;
        public bool saveSuccessScreens = true;
        public int warpSteps = 10;

        int errorCount = 0;
        int consecutiveErrorLimit = 4;

        int serverRetryCount = 0;
        int serverRetryLimit = 3;

        string magicLink = "https://fp23h.app.goo.gl/WUgf1VZZZ3XyT6oB8";

        ///Appium variables
        AppiumLocalService server;
        IOSDriver<IOSElement> idriver;
        AppiumOptions options;

        ScreenshotActions screenshotActions;
        Detector detector;
        Touch touchInput;
        Confirmer confirmer;

        bool initialised = false;

        public void InitializeParams()
        {
            var target = TestContext.Parameters.Get("Device");
            iphone = ParseTargtDevice(target);
            rootPath = TestContext.Parameters.Get("Rootpath");
            var warpSteps = TestContext.Parameters.Get("WarpSteps");
            Console.WriteLine("Targeted device: " + iphone + " Root path: " + rootPath);
            if (!int.TryParse(warpSteps, out this.warpSteps) || warpSteps.Equals(""))
            {
                this.warpSteps = 10;
                Console.WriteLine("Warp steps set to default: " + this.warpSteps);
            }
            else
            {
                Console.WriteLine("Warp steps set to: " + warpSteps);
            }
            Assert.AreNotEqual(Phone.non_recogised, iphone, "Target device was not recogised");
            Assert.IsFalse((!Directory.Exists(rootPath) || !Directory.Exists(rootPath + "AppiumTestsOutput/") || !Directory.Exists(rootPath + "AppiumTests/")), "Project path setup is incorrect");
            initialised = true;
        }

        public bool FindOrStartAppiumServer()
        {
            if (server != null && server.IsRunning)
            {
                serverRetryCount = 0;
                return true;
            }
            try
            {

                Console.WriteLine("Building new server service");
                //AppiumServiceBuilder serverBuilder = new AppiumServiceBuilder().WithIPAddress("127.0.0.1").UsingPort(4723).WithAppiumJS(new FileInfo("C:/Users/theC/node_modules/appium/build/lib/main.js"));
                AppiumServiceBuilder serverBuilder = new AppiumServiceBuilder().WithIPAddress("192.168.1.116").UsingAnyFreePort().WithAppiumJS(new FileInfo("C:/Users/theC/node_modules/appium/build/lib/main.js"));
                server = serverBuilder.Build();
                server.Start();
                Console.WriteLine("Server started on: " + server.ServiceUrl);
                if (server.IsRunning)
                {
                    serverRetryCount = 0;
                    return true;
                }
                else
                {
                    Console.WriteLine("Server not running...");
                    if (serverRetryCount >= serverRetryLimit)
                    {
                        Console.WriteLine("Failed to start server in " + serverRetryCount + " retries");
                        return false;
                    }
                    serverRetryCount++;
                    FindOrStartAppiumServer();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error staring appium server! " + e.Message);
                if (serverRetryCount >= serverRetryLimit)
                {
                    Console.WriteLine("Failed to start server in " + serverRetryCount + " retries");
                    return false;
                }
                serverRetryCount++;
                FindOrStartAppiumServer();
            }
            return false;
        }

        void SetUp(bool safari, bool reinstall, iPhone iphone)
        {
            if(!initialised) InitializeParams();
            Console.WriteLine("Setting up caps...");
            options = new AppiumOptions();
            options.AddAdditionalCapability("automationName", "XCUITest");
            options.AddAdditionalCapability("platformName", "iOS");
            options.AddAdditionalCapability("xcodeOrgId", "9B259JMCA9");
            options.AddAdditionalCapability("xcodeSigningId", "iPhone Developer");
            options.AddAdditionalCapability("shouldUseSingletonTestManager", false);
            options.AddAdditionalCapability("useNewWDA", true);
            options.AddAdditionalCapability("wdaLaunchTimeout", 999999999);
            options.AddAdditionalCapability("wdaConnectionTimeout", 999999999);
            options.AddAdditionalCapability("wdaStartupRetries", 3);
            options.AddAdditionalCapability("newCommandTimeout", 60);
            options.AddAdditionalCapability("startIWDP", true);

            if (safari)
            {
                Console.WriteLine("Starting Safari session.");
                options.AddAdditionalCapability("browserName", "Safari");
            }
            else
            {
                Console.WriteLine("Starting theViewer session.");
                options.AddAdditionalCapability("bundleId", "co.theConstruct.theViewer.CBD");
            }
            switch ((int)iphone)
            {
                case 0:
                    options.AddAdditionalCapability("udid", "bdc8a593bef30818df6ef3c410c9f5f332d18909");
                    options.AddAdditionalCapability("deviceName", "dariuszthecip6id2");
                    options.AddAdditionalCapability("platformVersion", "12.2");
                    break;
                case 1:
                    options.AddAdditionalCapability("udid", "f599b2841f6a45d6b9be0c38cda427610aa7bdab");
                    options.AddAdditionalCapability("deviceName", "iPhone");
                    options.AddAdditionalCapability("platformVersion", "11.3");
                    break;
                case 2:
                    options.AddAdditionalCapability("udid", "e06ca7466009f4661082b490a8a997fc27245159");
                    options.AddAdditionalCapability("deviceName", "dariusz@thec-ip6+");
                    options.AddAdditionalCapability("platformVersion", "12.2");

                    break;
            }

            if (reinstall)
            {
                Console.WriteLine("Will reinstall app");
                options.AddAdditionalCapability("noReset", "false");
                options.AddAdditionalCapability("app", "/Users/lukasz/Library/Developer/Xcode/DerivedData/Unity-iPhone-gftlbihiohmvlcbwrdiylqpkcnnl/Build/Products/Release-iphoneos/CBD.app");
            }
            else
            {
                Console.WriteLine("Won't reinstall app on start.");
                options.AddAdditionalCapability("noReset", "true");
            }
            FindOrStartAppiumServer();
            try
            {
                idriver = new IOSDriver<IOSElement>(server, options);
                if (idriver == null)
                {
                    Console.WriteLine("Driver returned null!");
                    Assert.Fail("Driver is null on start!");
                }
                string sessionID = idriver.SessionId.ToString();
                Console.WriteLine("Session id: " + sessionID);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error staring driver: " + e.Message);
            }

            screenshotActions = new ScreenshotActions(idriver, rootPath);
            detector = new Detector(screenshotActions);
            touchInput = new Touch(idriver);
            confirmer = new Confirmer(detector, screenshotActions, touchInput, false, rootPath, saveSuccessScreens);
            Console.WriteLine("Set up complete");
        }

        public void Close()
        {
            Console.WriteLine("Closing app");
            CloseDriver();
        }

        [TearDown]
        public void FailClose()
        {
            if(idriver!=null && idriver.Context!=null)
            {
                Console.WriteLine("Closing app");
                CloseDriver();
            }
            Console.WriteLine("Test ended");
        }

        [Test]
        public void MagicLinkTest()
        {
            if(!initialised) { InitializeParams(); }
            //SetDebugParams();
            
            //Start session with TheViewer app - install fresh theViewer app
            SetUp(false, true, iPhone.iPhone6Plus);
            screenshotActions.screenshotPrefix = "MagicLink";
            Close();
            System.Threading.Thread.Sleep(2000);
            //Start Safari session - Go to magic link
            SetUp(true, false, iPhone.iPhone6Plus);
            Console.WriteLine("Is browser: " + idriver.IsBrowser);
            Console.WriteLine("Context: " + idriver.Context);
            idriver.Url = magicLink;
            System.Threading.Thread.Sleep(5000);
            screenshotActions.LoadNewScreenshot();
            Close();
            SetUp(false, false, iPhone.iPhone6Plus);
            Console.WriteLine("Context: " + idriver.Context);
            Assert.AreEqual(ScreenOrientation.Landscape, idriver.Orientation);
            screenshotActions.SetScreenshotRatio(touchInput);
            bool found = detector.SeekTemplate(Element.NoButton, 10, 1000, true, saveSuccessScreens);
            Assert.IsTrue(found, "NoButton from VRModeAsk screen was not found!");
            touchInput.TapAtPoint(detector.centerPoint);   //for some reason this screen requries two taps
            touchInput.TapAtPoint(detector.centerPoint);
            bool confirm = confirmer.ConfirmEnterFirstMagicCode();
            Assert.IsTrue(confirm, "Could not confirm Test Tour!");
            System.Threading.Thread.Sleep(2000);
        }



        void CloseDriver()
        {
            try
            {
                Console.WriteLine("Attempting to close app");
                idriver.CloseApp();
                idriver.Quit();
                errorCount = 0;
            }
            catch (Exception e)
            {
                Console.WriteLine("Consecutive Error: " + errorCount + " Error message: " + e.Message);
                if (errorCount < consecutiveErrorLimit)
                {
                    CloseDriver();
                }
                else
                {
                    Assert.Fail("Could not close the driver");
                }
            }

        }

        iPhone ParseTargtDevice(string text)
        {
            iPhone iphone;
            if (!Enum.TryParse<iPhone>(text, out iphone))
            {
                iphone = iPhone.non_recogised;
            }
            return iphone;
        }

    }
}
