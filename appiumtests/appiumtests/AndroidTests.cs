﻿using System;
using System.IO;
using NUnit.Framework;
using OpenCvSharp;
using OpenCvSharp.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.Interfaces;
using OpenQA.Selenium.Appium.MultiTouch;
using OpenQA.Selenium.Appium.Service;
using OpenQA.Selenium.Appium.Service.Options;
using OpenQA.Selenium.Support.UI;
using Tesseract;

namespace appiumtests
{

    public class AndroidTests
    {
        //Set up Appium session on device
        public Phone phone;
        public string rootPath;
        public int warpSteps = 10;
        public bool saveSuccessScreens = true;

        int serverRetryCount = 0;
        int serverRetryLimit = 3;

        ///Appium variables
        AppiumLocalService server;
        AndroidDriver<AndroidElement> driver;
        AppiumOptions cap;

        ScreenshotActions screenshotActions;
        Detector detector;
        Touch touchInput;
        Confirmer confirmer;

        bool initialised = false;


        string firstCode = "ljo0s7";
        Key[] firstMagicCode = new Key[] { Key.L, Key.J, Key.O, Key.Zero, Key.S, Key.Seven };
        string secondCode = "toevg6";
        Key[] secondMagicCode = new Key[] { Key.T, Key.O, Key.E, Key.V, Key.G, Key.Six };
        string thirdCode = "makuy2";
        Key[] thirdMagicCode = new Key[] { Key.M, Key.A, Key.K, Key.U, Key.Y, Key.Two };
        string Test4kCode = "zdr1xa";
        Key[] Test4kMagicCode = new Key[] { Key.Z, Key.D, Key.R, Key.One, Key.X, Key.A };
        string Test8kCode = "vjuzpf";
        Key[] Test8kMagicCode = new Key[] { Key.V, Key.J, Key.U, Key.Z, Key.P, Key.F };
        string Test8kSphericCode = "j8cm9d";
        Key[] Test8kSphericMagicCode = new Key[] { Key.J, Key.Eight, Key.C, Key.M, Key.Nine, Key.D };


        public void InitializeParams()
        {
            var target = TestContext.Parameters.Get("Device");
            phone = ParseTargtDevice(target);
            rootPath = TestContext.Parameters.Get("Rootpath");
            var warpSteps = TestContext.Parameters.Get("WarpSteps");
            Console.WriteLine("Targeted device: " + phone + " Root path: " + rootPath);
            if (!int.TryParse(warpSteps, out this.warpSteps) || warpSteps.Equals(""))
            {
                this.warpSteps = 10;
                Console.WriteLine("Warp steps set to default: " + warpSteps);
            }
            else
            {
                Console.WriteLine("Warp steps set to: " + warpSteps);
            }
            Assert.AreNotEqual(Phone.non_recogised, phone, "Target device was not recogised");
            Assert.IsFalse((!Directory.Exists(rootPath) || !Directory.Exists(rootPath + "AppiumTestsOutput/") || !Directory.Exists(rootPath + "AppiumTests/")), "Project path setup is incorrect");
            initialised = true;
        }


        public bool FindOrStartAppiumServer()
        {
            if(server!=null && server.IsRunning)
            {
                serverRetryCount = 0;
                return true;
            }
            try
            {
                
                Console.WriteLine("Building new server service");
                //AppiumServiceBuilder serverBuilder = new AppiumServiceBuilder().WithIPAddress("127.0.0.1").UsingPort(4723).WithAppiumJS(new FileInfo("C:/Users/theC/node_modules/appium/build/lib/main.js"));
                AppiumServiceBuilder serverBuilder = new AppiumServiceBuilder().WithIPAddress("127.0.0.1").UsingAnyFreePort().WithAppiumJS(new FileInfo("C:/Users/theC/node_modules/appium/build/lib/main.js")).WithLogFile(new FileInfo("C:/Users/theC/Desktop/appium_log.txt"));
                server = serverBuilder.Build();
                server.Start();
                Console.WriteLine("Server started on: " + server.ServiceUrl);
                if (server.IsRunning)
                {
                    serverRetryCount = 0;
                    return true;
                }
                else
                {
                    Console.WriteLine("Server not running...");
                    if (serverRetryCount >= serverRetryLimit)
                    {
                        Console.WriteLine("Failed to start server in " + serverRetryCount + " retries");
                        return false;
                    }
                    serverRetryCount++;
                    FindOrStartAppiumServer();
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Error staring appium server! "  + e.Message);
                if(serverRetryCount >= serverRetryLimit)
                {
                    Console.WriteLine("Failed to start server in " + serverRetryCount + " retries");
                    return false;
                }
                serverRetryCount++;
                FindOrStartAppiumServer();
            }
            return false;
        }

        public void SetDebugParams()
        {
            phone = Phone.SmGalaxyS7edge;
            rootPath = "D:/praktyki/";
        }

        [SetUp]
        public void SetUp()
        {
            //SetDebugParams();
            if(!initialised) { InitializeParams(); }
            FindOrStartAppiumServer();
            SetUpCaps(phone);
            //driver = new AndroidDriver<AndroidElement>(new Uri("http://127.0.0.1:4723/wd/hub"), cap);
            driver = new AndroidDriver<AndroidElement>(server, cap);
            string sessionID = driver.SessionId.ToString();
            Console.WriteLine("Session id: " + sessionID);

            screenshotActions = new ScreenshotActions(driver, rootPath);
            detector = new Detector(screenshotActions);
            touchInput = new Touch(driver);
            confirmer = new Confirmer(detector, screenshotActions, touchInput, true, rootPath, saveSuccessScreens);
        }

        [TearDown]
        public void Close()
        {
            Console.WriteLine("Closing app");
            driver.CloseApp();
            driver.PressKeyCode(AndroidKeyCode.Keycode_HOME);
            driver.PressKeyCode(AndroidKeyCode.Keycode_APP_SWITCH);
            System.Threading.Thread.Sleep(1000);
            try
            {
                AndroidElement closeApps = driver.FindElementByAccessibilityId("Close all"); 
                closeApps.Click();
            }
            catch (Exception e)
            {
                Console.WriteLine("Closing all apps failed!");
                driver.PressKeyCode(AndroidKeyCode.Keycode_APP_SWITCH);
            }
            driver.Quit();
        }

        [Test, Order(1)]
        public void BasicTest()
        {
            UnlockPhone();
            driver.ResetApp();
            System.Threading.Thread.Sleep(1000);
            screenshotActions.SetScreenshotRatio(touchInput);
            Console.WriteLine("LaunchApp Context: " + driver.CurrentActivity);
            System.Threading.Thread.Sleep(5000);
            Console.WriteLine("1s Context: " + driver.CurrentActivity);
            Assert.AreEqual(ScreenOrientation.Landscape, driver.Orientation, "Incorrect screen orientation");
        }
        
        [Test, Order(2)]
        public void GoIntoTour()
        {
            UnlockPhone();
            driver.ResetApp();
            System.Threading.Thread.Sleep(1000);
            Assert.AreEqual(ScreenOrientation.Landscape, driver.Orientation);
            screenshotActions.screenshotPrefix = "GoIntoVestry70";
            screenshotActions.SetScreenshotRatio(touchInput);
            bool found = GrantAllows();
            Assert.IsTrue(found, "Permission allow button was not found!");
            found = detector.SeekTemplate(Element.NoButton, 10, 1000, true, saveSuccessScreens);
            Assert.IsTrue(found, "NoButton from VRModeAsk screen was not found!");
            touchInput.TapAtPoint(detector.centerPoint);   //two taps for focus
            touchInput.TapAtPoint(detector.centerPoint);
            found = detector.SeekTemplate(Element.SkipButton, 10, 1000, true, saveSuccessScreens);
            Assert.IsTrue(found, "Skip login button was not found!");
            touchInput.TapAtPoint(detector.centerPoint);
            found = confirmer.ConfirmHUD();
            Assert.IsTrue(found, "HUD was not found!");
            confirmer.SkipTutorial();
            found = detector.SeekTemplate(Element.GyroLockButton, 3, 1000, true, saveSuccessScreens);
            Assert.IsTrue(found, "GyroLock button not found!");
            touchInput.TapAtPoint(detector.centerPoint);
            Point gyroButtonPoint = detector.centerPoint;
            bool downloading = confirmer.WaitForDownload(gyroButtonPoint);
            Assert.IsFalse(downloading, "Downloading still in progress (timeout)");
            System.Threading.Thread.Sleep(500);
            found = detector.SeekThumbnail(ThumbnailTemplate.Vestry70, 3, 1000, saveSuccessScreens);
            Assert.IsTrue(found, "Thumbnail of 70Vestry tour was not found");
            touchInput.TapAtPoint(detector.centerPoint);
            touchInput.TapAtPoint(detector.centerPoint);
            System.Threading.Thread.Sleep(1500);
            found = confirmer.ConfirmVestry70();
            Assert.IsTrue(found, "70Vestry tour's landing pano not confirmed!");
            Console.WriteLine("Completed");
            Assert.Pass();
        }

        [Test, Order(3)]
        public void MagicCode()
        {
            UnlockPhone();
            driver.ResetApp();
            Assert.AreEqual(ScreenOrientation.Landscape, driver.Orientation);
            screenshotActions.screenshotPrefix = "MagicCode";
            screenshotActions.SetScreenshotRatio(touchInput);
            bool found = GrantAllows();
            Assert.IsTrue(found, "Permission allow button was not found!");
            found = detector.SeekTemplate(Element.NoButton, 10, 1000, true, saveSuccessScreens);
            Assert.IsTrue(found, "NoButton from VRModeAsk screen was not found!");
            touchInput.TapAtPoint(detector.centerPoint);   //for some reason this screen requries two taps
            touchInput.TapAtPoint(detector.centerPoint);
            found = detector.SeekTemplate(Element.SignInMagicCode, 10, 1000, true, saveSuccessScreens);
            Assert.IsTrue(found, "MagicCode on login screen was not found!");
            touchInput.TapAtPoint(detector.centerPoint);
            found = detector.SeekTemplate(Element.KeyboardPlay, 3, 200, true, saveSuccessScreens, true, 0.01f);
            Point keyboardPlayButton = detector.centerPoint;
            Assert.IsTrue(found, "Keyboard play key was not found!");
            found = confirmer.FindAndTapCode(new Key[] { Key.L, Key.J, Key.O, Key.Zero, Key.S, Key.Seven }); 
            Assert.IsTrue(found, "Passing magic code failed!");
            touchInput.TapAtPoint(keyboardPlayButton);
            System.Threading.Thread.Sleep(500);
            bool keyboardPresent = confirmer.CheckKeyboard(firstCode);
            if (keyboardPresent)
            {
                if (confirmer.greenCode)
                {
                    Console.WriteLine("Keyboard present, first magic code is correct. Waitnig for tour to load.");
                }
                else
                {
                    System.Threading.Thread.Sleep(1000);
                    confirmer.CheckKeyboard(firstCode);
                    if (keyboardPresent && !confirmer.greenCode)
                    {
                        Assert.Fail("First magic code validation failed");
                    }   
                }
            }
            else
            {
                Console.WriteLine("Keyboard not present. Assuming that first magic code has been correct.");
            }
            confirmer.greenCode = false;
            bool confirm = confirmer.ConfirmEnterFirstMagicCode();
            Assert.IsTrue(confirm, "Could not confirm Test Tour!");
            System.Threading.Thread.Sleep(200);
            Point tapPoint = new Point(10, 10);
            touchInput.DoubleTapAtPoint(tapPoint);
            System.Threading.Thread.Sleep(1000);
            confirm = confirmer.ConfirmHUDWithText(3, 500);
            Assert.IsTrue(confirm, "HUD was not found (text)");
            found = detector.SeekTemplate(Element.GyroLockButton, 5, 200, true, saveSuccessScreens);
            Assert.IsTrue(found, "GyroLock button not found");
            touchInput.TapAtPoint(detector.centerPoint);
            found = detector.SeekTemplate(Element.MagicCodeButton, 5, 200, true, saveSuccessScreens);
            Assert.IsTrue(found, "AddMagicCode button was not found");
            touchInput.TapAtPoint(detector.centerPoint);
            found = detector.SeekTemplate(Element.KeyboardPlay, 3, 200, true, true, true, 0.01f);
            Assert.IsTrue(found, "Keyborad play button was not found");
            keyboardPlayButton = detector.centerPoint;
            found = confirmer.FindAndTapCode(new Key[] { Key.T, Key.O, Key.E, Key.V, Key.G, Key.Six }); 
            Assert.IsTrue(found, "Passing magic code failed!");
            touchInput.TapAtPoint(keyboardPlayButton);
            System.Threading.Thread.Sleep(500);
            keyboardPresent = confirmer.CheckKeyboard(secondCode);
            if (keyboardPresent)
            {
                if (confirmer.greenCode)
                {
                    Console.WriteLine("Keyboard present, second magic code is correct. Waitnig for tour to load.");
                }
                else
                {
                    System.Threading.Thread.Sleep(1000);
                    keyboardPresent = confirmer.CheckKeyboard(secondCode);
                    if(keyboardPresent && !confirmer.greenCode)
                    {
                        Assert.Fail("Second magic code validation failed");
                    }
                }
            }
            else
            {
                Console.WriteLine("Keyboard not present. Assuming that second magic code has been correct.");
            }

            confirm = confirmer.ConfirmEnterSecondMagicCode();
            screenshotActions.LoadNewScreenshot();
            Assert.IsTrue(confirm, "Could not confirm Test Tour 2!");
            System.Threading.Thread.Sleep(1000);
            Console.WriteLine("Complete");
        }

        [Test, Order(11)]
        public void SharedExperience()  
        {
            UnlockPhone();
            driver.ResetApp();
            Assert.AreEqual(ScreenOrientation.Landscape, driver.Orientation);
            screenshotActions.screenshotPrefix = "SharedExperience";
            screenshotActions.SetScreenshotRatio(touchInput);
            bool found = GrantAllows();
            Assert.IsTrue(found, "Permission allow button was not found!");
            found = detector.SeekTemplate(Element.NoButton, 10, 1000, true, saveSuccessScreens);
            Assert.IsTrue(found, "NoButton from VRModeAsk screen was not found!");
            touchInput.TapAtPoint(detector.centerPoint);   //for some reason this screen requries two taps
            touchInput.TapAtPoint(detector.centerPoint);
            found = detector.SeekTemplate(Element.SignInMagicCode, 10, 1000, true, saveSuccessScreens);
            Assert.IsTrue(found, "MagicCode on login screen was not found!");
            touchInput.TapAtPoint(detector.centerPoint);
            found = detector.SeekTemplate(Element.KeyboardPlay, 3, 200, true, saveSuccessScreens, true, 0.01f);
            Point keyboardPlayButton = detector.centerPoint;
            Assert.IsTrue(found, "Keyboard play key was not found!");
            found = confirmer.FindAndTapCode(new Key[] { Key.L, Key.J, Key.O, Key.Zero, Key.S, Key.Seven });
            Assert.IsTrue(found, "Passing magic code failed!");
            touchInput.TapAtPoint(keyboardPlayButton);
            System.Threading.Thread.Sleep(500);
            bool keyboardPresent = confirmer.CheckKeyboard(firstCode);
            if (keyboardPresent)
            {
                if (confirmer.greenCode)
                {
                    Console.WriteLine("Keyboard present, first magic code is correct. Waitnig for tour to load.");
                }
                else
                {
                    System.Threading.Thread.Sleep(1000);
                    confirmer.CheckKeyboard(firstCode);
                    if (keyboardPresent && !confirmer.greenCode)
                    {
                        Assert.Fail("First magic code validation failed");
                    }
                }
            }
            else
            {
                Console.WriteLine("Keyboard not present. Assuming that first magic code has been correct.");
            }
            confirmer.greenCode = false;
            bool confirm = confirmer.ConfirmEnterFirstMagicCode();
            Assert.IsTrue(confirm, "Could not confirm Test Tour!");
            //Show HUD
            Point screenCenter = new Point(screenshotActions.screenshot.Width / 2, screenshotActions.screenshot.Height / 2);
            System.Threading.Thread.Sleep(200);
            Console.WriteLine("Show HUD");
            Point tapPoint = new Point(screenCenter.X, 20);
            touchInput.DoubleTapAtPoint(tapPoint);
            System.Threading.Thread.Sleep(1000);
            confirm = confirmer.ConfirmHUDWithText(3, 500);
            Assert.IsTrue(confirm, "HUD was not found (text)");
            found = detector.SeekTemplate(Element.GyroLockButton, 5, 200, true, saveSuccessScreens);
            Assert.IsTrue(found, "GyroLock button not found");
            touchInput.TapAtPoint(detector.centerPoint);
            //Validate Takeover button - log if host or not
            screenshotActions.LoadNewScreenshot();
            System.Threading.Thread.Sleep(1000);
            //Hide HUD
            Console.WriteLine("Hide HUD");
            touchInput.DoubleTapAtPoint(tapPoint);
            System.Threading.Thread.Sleep(1000);
            confirm = confirmer.ConfirmHUDWithText(3, 500);
            Assert.IsFalse(confirm, "HUD still presenet");
            //double tap to leave mark -cut screen 200x200 pix at center, double tap at center, cut screen 200x200 pix at center, compare cuts
            Mat beforeTap = screenshotActions.TakeCutScreenshot(screenCenter, 200, 200);
            touchInput.SlowDoubleTapAtPoint(screenCenter);
            Mat afterTap = screenshotActions.TakeCutScreenshot(screenCenter, 200, 200);
            Mat gray = afterTap.CvtColor(ColorConversionCodes.BGR2GRAY);
            CircleSegment[] circleSegments = Cv2.HoughCircles(gray, HoughMethods.Gradient, 10, 10, 300, 100, 5, 50);
            if(circleSegments.Length > 0)
            {
                for(int i =0; i < circleSegments.Length; i++)
                {
                    float radius = circleSegments[i].Radius;
                    if(radius!=0)
                    {
                        Point center = new Point(circleSegments[i].Center.X, circleSegments[i].Center.Y);
                        Console.WriteLine("Found circle after tap: center [" + center + "] radius [" + radius);
                        Cv2.Circle(afterTap, center, (int)radius, Scalar.Black);
                        screenshotActions.SaveResultScreenshot(afterTap, "mark", true);
                    }
                }
            }
            else
            {
                screenshotActions.SaveResultScreenshot(afterTap, "mark", false);
                Assert.Fail("Double tap did not leave mark");
            }
            Console.WriteLine("Complete");
        }

        [Test, Order(4)]
        public void PirateModeCube()
        {
            EnterMagicCodePart(false, "PirateModeCube", firstMagicCode);
            bool confirm = confirmer.ConfirmEnterFirstMagicCode();
            Assert.IsTrue(confirm, "Could not confirm Test Tour!");
            Point tapPoint = new Point(screenshotActions.screenshot.Width / 2, screenshotActions.screenshot.Height / 2);
            touchInput.DoubleTapAtPoint(tapPoint);
            System.Threading.Thread.Sleep(1000);
            confirm = confirmer.ConfirmHUDWithText(3, 500);
            Assert.IsTrue(confirm, "HUD was not found (text)");
            confirm = confirmer.ConfirmFirstTestTourHUD();
            Assert.IsTrue(confirm, "Bad HUD screen");
        }

        [Test, Order(5)]
        public void PirateModeSpheric()
        {
            EnterMagicCodePart(false, "PirateModeSpheric", thirdMagicCode);
            bool confirm = confirmer.ConfirmEnterThirdMagicCode();
            Assert.IsTrue(confirm, "Could not confirm Test Tour3!");
            Point tapPoint = new Point(screenshotActions.screenshot.Width / 2, screenshotActions.screenshot.Height / 2);
            touchInput.DoubleTapAtPoint(tapPoint);
            System.Threading.Thread.Sleep(1000);
            confirm = confirmer.ConfirmHUDWithText(3, 500);
            Assert.IsTrue(confirm, "HUD was not found (text)");
            confirm = confirmer.ConfirmThirdTestTourHUD();
            Assert.IsTrue(confirm, "Bad HUD screen");
        }

        [Test, Order(6)]
        public void VRModeCube()
        {
            EnterMagicCodePart(true, "VRModeCube", firstMagicCode);
            bool confirmed = false;
            for (int i = 0; i < 10; i++)
            {
                confirmed = confirmer.ConfirmVRModeTestTour(false);
                if (confirmed)
                {
                    break;
                }
                Console.WriteLine("Attempt: " + i + " no confirm");
                System.Threading.Thread.Sleep(1000);
            }
            if (!confirmed)
            {
                screenshotActions.SaveResultScreenshot(screenshotActions.screenshot, "TestTour_VRhistogram", false);
            }
            Assert.IsTrue(confirmed, "VRMode in TestTour not confirmed");
            Point tapPoint = new Point(screenshotActions.screenshot.Width / 4, screenshotActions.screenshot.Height / 2);
            touchInput.DoubleTapAtPoint(tapPoint);
            System.Threading.Thread.Sleep(1000);
            confirmed = confirmer.ConfirmVRModeTestTour(true);
            if (!confirmed)
            {
                screenshotActions.SaveResultScreenshot(screenshotActions.screenshot, "TestTour_VRhistogram_HUD", false);
            }
            Assert.IsTrue(confirmed, "VRMode in TestTour not confirmed");
        }

        [Test, Order(7)]
        public void VRModeSpheric()
        {
            EnterMagicCodePart(true, "VRModeSpheric", thirdMagicCode);
            bool confirmed = false;
            for (int i = 0; i < 3; i++)
            {
                confirmed = confirmer.ConfirmVRModeTestTour3(false);
                if (confirmed)
                {
                    break;
                }
                Console.WriteLine("Attempt: " + i + " no confirm");
                System.Threading.Thread.Sleep(1000);
            }
            if (!confirmed)
            {
                screenshotActions.SaveResultScreenshot(screenshotActions.screenshot, "TestTour3_VRhistogram", false);
            }
            Assert.IsTrue(confirmed, "VRMode in Test Tour 3 not confirmed");
            Point tapPoint = new Point(screenshotActions.screenshot.Width / 4, screenshotActions.screenshot.Height / 2);
            touchInput.DoubleTapAtPoint(tapPoint);
            System.Threading.Thread.Sleep(1000);
            screenshotActions.LoadNewScreenshot();
            confirmed = confirmer.ConfirmVRModeTestTour3(true);
            if (!confirmed)
            {
                screenshotActions.SaveResultScreenshot(screenshotActions.screenshot, "TestTour3_VRhistogram_HUD", false);
            }
            Assert.IsTrue(confirmed, "VRMode in TestTour not confirmed");
        }

        [Test, Order(8)]
        public void FloorplanJPG()
        {
            EnterMagicCodePart(false, "Floorplan_", Test4kMagicCode);
            bool confirmed = confirmer.ConfirmRedBlueSpheric(false,true, 10, 1000);
            Assert.IsTrue(confirmed, "Could not confirm entering Test 4k landing pano");
            screenshotActions.LoadNewScreenshot();
            confirmed = detector.SeekTemplate(Element.RedFloorplan, 3, 200, true, saveSuccessScreens);
            Assert.IsTrue(confirmed, "Floorplan icon not found");
            touchInput.TapAtPoint(detector.centerPoint);
            confirmed = detector.SeekTemplate(Element.DoggoFloorplan, 3, 500, true,saveSuccessScreens, true, 0.05f);
            Assert.IsTrue(confirmed, "Floorplan image not found in view");
        }

        [Test, Order(9)]
        public void FloorplanPNG()
        {
            EnterMagicCodePart(false, "Floorplan_", Test8kMagicCode);
            bool confirmed = confirmer.ConfirmCyanYellowSpheric(false, true, 10, 1000);
            Assert.IsTrue(confirmed, "Could not confirm entering Test 8k landing pano");
            screenshotActions.LoadNewScreenshot();
            confirmed = detector.SeekTemplate(Element.CyanFloorplan, 3, 200, true, saveSuccessScreens);
            Assert.IsTrue(confirmed, "Floorplan icon not found");
            touchInput.TapAtPoint(detector.centerPoint);
            confirmed = detector.SeekTemplate(Element.DoggoPNGFloorplan, 3, 500, true,saveSuccessScreens, true, 0.05f);
            Assert.IsTrue(confirmed, "Floorplan image not found in view");
        }

        [Test, Order(10)]
        public void WarpWalk()
        {
            EnterMagicCodePart(false, "Warp_", Test8kSphericMagicCode);
            bool confirmed = confirmer.ConfirmRedBlueSpheric(false, false, 10, 1000);
            Assert.IsTrue(confirmed, "Could not confirm entering Test 8k Spheric RedBlue spheric pano");
            bool onRed = true;
            Console.WriteLine("Show HUD");
            Point tapPoint = new Point(screenshotActions.screenshot.Width / 2, 20);
            touchInput.DoubleTapAtPoint(tapPoint);
            confirmed = detector.SeekTemplate(Element.GyroLockButton, 5, 200, true, saveSuccessScreens);
            Assert.IsTrue(confirmed, "GyroLock button not found");
            touchInput.TapAtPoint(detector.centerPoint);
            Console.WriteLine("Hide HUD");
            touchInput.DoubleTapAtPoint(tapPoint);
            System.Threading.Thread.Sleep(1000);
            confirmed = confirmer.ConfirmHUDWithText(3, 500, true);
            Assert.IsFalse(confirmed, "HUD still presenet");
            OpenCvSharp.Rect leftHalf = new OpenCvSharp.Rect(screenshotActions.screenshot.Width / 2, screenshotActions.screenshot.Height / 2, screenshotActions.screenshot.Width / 2, screenshotActions.screenshot.Height / 2);
            for(int i=0; i < warpSteps; i++)
            {
                Console.WriteLine("Warp step: " + i + "/" + warpSteps);
                confirmed = detector.FindWarp(saveSuccessScreens);
                Assert.IsTrue(confirmed, "Did not found warp");
                touchInput.TapAtPoint(detector.centerPoint);
                if(onRed)
                {
                    confirmed = confirmer.ConfirmCyanYellowSpheric(false, false, 10, 1000);
                    Assert.IsTrue(confirmed, "Could not confirm entering Test 8k Spheric CyanYellow spheric pano");
                    onRed = false;
                }
                else
                {
                    confirmed = confirmer.ConfirmRedBlueSpheric(false, false, 10, 1000);
                    Assert.IsTrue(confirmed, "Could not confirm entering Test 8k Spheric RedBlue spheric pano");
                    onRed = true;
                }
            }
        }


        //------------------------NON-TESTS------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        void UnlockPhone()
        {
            if (driver.IsLocked())
            {
                driver.Unlock();
                System.Threading.Thread.Sleep(30);
                Console.WriteLine("Device unlock context: " + driver.CurrentActivity);
                touchInput.UnlockSwipe();
                if (phone==Phone.SmGalaxyS7edge)
                {
                    System.Threading.Thread.Sleep(30);
                    UnlockCode();
                }
            }
            else
            {
                Console.WriteLine("Device already unlocked - context: " + driver.CurrentActivity);
            }
        }

        /// <summary>
        /// Seeks Allow button and presses five times
        /// </summary>
        /// <returns></returns>
        bool GrantAllows()
        {
            bool found = detector.SeekTemplate(Element.AllowButton, 10, 1000, true, saveSuccessScreens);
            if (found)
            {
                for (int i = 0; i < 5; i++)
                {
                    touchInput.TapAtPoint(detector.centerPoint);
                }
            }
            return found;
        }

        void UnlockCode()
        {
            for (int i = 1; i < 7; i++)
            {
                AndroidElement codeKey = driver.FindElementByAccessibilityId(i.ToString());
                codeKey.Click();
                System.Threading.Thread.Sleep(30);
            }
            AndroidElement ok = driver.FindElementByAccessibilityId("OK");
            ok.Click();
        }

        void EnterMagicCodePart(bool VRMode, string screenshotPrefix, Key[] magicCode)
        {
            screenshotActions.screenshotPrefix = screenshotPrefix;
            UnlockPhone();
            driver.ResetApp();
            Assert.AreEqual(ScreenOrientation.Landscape, driver.Orientation);
            screenshotActions.SetScreenshotRatio(touchInput);
            Element answer = VRMode ? Element.YesButton : Element.NoButton;
            GrantAllows();
            bool found = detector.SeekTemplate(answer, 10, 1000, true, saveSuccessScreens);
            Assert.IsTrue(found, answer + " from VRModeAsk screen was not found!");
            touchInput.TapAtPoint(detector.centerPoint);   //for some reason this screen requries two taps
            touchInput.TapAtPoint(detector.centerPoint);
            found = detector.SeekTemplate(Element.SignInMagicCode, 10, 1000, true, saveSuccessScreens);
            Assert.IsTrue(found, "MagicCode on login screen was not found!");
            touchInput.TapAtPoint(detector.centerPoint);
            found = detector.SeekTemplate(Element.KeyboardPlay, 3, 200, true, saveSuccessScreens, true, 0.01f);
            Point keyboardPlayButton = detector.centerPoint;
            Assert.IsTrue(found, "Keyboard play key was not found!");
            found = confirmer.FindAndTapCode(magicCode);
            Assert.IsTrue(found, "Passing magic code failed!");
            System.Threading.Thread.Sleep(100);
            touchInput.TapAtPoint(keyboardPlayButton);
            System.Threading.Thread.Sleep(500);
            bool keyboardPresent = confirmer.CheckKeyboard(firstCode);
            if (keyboardPresent)
            {
                if (confirmer.greenCode)
                { Console.WriteLine("Keyboard present, first magic code is correct. Waitnig for tour to load."); }
                else
                { Assert.Fail("First magic code validation failed"); }
            }
            else
            { Console.WriteLine("Keyboard not present. Assuming that first magic code has been correct."); }
            confirmer.greenCode = false;
        }

        void SetUpCaps(Phone phone)
        {
            cap = new AppiumOptions();
            cap.AddAdditionalCapability("automationName", "uiautomator2");
            cap.AddAdditionalCapability("platformName", "Android");
            cap.AddAdditionalCapability("appPackage", "co.theConstruct.theViewer.CBD");
            cap.AddAdditionalCapability("appActivity", "co.theconstructutils.viewerplugin.ViewerActivity");
            cap.AddAdditionalCapability("newCommandTimeout", "60");
            cap.AddAdditionalCapability("autoLaunch", false);
            cap.AddAdditionalCapability("skipUnlock", true);

            switch((int)phone)
            {
                case 0:
                    cap.AddAdditionalCapability("deviceName", "Galaxy S6");
                    cap.AddAdditionalCapability("udid", "06157df6b8718307");
                    cap.AddAdditionalCapability("platformVersion", "7.0");
                    break;
                case 1:
                    cap.AddAdditionalCapability("deviceName", "Samsung Galaxy S7 edge");
                    cap.AddAdditionalCapability("udid", "e4381d5e");
                    cap.AddAdditionalCapability("platformVersion", "8.0");
                    break;
            }
            
        }


        Phone ParseTargtDevice(string text)
        {
            Phone phone;
            if(!Enum.TryParse<Phone>(text, out phone))
            {
                phone = Phone.non_recogised;
            }
            return phone;
        }

       

    }
}
