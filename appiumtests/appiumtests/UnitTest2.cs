﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using OpenCvSharp;
using OpenCvSharp.XFeatures2D;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.Interfaces;
using OpenQA.Selenium.Appium.MultiTouch;
using System.Drawing;
using Tesseract;
using OpenCvSharp.Extensions;

namespace appiumtests
{
    /*
    //Stubbed screenshot tests
    public class UnitTest2
    {
        string templatesPath = "D:/praktyki/AppiumTestsOutput/templates/";
        string stubsPath = "D:/praktyki/AppiumTestsOutput/StubTests/stubs/";
        string resultsPath = "D:/praktyki/AppiumTestsOutput/StubTests/results/";

        Mat screenshot;
        Mat template;
        Mat thumbnail;

        OpenCvSharp.Size defaultScreenshotSize = new OpenCvSharp.Size(2560, 1140);
        OpenCvSharp.Size defaultNoButtonSize = new OpenCvSharp.Size();
        OpenCvSharp.Size currentScreenshotSize;
        bool needScaling = false;
        Template currentTemplate;
        ThumbnailTemplate currentThumbnail;

        OpenCvSharp.Point tapPoint;
        [SetUp]
        public void SetUp()
        {
            Console.WriteLine("Stubbed screenshot test started");
        }


        [Test]
        public void DetectNoOnStub()
        {
            LoadStub(Stub.VRModeAsk);
            bool found = SeekTemplate(Template.NoButton, 3, 1000);
            Assert.IsTrue(found);
        }

        [Test]
        public void FailToDetectNoOnStub()
        {
            LoadStub(Stub.VRModeAskDirty);
            bool found = SeekTemplate(Template.NoButton, 3, 1000);
            Assert.IsFalse(found);
        }

        [Test]
        public void DetectSkipButtonOnStub()
        {
            LoadStub(Stub.LoginPickScreen);
            bool found = SeekTemplate(Template.SkipButton, 3, 1000);
            Assert.IsTrue(found);
        }

        [Test]
        public void DetectHUDLogo()
        {
            LoadStub(Stub.CaveHUDFull);
            bool found = SeekTemplate(Template.HUDLogo, 3, 1000);
            Assert.IsTrue(found);
        }

        [Test]
        public void DetectNoButtonScaleTest()
        {
            LoadStub(Stub.VRModeAsk);
            Console.WriteLine("Screenshot size: " + screenshot.Size());
            LoadTemplate(Template.NoButton);
            Console.WriteLine("Template size: " + template.Size());
            bool found = SeekTemplate(Template.NoButton, 3, 1000);
            Assert.IsTrue(found);
        }

        [Test]
        public void FilteringTest()
        {
            LoadStub(Stub.CaveHUDFull);
            Mat filterScreenshot = FilterThreshold(screenshot);
            Console.WriteLine("Filtered screenshot channels: " + filterScreenshot.Channels());
            Cv2.ImShow("Filtered", filterScreenshot);
            Cv2.WaitKey();
        }

        [Test]
        public void ConfirmHUDTest()
        {
            bool found = false;
            LoadStub(Stub.CaveHUDFull);
            found = SeekTemplate(Template.HUDLogo, 3, 300);
            Assert.IsTrue(found);
            Console.WriteLine("Found " + currentTemplate);
            found = SeekTemplate(Template.VRToursHeader, 3, 300);
            Assert.IsTrue(found);
            Console.WriteLine("Found " + currentTemplate);
            found = SeekThumbnail(ThumbnailTemplate.Vestry70,3,300);
            Assert.IsTrue(found);
            Console.WriteLine("Found " + currentThumbnail);
        }


        [Test]
        public void MaskCreator()
        {
            Mat mask_input = Cv2.ImRead(templatesPath + "mask_22.png");
            Mat mask_filter = FilterThreshold(mask_input);
            Mat mask_output = new Mat();
            mask_output.Create(mask_filter.Size(),MatType.CV_32FC1);
            Cv2.BitwiseNot(mask_filter, mask_output);
            Cv2.ImWrite(templatesPath + "mask_22.png", mask_output);
        }

        [Test]
        public void DetectWithMaskTest()
        {
            LoadStub(Stub.CaveHUDFull);
            bool found = SeekTemplateWithMask(Template.HUDLogo, 3, 300);
            Assert.IsTrue(found);
        }


        [Test]
        public void MaskCutTest()
        {
            LoadTemplate(Template.HUDLogo);
            Mat mask = LoadMask(currentTemplate);
            Mat result = new Mat();
            Cv2.Canny(mask, result, 50, 200);
            Cv2.ImShow("result", mask);
        }

        [Test]
        public void FindHUDLogoCroocked()
        {
            LoadStub(Stub.CaveHUDCrooked);
            bool found = SeekTemplate(Template.HUDLogo, 3, 100);
            Assert.IsTrue(found);
        }


        [Test]
        public void DownloadingLabelTest()
        {
            LoadStub(Stub.CaveHUDFull);
            bool found = SeekTemplateWithFilter(Template.DownloadingLabel, 3, 100);
            Assert.IsTrue(found);
        }

        [Test]
        public void DownoladingThreshTest()
        {
            LoadTemplate(Template.DownloadingLabel);
            Mat filter_templ = FilterNonWhite(template);
            Cv2.ImShow("filter_templ", filter_templ);
            LoadStub(Stub.CaveHUDCrooked);
            Mat filter_scr = FilterNonWhite(screenshot);
            Cv2.ImShow("filter_scr", filter_scr);
            Cv2.WaitKey();
            Bitmap bitmap = BitmapConverter.ToBitmap(filter_templ);

            TesseractEngine engine = new TesseractEngine("D:/praktyki/AppiumTests/appiumtests/appiumtests/tessdata", "eng");
            BitmapToPixConverter conv = new BitmapToPixConverter();
            Pix img = conv.Convert(bitmap);
            Page page = engine.Process(img);
            Tesseract.Rect roi = page.RegionOfInterest;

            string text = page.GetText();
            Console.WriteLine("Read text: " + text);
        }



        //-----------------------------Not right now-----------------------------------



        [Test]
        public void FeatureMatchingTest()
        {
            LoadStub(Stub.SplashScreen); //Mat screenshot
            LoadTemplate(Template.HUDLogo); //Mat template
            Mat grayS = screenshot.CvtColor(ColorConversionCodes.BGR2GRAY);
            Mat grayT = template.CvtColor(ColorConversionCodes.BGR2GRAY);
            SURF surf = SURF.Create(500, 4, 2, true);

            KeyPoint[] keypoints1, keypoints2;
            MatOfFloat descriptors1 = new MatOfFloat();
            MatOfFloat descriptors2 = new MatOfFloat();
            surf.DetectAndCompute(grayT, null, out keypoints1, descriptors1);
            surf.DetectAndCompute(grayS, null, out keypoints2, descriptors2);

            Mat templateKeypoints = grayT;
            for (int i = 0; i < keypoints1.Length; i++)
            {
                Cv2.Circle(templateKeypoints, (OpenCvSharp.Point)keypoints1[i].Pt, 10, Scalar.Red, 3, LineTypes.Link8, 0);
                
            }
            Cv2.ImShow("keypoints", templateKeypoints);

            // Matching descriptor vectors with a brute force matcher
            BFMatcher matcher = new BFMatcher(NormTypes.L2, false);
            DMatch[] matches =  matcher.Match(descriptors1, descriptors2);
            DMatch[][] knn_matches = matcher.KnnMatch(descriptors1, descriptors2, 2);
            Console.WriteLine("Matches: " + matches.Length);

            List<DMatch> filtered_matches = new List<DMatch>();
            float distance_thresh = 0.75f;
            /*
            for (int i = 0; i < matches.Length; i++)
            {
                Console.WriteLine(i + ".  img index: " + matches[i].ImgIdx + " query index: " + matches[i].QueryIdx + " train index: " + matches[i].TrainIdx + " Distance: " + matches[i].Distance);
            }
            
            for (int i = 0; i < knn_matches.Length / 2; i++)
            {
                if (knn_matches[i][0].Distance < distance_thresh * knn_matches[i][1].Distance)
                {
                    Console.WriteLine("Good match!: " + i);
                }
            }

            for (int i = 0; i < knn_matches.Length/2; i++)
            {
                Console.WriteLine(i + ".  1st match img index: " + knn_matches[i][0].ImgIdx + " query index: " + knn_matches[i][0].QueryIdx + " train index: " + knn_matches[i][0].TrainIdx + " Distance: " + knn_matches[i][0].Distance);
                Console.WriteLine(i + ".  2nd match img index: " + knn_matches[i][1].ImgIdx + " query index: " + knn_matches[i][1].QueryIdx + " train index: " + knn_matches[i][1].TrainIdx + " Distance: " + knn_matches[i][1].Distance);
            }

            // Draw matches
            Mat view = new Mat();
            //Cv2.DrawMatches(template, keypoints1, screenshot, keypoints2, matches, view);
            view = grayS;
            for (int i = 0; i < matches.Length; i++)
            {
                Cv2.Circle(view, (OpenCvSharp.Point)keypoints2[knn_matches[i][0].TrainIdx].Pt, 10, Scalar.Red, 3, LineTypes.Link8, 0);
                Cv2.Circle(view, (OpenCvSharp.Point)keypoints2[knn_matches[i][1].TrainIdx].Pt, 10, Scalar.Blue, 3, LineTypes.Link8, 0);
            }
            Cv2.ImShow("view", view);
            Cv2.WaitKey();

        }

        [Test]
        public void FeatureMatchingTestV2()
        {
            LoadStub(Stub.CaveHUDFull); //Mat screenshot
            LoadTemplate(Template.HUDLogo); //Mat template
            //Mat grayS = screenshot.CvtColor(ColorConversionCodes.BGR2GRAY);
            //Mat grayT = template.CvtColor(ColorConversionCodes.BGR2GRAY);
            Mat grayS = FilterThreshold(screenshot);
            Mat grayT = FilterThreshold(template);
            AKAZE orb = AKAZE.Create();

            KeyPoint[] keypoints1, keypoints2;
            MatOfFloat descriptors1 = new MatOfFloat();
            MatOfFloat descriptors2 = new MatOfFloat();
            orb.DetectAndCompute(grayT, null, out keypoints1, descriptors1);
            orb.DetectAndCompute(grayS, null, out keypoints2, descriptors2);

            for (int i = 0; i < keypoints1.Length; i++)
            {
                Cv2.Circle(template, (OpenCvSharp.Point)keypoints1[i].Pt, 3, Scalar.Red, 1, LineTypes.Link8, 0);

            }
            Cv2.ImShow("keypoints", template);

            // Matching descriptor vectors with a brute force matcher
            BFMatcher matcher = new BFMatcher();
            DMatch[] matches = matcher.Match(descriptors1, descriptors2);
            DMatch[][] knn_matches = matcher.KnnMatch(descriptors1, descriptors2, 2);
            Console.WriteLine("Matches: " + matches.Length);

            List<DMatch> filtered_matches = new List<DMatch>();
            float distance_thresh = 0.75f;
            for (int i = 0; i < knn_matches.Length / 2; i++)
            {
                if (knn_matches[i][0].Distance < distance_thresh * knn_matches[i][1].Distance)
                {
                    Console.WriteLine("Good match!: " + i);
                    filtered_matches.Add(knn_matches[i][0]);
                }
            }
            Console.WriteLine("Good matches count: " + filtered_matches.Count);


            for (int i = 0; i < knn_matches.Length / 2; i++)
            {
                Console.WriteLine(i + ".  1st match img index: " + knn_matches[i][0].ImgIdx + " query index: " + knn_matches[i][0].QueryIdx + " train index: " + knn_matches[i][0].TrainIdx + " Distance: " + knn_matches[i][0].Distance);
                Console.WriteLine(i + ".  2nd match img index: " + knn_matches[i][1].ImgIdx + " query index: " + knn_matches[i][1].QueryIdx + " train index: " + knn_matches[i][1].TrainIdx + " Distance: " + knn_matches[i][1].Distance);
            }

            // Draw matches
            Mat view = new Mat();
            //Cv2.DrawMatches(template, keypoints1, screenshot, keypoints2, matches, view);
            view = screenshot;
            for (int i = 0; i < filtered_matches.Count; i++)
            {
                //Cv2.Circle(view, (Point)keypoints2[knn_matches[i][0].TrainIdx].Pt, 10, Scalar.Red, 3, LineTypes.Link8, 0);
                //Cv2.Circle(view, (Point)keypoints2[knn_matches[i][1].TrainIdx].Pt, 10, Scalar.Blue, 3, LineTypes.Link8, 0);
                Cv2.Circle(view, (OpenCvSharp.Point)keypoints2[filtered_matches[i].TrainIdx].Pt, 10, Scalar.Green, 2, LineTypes.Link8, 0);
            }
            Cv2.ImShow("view", view);
            Cv2.WaitKey();

        }

        Mat FilterNonWhite(Mat image)
        {
            Mat result = image.Threshold(200, 255, ThresholdTypes.Tozero);
            return result;
        }



        //-----------------------------Non test methods----------------------------------

        ///image can be screenshot or template
        Mat FilterThreshold(Mat image)
        {
            //Filter screenshot
            Mat[] channels = image.Split();
            channels[0] = channels[0].Threshold(185, 255, ThresholdTypes.Tozero);
            channels[1] = channels[1].Threshold(185, 255, ThresholdTypes.Tozero);
            channels[2] = channels[2].Threshold(190, 255, ThresholdTypes.Tozero);
            Mat result = new Mat();
            Cv2.Merge(channels, result);
            result = result.CvtColor(ColorConversionCodes.BGR2GRAY);
            return result;
        }


        //---------------------------------------------------------------------------

        void LoadStub(Stub stub)
        {
            screenshot = Cv2.ImRead(stubsPath + "stub_" + (int)stub + ".png");
            Console.WriteLine("Loaded stub: " + stub);
        }

        void LoadTemplate(Template template)
        {
            string newTemplateName = "detection_template_" + (int)template + ".png";
            this.template = Cv2.ImRead(templatesPath + newTemplateName);
            currentTemplate = template;
            Console.WriteLine("Loaded detection template " + template + " (" + newTemplateName + ")");
        }

        void LoadThumbnailTemplate(ThumbnailTemplate thumbnailTemplate)
        {
            string newTemplateName = "thumbnail_" + (int)thumbnailTemplate + ".png";
            this.thumbnail = Cv2.ImRead(templatesPath+"Thumbnails/" + newTemplateName);
            currentThumbnail = thumbnailTemplate;
            Console.WriteLine("Loaded detection template " + template + " (" + newTemplateName + ")");
        }

        Mat LoadMask(Template template)
        {
            Mat mask = Cv2.ImRead(templatesPath + "mask_" + (int)template + ".png");
            return mask;
        }

        bool DetectTemplateOnScreenshot()
        {
            Mat result = new Mat();
            int result_cols = screenshot.Cols - template.Cols + 1;
            int result_rows = screenshot.Rows - template.Rows + 1;
            result.Create(MatType.CV_32FC1, result_cols, result_rows);

            Cv2.MatchTemplate(screenshot, template, result, TemplateMatchModes.SqDiffNormed);

            double minVal = 0; double maxVal = 0; OpenCvSharp.Point minLoc = new OpenCvSharp.Point(); OpenCvSharp.Point maxLoc = new OpenCvSharp.Point();
            OpenCvSharp.Point matchLoc;
            Cv2.MinMaxLoc(result, out minVal, out maxVal, out minLoc, out maxLoc);
            matchLoc = minLoc;
            Console.WriteLine("Min val: [" + minVal + "] minLoc: [" + minLoc + "] maxVal: [" + maxVal + "] maxLoc: [" + maxLoc + "]");
            if (minVal <= 0.2)
            {
                /// Mark found template and save screenshot
                Cv2.Rectangle(screenshot, matchLoc, new OpenCvSharp.Point(matchLoc.X + template.Cols, matchLoc.Y + template.Rows), new Scalar(), 2, LineTypes.Link8, 0);
                OpenCvSharp.Point centralMatchPoint = new OpenCvSharp.Point(matchLoc.X + template.Cols / 2, matchLoc.Y + template.Rows / 2);
                tapPoint = centralMatchPoint;
                Cv2.Circle(screenshot, tapPoint, 10, Scalar.Red, 4, LineTypes.Link8, 0);
                Cv2.ImWrite(resultsPath + "result_" + currentTemplate + ".png", screenshot);
                Console.WriteLine("Found match! Central point of found " + currentTemplate + ": " + centralMatchPoint);
                return true;
            }
            return false;
        }

        bool DetectTemplateOnScreenshotWithMask(Mat mask)
        {
            Mat result = new Mat();
            int result_cols = screenshot.Cols - template.Cols + 1;
            int result_rows = screenshot.Rows - template.Rows + 1;
            result.Create(MatType.CV_32FC1, result_cols, result_rows);
            Cv2.MatchTemplate(screenshot, template, result, TemplateMatchModes.SqDiff, mask);
            double minVal = 0; double maxVal = 0; OpenCvSharp.Point minLoc = new OpenCvSharp.Point(); OpenCvSharp.Point maxLoc = new OpenCvSharp.Point();
            OpenCvSharp.Point matchLoc;
            Cv2.MinMaxLoc(result, out minVal, out maxVal, out minLoc, out maxLoc);
            matchLoc = minLoc;
            Console.WriteLine("Min val: [" + minVal + "] minLoc: [" + minLoc + "] maxVal: [" + maxVal + "] maxLoc: [" + maxLoc + "]");
            if (minVal <= 0.2)
            {
                /// Mark found template and save screenshot
                Cv2.Rectangle(screenshot, matchLoc, new OpenCvSharp.Point(matchLoc.X + template.Cols, matchLoc.Y + template.Rows), new Scalar(), 2, LineTypes.Link8, 0);
                OpenCvSharp.Point centralMatchPoint = new OpenCvSharp.Point(matchLoc.X + template.Cols / 2, matchLoc.Y + template.Rows / 2);
                tapPoint = centralMatchPoint;
                Cv2.Circle(screenshot, tapPoint, 10, Scalar.Red, 4, LineTypes.Link8, 0);
                Cv2.ImWrite(resultsPath + "result_" + currentTemplate + ".png", screenshot);
                Console.WriteLine("Found match! Central point of found " + currentTemplate + ": " + centralMatchPoint);
                return true;
            }
            return false;
        }


        bool DetectTemplateWithFilter(Mat mask)
        {
            Mat filter_screenshot = FilterThreshold(screenshot);
            Mat filter_template = FilterThreshold(template);
            Mat filter_mask = FilterThreshold(mask);
            filter_screenshot = filter_screenshot.CvtColor(ColorConversionCodes.GRAY2BGR);
            filter_template = filter_template.CvtColor(ColorConversionCodes.GRAY2BGR);
            filter_mask = filter_mask.CvtColor(ColorConversionCodes.GRAY2BGR);
            Console.WriteLine("screenshot chan " + filter_screenshot.Size() + " filterTemplate chan: " + filter_template.Size() + " filterMask chan: " + filter_mask.Size());
            Cv2.ImShow("filter screen", filter_screenshot);
            Cv2.ImShow("filter template", filter_template);
            Cv2.WaitKey();
            Mat result = new Mat();
            int result_cols = screenshot.Cols - template.Cols + 1;
            int result_rows = screenshot.Rows - template.Rows + 1;
            result.Create(MatType.CV_32FC1, result_cols, result_rows);
            Console.WriteLine("Attempting match");
            Cv2.MatchTemplate(filter_screenshot, filter_template, result, TemplateMatchModes.SqDiff, mask);
            Console.WriteLine("Post match");
            double minVal = 0; double maxVal = 0; OpenCvSharp.Point minLoc = new OpenCvSharp.Point(); OpenCvSharp.Point maxLoc = new OpenCvSharp.Point();
            OpenCvSharp.Point matchLoc;
            Cv2.MinMaxLoc(result, out minVal, out maxVal, out minLoc, out maxLoc);
            matchLoc = minLoc;
            Console.WriteLine("Min val: [" + minVal + "] minLoc: [" + minLoc + "] maxVal: [" + maxVal + "] maxLoc: [" + maxLoc + "]");
            //if (minVal <= 0.2)
            {
                /// Mark found template and save screenshot
                Cv2.Rectangle(filter_screenshot, matchLoc, new OpenCvSharp.Point(matchLoc.X + template.Cols, matchLoc.Y + template.Rows), new Scalar(), 2, LineTypes.Link8, 0);
                OpenCvSharp.Point centralMatchPoint = new OpenCvSharp.Point(matchLoc.X + template.Cols / 2, matchLoc.Y + template.Rows / 2);
                tapPoint = centralMatchPoint;
                Cv2.Circle(filter_screenshot, tapPoint, 10, Scalar.Red, 4, LineTypes.Link8, 0);
                Cv2.ImWrite(resultsPath + "result_" + currentTemplate + ".png", filter_screenshot);
                Console.WriteLine("Found match! Central point of found " + currentTemplate + ": " + centralMatchPoint);
                return true;
            }
            return false;
        }


        bool DetectThumbnailOnScreenshot() //should work different way than detecting template
        {
            Mat result = new Mat();
            int result_cols = screenshot.Cols - thumbnail.Cols + 1;
            int result_rows = screenshot.Rows - thumbnail.Rows + 1;
            result.Create(MatType.CV_32FC1, result_cols, result_rows);

            Cv2.MatchTemplate(screenshot, thumbnail, result, TemplateMatchModes.SqDiffNormed);

            double minVal = 0; double maxVal = 0; OpenCvSharp.Point minLoc = new OpenCvSharp.Point(); OpenCvSharp.Point maxLoc = new OpenCvSharp.Point();
            OpenCvSharp.Point matchLoc;
            Cv2.MinMaxLoc(result, out minVal, out maxVal, out minLoc, out maxLoc);
            matchLoc = minLoc;
            Console.WriteLine("Min val: [" + minVal + "] minLoc: [" + minLoc + "] maxVal: [" + maxVal + "] maxLoc: [" + maxLoc + "]");
            if (minVal <= 0.2)
            {
                /// Mark found template and save screenshot
                Cv2.Rectangle(screenshot, matchLoc, new OpenCvSharp.Point(matchLoc.X + thumbnail.Cols, matchLoc.Y + thumbnail.Rows), new Scalar(), 2, LineTypes.Link8, 0);
                OpenCvSharp.Point centralMatchPoint = new OpenCvSharp.Point(matchLoc.X + thumbnail.Cols / 2, matchLoc.Y + thumbnail.Rows / 2);
                tapPoint = centralMatchPoint;
                Cv2.Circle(screenshot, tapPoint, 10, Scalar.Red, 4, LineTypes.Link8, 0);
                Cv2.ImWrite(resultsPath + "result_" + currentThumbnail + ".png", screenshot);
                Console.WriteLine("Found match! Central point of found " + currentThumbnail + ": " + centralMatchPoint);
                return true;
            }
            return false;
        }


        bool SeekTemplate(Template template, int retries, int waitDuration)
        {
            LoadTemplate(template);
            for(int i = 0; i < retries; i++)
            {
                
                if(DetectTemplateOnScreenshot())
                {
                    return true;
                }
                System.Threading.Thread.Sleep(waitDuration);
            }
            //Save last try's screenshot if failed to find template
            Cv2.ImWrite(resultsPath + "FAIL_result_" + currentTemplate + ".png", screenshot);
            return false;
        }

        bool SeekTemplateWithMask(Template template, int retries, int waitDuration)
        {
            LoadTemplate(template);
            Mat mask = LoadMask(template);
            Console.WriteLine("Loaded mask for " + template + "(" + (int)template + ") has " + mask.Channels() + " channels");
            for (int i = 0; i < retries; i++)
            {

                if (DetectTemplateOnScreenshotWithMask(mask))
                {
                    return true;
                }
                System.Threading.Thread.Sleep(waitDuration);
            }
            //Save last try's screenshot if failed to find template
            Cv2.ImWrite(resultsPath + "FAIL_result_" + currentTemplate + ".png", screenshot);
            return false;
        }

        bool SeekTemplateWithFilter(Template template, int retries, int waitDuration)
        {
            LoadTemplate(template);
            Mat mask = LoadMask(template);
            for (int i = 0; i < retries; i++)
            {

                if (DetectTemplateWithFilter(mask))
                {
                    return true;
                }
                System.Threading.Thread.Sleep(waitDuration);
            }
            //Save last try's screenshot if failed to find template
            Cv2.ImWrite(resultsPath + "FAIL_result_" + currentTemplate + ".png", screenshot);
            return false;
        }



        bool SeekThumbnail(ThumbnailTemplate thumbnail, int retries, int waitDuration)
        {
            LoadThumbnailTemplate(thumbnail);
            for(int i = 0; i < retries; i++)
            {
                if(DetectThumbnailOnScreenshot())
                {
                    return true;
                }
                System.Threading.Thread.Sleep(waitDuration);
            }
            //Save last try's screenshot if failed to find template
            Cv2.ImWrite(resultsPath + "FAIL_result_" + currentThumbnail + ".png", screenshot);
            return false;
        }

        enum Stub
        {
            VRModeAsk       = 0,
            VRModeAskDirty  = 1,
            CaveHUDFull     = 2,
            CaveEmpty       = 3,
            SplashScreen    = 4,
            LoginPickScreen = 5,
            CaveHUDCrooked  = 6,
            NotRecognized = 12,
            RedVestryKeyboard = 13
        }
    
    }
    */
}
