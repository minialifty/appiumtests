﻿using System;
using NUnit.Framework;
using OpenCvSharp;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.MultiTouch;
using OpenQA.Selenium.Appium.iOS;

namespace appiumtests
{
    class Touch
    {
        bool android;
        int screenshotRatio = 1;

        AndroidDriver<AndroidElement> driver;
        IOSDriver<IOSElement> idriver;

        int errorCount = 0;
        int consecutiveErrorLimit = 4;

        public Touch(AndroidDriver<AndroidElement> driver)
        {
            android = true;
            this.driver = driver;
        }

        public Touch(IOSDriver<IOSElement> idriver)
        {
            android = false;
            this.idriver = idriver;
        }

        public void TapAtPoint(Point point)
        {
            if(android)
            {
                TapAtPointAndroid(point);
            }
            else
            {
                point.X = point.X / screenshotRatio;
                point.Y = point.Y / screenshotRatio;
                TapAtPointIOS(point);
            }
        }

        public void TapAtPointAndroid(Point point)
        {
            try
            {
                TouchAction touchAction = new TouchAction(driver);
                touchAction.Press(point.X, point.Y).Wait(20).Release().Perform();
                Console.WriteLine("Tapping at point: " + point.X + "," + point.Y);
                errorCount = 0;
            }
            catch (Exception e)
            {
                errorCount++;
                Console.WriteLine("Performing touch failed ( consecutive: " + errorCount + "). Error: " + e.Message);
                Console.WriteLine("Driver context: " + driver.Context + " activity: " + driver.CurrentActivity);
                if ((driver.CurrentActivity.Equals("co.theconstructutils.viewerplugin.ViewerActivity") || driver.CurrentActivity.Equals(".permission.ui.GrantPermissionsActivity")) && errorCount < consecutiveErrorLimit)
                {
                    System.Threading.Thread.Sleep(100);
                    TapAtPoint(point);
                }
                else
                {
                    Assert.Fail("Performing single tap failed!");
                }
            }
        }

        public void TapAtPointIOS(Point point)
        {
            try
            {
                TouchAction touchAction = new TouchAction(idriver);
                touchAction.Press(point.X, point.Y).Wait(20).Release().Perform();
                Console.WriteLine("iOS: Tapping at point: " + point.X + "," + point.Y);
                errorCount = 0;
            }
            catch (Exception e)
            {
                errorCount++;
                Console.WriteLine("iOS: Performing touch failed ( consecutive: " + errorCount + "). Error: " + e.Message);
                if (errorCount < consecutiveErrorLimit)
                {
                    System.Threading.Thread.Sleep(100);
                    TapAtPointIOS(point);
                }
                else
                {
                    Assert.Fail("iOS: Performing single tap failed!");
                }
            }
        }

        public void DoubleTapAtPoint(Point point)
        {
            if(android)
            {
                DoubleTapAtPointAndroid(point);
            }
            else
            {
                point.X = point.X / screenshotRatio;
                point.Y = point.Y / screenshotRatio;
                DoubleTapAtPointIOS(point);
            }
        }

        public void DoubleTapAtPointAndroid(Point point)
        {
            try
            {
                TouchAction touchAction = new TouchAction(driver);
                touchAction.Tap(point.X, point.Y, 2).Perform();
                errorCount = 0;
                Console.WriteLine("Android: double tap performed [" + point.X + ", " + point.Y + "]");
            }
            catch (Exception e)
            {
                errorCount++;
                Console.WriteLine("Performing double tap failed ( consecutive: " + errorCount + "). Error: " + e.Message);
                if (driver.Context.Equals("NATIVE_APP") && driver.CurrentActivity.Equals("co.theconstructutils.viewerplugin.ViewerActivity") && errorCount < consecutiveErrorLimit)
                {
                    System.Threading.Thread.Sleep(100);
                    DoubleTapAtPointAndroid(point);
                }
                else
                {
                    Assert.Fail("Performing double tap failed!");
                }
            }
        }

        public void DoubleTapAtPointIOS(Point point)
        {
            try
            {
                TouchAction touchAction = new TouchAction(idriver);
                touchAction.Tap(point.X, point.Y, 2).Perform();
                Console.WriteLine("iOS: double tap performed [" + point.X +", " + point.Y + "]");
                errorCount = 0;
            }
            catch (Exception e)
            {
                errorCount++;
                Console.WriteLine("iOS: Performing double tap failed ( consecutive: " + errorCount + "). Error: " + e.Message);
                if ( errorCount < consecutiveErrorLimit)
                {
                    System.Threading.Thread.Sleep(100);
                    DoubleTapAtPointIOS(point);
                }
                else
                {
                    Assert.Fail("iOS: Performing double tap failed!");
                }
            }

        }


        public void SlowDoubleTapAtPoint(Point point)
        {
            if (android)
            {
                SlowDoubleTapAtPointAndroid(point);
            }
            else
            {
                point.X = point.X / screenshotRatio;
                point.Y = point.Y / screenshotRatio;
                SlowDoubleTapAtPointiOS(point);
            }
        }

        public void SlowDoubleTapAtPointAndroid(Point point)
        {
            try
            {
                TouchAction touchAction = new TouchAction(driver);
                touchAction.Press(point.X, point.Y).Wait(10).Release().Wait(700).Tap(point.X, point.Y).Press(point.X, point.Y).Wait(10).Release().Perform();
                errorCount = 0;
                Console.WriteLine("Android: slow double tap performed [" + point.X + ", " + point.Y + "]");
            }
            catch (Exception e)
            {
                errorCount++;
                Console.WriteLine("Android: Performing slow double tap failed ( consecutive: " + errorCount + "). Error: " + e.Message);
                if (errorCount < consecutiveErrorLimit)
                {
                    System.Threading.Thread.Sleep(100);
                    SlowDoubleTapAtPointAndroid(point);
                }
                else
                {
                    Assert.Fail("Andoid: Performing slow double tap failed!");
                }
            }
        }

        public void SlowDoubleTapAtPointiOS(Point point)
        {
            try
            {
                TouchAction touchAction = new TouchAction(idriver);
                touchAction.Press(point.X, point.Y).Wait(10).Release().Wait(700).Tap(point.X, point.Y).Press(point.X, point.Y).Wait(10).Release().Perform();
                errorCount = 0;
                Console.WriteLine("iOS: slow double tap performed [" + point.X + ", " + point.Y + "]");
            }
            catch (Exception e)
            {
                errorCount++;
                Console.WriteLine("iOS: Performing slow double tap failed ( consecutive: " + errorCount + "). Error: " + e.Message);
                if (errorCount < consecutiveErrorLimit)
                {
                    System.Threading.Thread.Sleep(100);
                    SlowDoubleTapAtPointiOS(point);
                }
                else
                {
                    Assert.Fail("iOS: Performing slow double tap failed!");
                }
            }

        }


        public void UnlockSwipe()
        {
            TouchAction touchAction = new TouchAction(driver);
            touchAction.Press(720, 2350).MoveTo(720, 1340).Release().Perform();
        }

        public void SetScreenshotRatio(int screenshotRatio)
        {
            this.screenshotRatio = screenshotRatio;
        }

    }
}
