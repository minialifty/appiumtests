﻿using System;
using NUnit.Framework;
using OpenCvSharp;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.iOS;


namespace appiumtests
{
    class ScreenshotActions
    {
        Size screenSize;
        public int screenshotRatio = 1;
        ResolutionSet resolutionSet;

        bool android;
        public string pathRoot = "no_path";
        public string screenshotsPath = "no_path";
        public string templatesPath = "no_path";
        public string resultsPath = "no_path";

        public string screenshotPrefix = "";
        int screenshotCount = 0;
        int errorCount = 0;
        int consecutiveErrorLimit = 4;
        int resultCount = 0;

        public Mat screenshot = new Mat();
        public Element currentElement;
        public Template currentTemplate;
        public Mat template;
        public KeyTemplate currentKey;
        public Mat keyTemplate;
        public ThumbnailTemplate currentThumbnail;
        public Mat thumbnailTemplate;

        AndroidDriver<AndroidElement> driver;
        IOSDriver<IOSElement> idriver;

        public ScreenshotActions(AndroidDriver<AndroidElement> driver, string localPath)
        {
            android = true;
            this.driver = driver;
            pathRoot = localPath;
            screenshotsPath = pathRoot + "AppiumTestsOutput/screenshots/";
            templatesPath = pathRoot + "AppiumTestsOutput/templates/";
            resultsPath = pathRoot + "AppiumTestsOutput/results/";
        }

        public ScreenshotActions(IOSDriver<IOSElement> idriver, string localPath)
        {
            android = false;
            this.idriver = idriver;
            pathRoot = localPath;
            screenshotsPath = pathRoot + "AppiumTestsOutput/screenshots/";
            templatesPath = pathRoot + "AppiumTestsOutput/templates/";
            resultsPath = pathRoot + "AppiumTestsOutput/results/";
        }


        public void LoadNewScreenshot()
        {
            if(android)
            {
                LoadNewScreenshotAndroid();
            }
            else
            {
                LoadNewScreenshotIOS();
            }
        }

        public void LoadNewScreenshotIOS()
        {
            Mat result = new Mat();
            try
            {
                Screenshot screenshot = idriver.GetScreenshot();
                string filename = screenshotPrefix + "screenshot_" + screenshotCount + ".png";
                screenshot.SaveAsFile(screenshotsPath + filename);
                Console.WriteLine("iOS: Took screenshot" + filename);
                this.screenshot = Cv2.ImRead(screenshotsPath + filename);
                screenshotCount++;
                errorCount = 0;
            }
            catch (Exception e)
            {
                Console.WriteLine("iOS: Taking screenshot failed. Error count: " + errorCount);
                Console.WriteLine("iOS: Taking screenshot failed. Error:" + e.Message);
                if (idriver.Context.Equals("NATIVE_APP") && errorCount < consecutiveErrorLimit)
                {
                    errorCount++;
                    System.Threading.Thread.Sleep(500);
                    LoadNewScreenshotIOS();
                }
                else
                {
                    Assert.Fail("iOS: Taking new screenshot failed!");
                }
            }
        }

        public void LoadNewScreenshotAndroid()
        {
            Mat result = new Mat();
            try
            {
                Screenshot screenshot = driver.GetScreenshot();
                string filename = screenshotPrefix + "screenshot_" + screenshotCount + ".png";
                screenshot.SaveAsFile(screenshotsPath + filename);
                Console.WriteLine("Took screenshot" + filename);
                this.screenshot = Cv2.ImRead(screenshotsPath + filename);
                screenshotCount++;
                errorCount = 0;
            }
            catch (Exception e)
            {
                Console.WriteLine("Android: Taking screenshot failed. Activity: " + driver.CurrentActivity+ " Error count: " + errorCount);
                Console.WriteLine("Android: Taking screenshot failed. Error:" + e.Message);
                if (driver.Context.Equals("NATIVE_APP") && driver.CurrentActivity.Equals("co.theconstructutils.viewerplugin.ViewerActivity") && errorCount < consecutiveErrorLimit)
                {
                    errorCount++;
                    System.Threading.Thread.Sleep(500);
                    LoadNewScreenshotAndroid();
                }
                else
                {
                    Assert.Fail("Taking new screenshot failed!");
                }
            }
        }

        public void LoadNewTempScreenshot()
        {
            if(android)
            {
                LoadNewTempScreenshotAndroid();
            }
            else
            {
                LoadNewTempScreenshotIOS();
            }
        }

        public void LoadNewTempScreenshotIOS()
        {
            try
            {
                Screenshot screenshot = idriver.GetScreenshot();
                string filename = screenshotPrefix + "00_screenshot_temp.png";
                screenshot.SaveAsFile(screenshotsPath + filename);
                Console.WriteLine("Took screenshot" + filename);
                this.screenshot = Cv2.ImRead(screenshotsPath + filename);
                screenshotCount++;
                errorCount = 0;
            }
            catch (Exception e)
            {
                Console.WriteLine("Taking screenshot failed. Error count: " + errorCount);
                Console.WriteLine("Taking screenshot failed. Error:" + e.Message);
                if (idriver.Context.Equals("NATIVE_APP") && errorCount < consecutiveErrorLimit)
                {
                    errorCount++;
                    System.Threading.Thread.Sleep(500);
                    LoadNewTempScreenshotIOS();
                }
                else
                {
                    Assert.Fail("Taking new temporary screenshot failed!");
                }
            }
        }

        public void LoadNewTempScreenshotAndroid()
        {
            try
            {
                Screenshot screenshot = driver.GetScreenshot();
                string filename = screenshotPrefix + "00_screenshot_temp.png";
                screenshot.SaveAsFile(screenshotsPath + filename);
                Console.WriteLine("Took screenshot" + filename);
                this.screenshot = Cv2.ImRead(screenshotsPath + filename);
                screenshotCount++;
                errorCount = 0;
            }
            catch (Exception e)
            {
                Console.WriteLine("Taking screenshot failed. Error:" + e.Message);
                Console.WriteLine("Taking screenshot failed. Activity: " + driver.CurrentActivity + " Error count: " + errorCount);
                if (driver.Context.Equals("NATIVE_APP") && driver.CurrentActivity.Equals("co.theconstructutils.viewerplugin.ViewerActivity") && errorCount < consecutiveErrorLimit)
                {
                    errorCount++;
                    System.Threading.Thread.Sleep(500);
                    LoadNewTempScreenshotAndroid();
                }
                else
                {
                    Assert.Fail("Taking new temporary screenshot failed!");
                }
            }
        }


        public void LoadTemplate(Template template)
        {
            //iOS templates (different resolution) numbers start at 100
            string newTemplateName = "detection_template_" + (int)template + ".png";
            this.template = Cv2.ImRead(templatesPath + newTemplateName);
            currentTemplate = template;
            Console.WriteLine("Loaded detection template " + template + " (" + newTemplateName + ")");
        }

        public Mat LoadHistogram(Histogram histogram)
        {
            string newTemplateName = "histogram_" + (int)histogram + ".png";
            Mat hist = Cv2.ImRead(templatesPath + newTemplateName);
            Console.WriteLine("Loaded histogram template " + histogram + " (" + newTemplateName + ")");
            return hist;
        }

        public void LoadThumbnailTemplate(ThumbnailTemplate thumbnailTemplate)
        {
            string newTemplateName = "thumbnail_" + (int)thumbnailTemplate + ".png";
            this.thumbnailTemplate = Cv2.ImRead(templatesPath + "Thumbnails/" + newTemplateName);
            currentThumbnail = thumbnailTemplate;
            Console.WriteLine("Loaded thumbnail template " + thumbnailTemplate + " (" + newTemplateName + ")");
        }

        public void LoadKeyTemplate(KeyTemplate key)
        {
            string newKeyTemplateName = "key_" + (int)key + ".png";
            this.keyTemplate = Cv2.ImRead(templatesPath + "keyboard/" + newKeyTemplateName);
            currentKey = key;
            Console.WriteLine("Loaded key template " + key + " (" + newKeyTemplateName + ")");
        }

        public void LoadKey(Key key)
        {
            switch ((int)resolutionSet)
            {
                case 0:
                    Console.WriteLine("Unsupported resolution while loading key template!");
                    Assert.Fail("Unsupported resolution while loading key template!");
                    break;
                case 1:
                   LoadKeyTemplate((KeyTemplate)(int)key);
                    break;
                case 2:
                    LoadKeyTemplate((KeyTemplate)(int)key + 100);
                    break;
                case 3:
                    LoadKeyTemplate((KeyTemplate)(int)key + 200);
                    break;
                default:
                    Console.WriteLine("Resolution not recogized while loading key template!");
                    Assert.Fail("Resolution not recogized while loading key template!");
                    break;
            }
        }

        public void SaveResultScreenshot(Mat result, bool success)
        {
            string successString = "";
            if(!success) { successString = "FAIL_"; }
            Cv2.ImWrite(resultsPath + successString + screenshotPrefix + "_" + resultCount + "_result_" + currentTemplate + ".png", screenshot);
            resultCount++;
        }

        public void SaveResultScreenshot(Mat result, string customName, bool success)
        {
            string successString = "";
            if (!success) { successString = "FAIL_"; }
            Cv2.ImWrite(resultsPath + successString + screenshotPrefix + "_" + resultCount + "_result_" + customName + ".png", screenshot);
            resultCount++;
        }

        /// <summary>
        /// Red becomes white, all rest black
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public Mat FilterRed(Mat image)
        {
            Mat hsv_image = image.CvtColor(ColorConversionCodes.BGR2HSV);
            Mat result = hsv_image.InRange(new Scalar(0, 100, 100), new Scalar(10, 255, 255));
            return result;
        }

        /// <summary>
        /// Green becomes white, all rest black
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public Mat FilterGreen(Mat image)
        {
            Mat hsv_image = image.CvtColor(ColorConversionCodes.BGR2HSV);
            Mat result = hsv_image.InRange(new Scalar(10, 100, 100), new Scalar(150, 255, 255));
            return result;
        }

        /// <summary>
        /// Thresholds image 200-255
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public Mat FilterNonWhite(Mat image)
        {
            Mat result = image.Threshold(200, 255, ThresholdTypes.Tozero);
            return result;
        }

        public void SetScreenshotRatio(Touch touchInput)
        {
            LoadNewTempScreenshot();
            System.Drawing.Size screenSize;
            if(android)
            {
                screenSize = driver.Manage().Window.Size;
            }
            else
            {
                screenSize = idriver.Manage().Window.Size;
            }
            this.screenSize = new Size(screenSize.Width, screenSize.Height);
            Console.WriteLine("Screen size: " + screenSize.ToString());
            Size screenshotSize = screenshot.Size();
            Console.WriteLine("Screenshot size: " + screenshotSize.ToString());
            screenshotRatio = screenshotSize.Width / screenSize.Width;
            touchInput.SetScreenshotRatio(screenshotRatio);
            Console.WriteLine("Ratio: " + screenshotRatio);
            this.resolutionSet = MatchResolutionSet(screenshotSize);
        }

        public void LoadElement(Element element)
        {
            switch((int)resolutionSet)
            {
                case 0:
                    Console.WriteLine("Unsupported resolution while loading template!");
                    Assert.Fail("Unsupported resolution while loading template!");
                    break;
                case 1:
                    LoadTemplate((Template)(int)element);
                    break;
                case 2:
                    LoadTemplate((Template)(int)element + 100);
                    break;
                case 3:
                    LoadTemplate((Template)(int)element + 200);
                    break;
                default:
                    Console.WriteLine("Resolution not recogized while loading template!");
                    Assert.Fail("Resolution not recogized while loading template!");
                    break;
            }
        }

        public Mat CutScreen(Point centerPoint, int width, int height, Mat screenshot)
        {
            Mat cutout = screenshot[centerPoint.Y - height / 2, centerPoint.Y + height / 2, centerPoint.X - width / 2, centerPoint.X + width / 2];
            return cutout;
        }

        public Mat TakeCutScreenshot(Point centerPoint, int width, int height)
        {
            LoadNewScreenshot();
            return CutScreen(centerPoint, width, height, screenshot);
        }

        public Mat Histogram(Mat screenshot)
        {
            Rangef[] ranges = { new Rangef(0, 179) };
            int hbins = 16;
            int[] channels = { 0 }; //only hue channel
            int[] histSize = { hbins };
            Mat screenshot_hsv = screenshot.CvtColor(ColorConversionCodes.BGR2HSV);
            Mat[] img = { screenshot_hsv };
            Mat h_hist = new Mat();
            Cv2.CalcHist(img, channels, null, h_hist, 1, histSize, ranges);
            double minVal;
            double maxVal;
            Point minLoc;
            Point maxLoc;
            Cv2.MinMaxLoc(h_hist, out minVal, out maxVal, out minLoc, out maxLoc);
            Console.WriteLine("Size: " + h_hist.Size());
            Console.WriteLine("Histogram: min " + minVal + "[" + minLoc + "] max " + maxVal + "[" + maxLoc + "]");
            for (int i = 0; i < h_hist.Rows; i++)
            {
                Console.WriteLine("Bucket hue: " + i + ": " + h_hist.At<float>(0, i));
            }
            return h_hist;
        }

        public Mat NewHistogram()
        {
            LoadNewTempScreenshot();
            Rangef[] ranges = { new Rangef(0, 179) };
            int hbins = 16;
            int[] channels = { 0 }; //only hue channel
            int[] histSize = { hbins };
            Mat screenshot_hsv = screenshot.CvtColor(ColorConversionCodes.BGR2HSV);
            Mat[] img = { screenshot_hsv };
            Mat h_hist = new Mat();
            Cv2.CalcHist(img, channels, null, h_hist, 1, histSize, ranges);
            double minVal;
            double maxVal;
            Point minLoc;
            Point maxLoc;
            Cv2.MinMaxLoc(h_hist, out minVal, out maxVal, out minLoc, out maxLoc);
            Console.WriteLine("Size: " + h_hist.Size());
            Console.WriteLine("Histogram: min " + minVal + "[" + minLoc + "] max " + maxVal + "[" + maxLoc + "]");
            for (int i = 0; i < h_hist.Rows; i++)
            {
                Console.WriteLine("Bucket hue: " + i + ": " + h_hist.At<float>(0, i));
            }
            return h_hist;
        }

        public Mat NewHistogramSaurationChannel()
        {
            LoadNewTempScreenshot();
            Rangef[] ranges = {new Rangef(0,255) }; //hue range, sauration range
            int sbins = 17;
            int[] channels = {1 }; 
            int[] histSize = { sbins };
            Mat screenshot_hsv = screenshot.CvtColor(ColorConversionCodes.BGR2HSV);
            Mat[] img = { screenshot_hsv };
            Mat h_hist = new Mat();
            Cv2.CalcHist(img, channels, null, h_hist, 1, histSize, ranges);
            double minVal;
            double maxVal;
            Point minLoc;
            Point maxLoc;
            Cv2.MinMaxLoc(h_hist, out minVal, out maxVal, out minLoc, out maxLoc);
            Console.WriteLine("Size: " + h_hist.Size());
            Console.WriteLine("Histogram: min " + minVal + "[" + minLoc + "] max " + maxVal + "[" + maxLoc + "]");
            for (int i = 0; i < h_hist.Rows; i++)
            {
                Console.WriteLine("Bucket sauration: " + i + ": " + h_hist.At<float>(0, i));
            }
            return h_hist;
        }

        public Mat HistogramSaurationChannel(Mat screenshot)
        {
            Rangef[] ranges = {  new Rangef(0, 255) };
            int sbins = 17;
            int[] channels = { 1 };
            int[] histSize = { sbins};
            Mat screenshot_hsv = screenshot.CvtColor(ColorConversionCodes.BGR2HSV);
            Mat[] img = { screenshot_hsv };
            Mat h_hist = new Mat();
            Cv2.CalcHist(img, channels, null, h_hist, 1, histSize, ranges);
            double minVal;
            double maxVal;
            Point minLoc;
            Point maxLoc;
            Cv2.MinMaxLoc(h_hist, out minVal, out maxVal, out minLoc, out maxLoc);
            Console.WriteLine("Size: " + h_hist.Size());
            Console.WriteLine("Histogram: min " + minVal + "[" + minLoc + "] max " + maxVal + "[" + maxLoc + "]");
            for (int i = 0; i < h_hist.Rows; i++)
            {
                Console.WriteLine("Bucket sauration: " + i + ": " + h_hist.At<float>(0, i));
            }
            return h_hist;
        }

        public bool HistogramCompareDoubleChannel(Mat template, float threshold = 0.1f)
        {
            Mat screenHistHue = NewHistogram();
            Mat templateHistHue = Histogram(template);
            double resultHue = Cv2.CompareHist(screenHistHue, templateHistHue, HistCompMethods.Bhattacharyya);
            Console.WriteLine("Hue Hist Comparsion result = " + resultHue);
            if (resultHue >= threshold)
            {
                return false;
            }
            Mat screenHistSaur = NewHistogramSaurationChannel();
            Mat templateHistSaur = HistogramSaurationChannel(template);
            double resultSaur = Cv2.CompareHist(screenHistSaur, templateHistSaur, HistCompMethods.Bhattacharyya);
            Console.WriteLine("Sauration Hist Comparsion result = " + resultSaur);
            if (resultSaur < threshold)
            {
                return true;
            }
            return false;
        }

        public bool HistogramCompare(Mat screenshot, Mat template, float threshold = 0.1f)
        {
            Mat screenHist = Histogram(screenshot);
            Mat templateHist = Histogram(template);
            double result = Cv2.CompareHist(screenHist, templateHist, HistCompMethods.Bhattacharyya);
            Console.WriteLine("Hist Comparsion result = " + result);
            if (result < threshold)
            {
                return true;
            }
            return false;
        }

        public bool HistogramCompare(Mat template, float threshold = 0.1f)
        {
            Mat screenHist = NewHistogram();
            Mat templateHist = Histogram(template);
            double result = Cv2.CompareHist(screenHist, templateHist, HistCompMethods.Bhattacharyya);
            Console.WriteLine("Hist Comparsion result = " + result);
            if(result<threshold)
            {
                return true;
            }
            return false;
        }

        public bool HistogramCompareLR(Histogram histogramTemplate, float threshold = 0.1f)
        {
            LoadNewTempScreenshot();
            Console.WriteLine("Creating L i R screenshot parts");
            Mat LScreen = screenshot[new Rect(0, 0, screenshot.Width / 2, screenshot.Height)];
            Mat RScreen =  screenshot[new Rect(screenshot.Width/2, 0, screenshot.Width/2, screenshot.Height)];
            Mat templ = LoadHistogram(histogramTemplate);
            Console.WriteLine("Creating L i R template parts");
            Mat LTempl = templ[new Rect(0, 0, templ.Width / 2, templ.Height)];
            Mat RTempl = templ[new Rect(templ.Width / 2, 0, templ.Width / 2, templ.Height)];
            bool compared = HistogramCompare(LScreen, LTempl);
            if(compared)
            {
                compared = HistogramCompare(RScreen, RTempl);
            }
            return compared;

        }

        public ResolutionSet MatchResolutionSet(Size screenSize)
        {
            Size tempSize;
            int n = 4; //number of supported resolutions
            for(int i = 1; i < n; i++)
            {
                tempSize = GetResolutionAsSize((ResolutionSet)i);
                if(tempSize.Equals(screenSize))
                {
                    Console.WriteLine("Matched resolution: " + (ResolutionSet)i);
                    return (ResolutionSet)i;
                }
            }
            Console.WriteLine("Matched resolution is unsupported! [" + screenSize + "]");
            return ResolutionSet.unsupported;
        }

        public Size GetResolutionAsSize(ResolutionSet resolution)
        {
            switch((int)resolution)
            {
                case 0:
                    Console.WriteLine("Calling Resolution as size on unsupported resolution!");
                    return new Size(0, 0);
                case 1: return new Size(2560, 1440);
                case 2: return new Size(2208, 1242);
                default:
                    Console.WriteLine("Calling Resolution as size on undefined resolution!");
                    return new Size(0, 0);
            }
        }

    }
}
