﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace appiumtests
{

    public enum Template
    {
        //r2560x1440 set
        SplashLogo = 0,
        DevelopmentBuild = 1,
        StartInVRModeAsk = 2,
        NoButton = 3,
        YesButton = 4,
        SignInGoogle = 5,
        SignInFacebook = 6,
        SignInMagicCode = 7,
        SkipButton = 8,
        VRToursHeader = 9,
        MagicCodeButton = 10,
        HUDLogo = 11,
        Actions = 12,
        LoadImageButton = 13,
        GyroUnlockedButton = 14,
        AudioButton = 15,
        TutuorialButton = 16,
        NextPageButton = 17,
        AudioMutedButton = 18,
        AllowButton = 19,
        SkipTutorialButton = 20,
        GyroLockedButton = 21,
        DownloadingLabel = 22,
        Vestry70BlueVase = 23,
        Vestry70Chair = 24,
        Vestry70OrangeVase = 25,
        KeyboardPlay = 26,
        KeyboardExit = 27,
        TestSV1CubeTLamp = 28,
        TestSV1CubeTRobotPlate = 29,
        KeyboardPlayPress = 30,
        Vestry70Armchair = 31,
        Vestry70Lamp = 32,
        RedFloorplan = 33,
        DoggoFloorplan = 34,
        CyanFloorplan = 35,
        DoggoPNGFloorplan = 36,
        //r2208x1242 set
        iOS_NoButton = 103,
        iOS_YesButton = 104,
        iOS_SignInMagicCode = 107,
        iOS_SkipButton = 108,
        iOS_VRToursHeader = 109,
        iOS_MagicCodeButton = 110,
        iOS_HUDLogo = 111,
        iOS_SkipTutorialButton = 120,
        iOS_GyroLockedButton = 121,
        iOS_Vestry70BlueVase = 123,
        iOS_Vestry70OrangeVase = 125,
        iOS_KeyboardPlay = 126,
        iOS_KeyboardPlayPress = 130,
        iOS_Vestry70Armchair = 131,
        iOS_Vestry70Lamp = 132,
        iOS_RedFloorplan = 133,
        iOS_DoggoFlooplan = 134,
        iOS_CyanFloorplan = 135,
        iOS_DoggoPNGFloorplan = 136,

        iOS_small_NoButton = 203,
        iOS_small_SkipButton = 208,

        GVR_SkipButton = 308
    }

    public enum KeyTemplate
    {
        //r2560x1440
        Zero = 0,
        One = 1,
        Two = 2,
        Three = 3,
        Four = 4,
        Five = 5,
        Six = 6,
        Seven = 7,
        Eight = 8,
        Nine = 9,
        Q = 10,
        W = 11,
        E = 12,
        R = 13,
        T = 14,
        Y = 15,
        U = 16,
        I = 17,
        O = 18,
        P = 19,
        A = 20,
        S = 21,
        D = 22,
        F = 23,
        G = 24,
        H = 25,
        J = 26,
        K = 27,
        L = 28,
        Z = 29,
        X = 30,
        C = 31,
        V = 32,
        B = 33,
        N = 34,
        M = 35,
        //r2208x1242 set
        iZero = 100,
        iOne = 101,
        iTwo = 102,
        iThree = 103,
        iFour = 104,
        iFive = 105,
        iSix = 106,
        iSeven = 107,
        iEight = 108,
        iNine = 109,
        iQ = 110,
        iW = 111,
        iE = 112,
        iR = 113,
        iT = 114,
        iY = 115,
        iU = 116,
        iI = 117,
        iO = 118,
        iP = 119,
        iA = 120,
        iS = 121,
        iD = 122,
        iF = 123,
        iG = 124,
        iH = 125,
        iJ = 126,
        iK = 127,
        iL = 128,
        iZ = 129,
        iX = 130,
        iC = 131,
        iV = 132,
        iB = 133,
        iN = 134,
        iM = 135

    }

    public enum Key
    {
        Zero = 0,
        One = 1,
        Two = 2,
        Three = 3,
        Four = 4,
        Five = 5,
        Six = 6,
        Seven = 7,
        Eight = 8,
        Nine = 9,
        Q = 10,
        W = 11,
        E = 12,
        R = 13,
        T = 14,
        Y = 15,
        U = 16,
        I = 17,
        O = 18,
        P = 19,
        A = 20,
        S = 21,
        D = 22,
        F = 23,
        G = 24,
        H = 25,
        J = 26,
        K = 27,
        L = 28,
        Z = 29,
        X = 30,
        C = 31,
        V = 32,
        B = 33,
        N = 34,
        M = 35
    }

    public enum ThumbnailTemplate
    {
        LakeHouse = 0,
        Vestry70 = 1,
        Foodcourt = 2,
        Broome565 = 3,
        MicrosoftEntrance = 4,
        STHLMNEW = 5,
        iOS_Vestry70 = 101
    }


    public enum Element
    {
        NoButton = 3,
        YesButton = 4,
        SignInMagicCode = 7,
        SkipButton = 8,
        VRToursHeader = 9,
        MagicCodeButton = 10,
        HUDLogo = 11,
        AllowButton = 19,
        SkipTutorialButton = 20,
        GyroLockButton = 21,
        Vestry70BlueVase = 23,
        Vestry70OrangeVase = 25,
        KeyboardPlay = 26,
        KeyboardPlayPress = 30,
        Vestry70Armchair = 131,
        Vestry70Lamp = 132,
        RedFloorplan = 33,
        DoggoFloorplan = 34,
        CyanFloorplan = 35,
        DoggoPNGFloorplan = 36
    }

    public enum Histogram
    {
        TestTourLanding = 0,
        TestTour2Landing = 1,
        TestTourLandingHUD = 2,
        TestTourLandingVR = 3,
        TestTour3LandingVR = 4,
        TTLandHUDVR = 5,
        TT3LandHUDVR = 6,
        TestTour3Landing = 7,
        TestTour3LandingHUD = 8,
        ResizeTour = 9,
        RedSpheric = 12,
        CyanSpheric = 13,
        RedSphericNoFloorplan = 14,
        CyanSphericNoFloorplan = 15,
        iOSTestTourLanding = 100,
        iOSTestTour2Landing = 101,
        iOSTestTourLandingHUD = 102,
        iOSTestTourLandingVR = 103,
        iOSTestTour3LandingVR = 104,
        iOSTTLandHUDVR  = 105,
        iOSTT3LandHUDVR = 106,
        iOSTestTour3Landing = 107,
        iOSTestTour3LandingHUD = 108,
        iOSResizeTour = 109,
        iOSRedSpheric = 112,
        iOSCyanSpheric = 113,
        iOSRedSphericNoFloorplan = 114,
        iOSCyanSphericNoFloorplan = 115,
        GVR_LoginScreen = 300,
        GVR_TestTourLanding = 303,
        GVR_TestTourLandingHUD = 305,
        GVR_Tour8k = 309,
        GVR_Tour8kHUD = 310
    }

    /// <summary>
    /// Describes resolutions set of templates
    /// </summary>
    public enum ResolutionSet //MatchResolutionSet uses number of supported resolutions
    {
        unsupported = 0,
        r2560x1440 = 1,
        r2208x1242 = 2,
        r1136x640  = 3
    }

    public enum Phone
    {
        SmGalaxyS6 = 0,
        SmGalaxyS7edge = 1,
        non_recogised = 2
    }

    public enum iPhone
    {
        First = 0,
        iPhone5S = 1,
        iPhone6Plus = 2,
        non_recogised =3
    }
    
    public enum TestDevice
    {
        SmGalaxyS6 = 0,
        SmGalaxyS7edge = 1,
        iPhone6 = 2,
        iPhone6Plus = 3
    }
}
