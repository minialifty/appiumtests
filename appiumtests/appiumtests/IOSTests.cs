﻿using System;
using System.IO;
using NUnit.Framework;
using OpenCvSharp;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.iOS;
using OpenQA.Selenium.Appium.Service;

namespace appiumtests
{

    public class IOSTests
    {
        iPhone iphone;
        string rootPath;
        public int warpSteps = 10;
        public bool saveSuccessScreens = true;

        AppiumLocalService server;
        IOSDriver<IOSElement> idriver;
        AppiumOptions options;

        int serverRetryCount = 0;
        int serverRetryLimit = 3;

        ScreenshotActions screenshotActions;
        Detector detector;
        Touch touchInput;
        Confirmer confirmer;

        Point gyroButtonPoint;
        int errorCount = 0;
        int consecutiveErrorLimit = 4;

        string firstCode = "ljo0s7";
        Key[] firstMagicCode = new Key[] { Key.L, Key.J, Key.O, Key.Zero, Key.S, Key.Seven };
        string secondCode = "toevg6";
        Key[] secondMagicCode = new Key[] { Key.T, Key.O, Key.E, Key.V, Key.G, Key.Six };
        string thirdCode = "makuy2";
        Key[] thirdMagicCode = new Key[] { Key.M, Key.A, Key.K, Key.U, Key.Y, Key.Two };
        string fourthCode = "eslyed";
        Key[] fourthMagicCode = new Key[] { Key.E, Key.S, Key.L, Key.Y, Key.E, Key.D };

        bool initialised = false;

        public void InitializeParams()
        {
            var target = TestContext.Parameters.Get("Device");
            iphone = ParseTargtDevice(target);
            rootPath = TestContext.Parameters.Get("Rootpath");
            var warpSteps = TestContext.Parameters.Get("WarpSteps");
            Console.WriteLine("Targeted device: " + iphone + " Root path: " + rootPath);
            if (!int.TryParse(warpSteps, out this.warpSteps) || warpSteps.Equals(""))
            {
                this.warpSteps = 10;
                Console.WriteLine("Warp steps set to default: " + this.warpSteps);
            }
            else
            {
                Console.WriteLine("Warp steps set to: " + warpSteps);
            }
            Assert.AreNotEqual(Phone.non_recogised, iphone, "Target device was not recogised");
            Assert.IsFalse((!Directory.Exists(rootPath) || !Directory.Exists(rootPath + "AppiumTestsOutput/") || !Directory.Exists(rootPath + "AppiumTests/")), "Project path setup is incorrect");
            initialised = true;
        }

        public bool FindOrStartAppiumServer()
        {
            if (server != null && server.IsRunning)
            {
                serverRetryCount = 0;
                return true;
            }
            try
            {

                Console.WriteLine("Building new server service");
                //AppiumServiceBuilder serverBuilder = new AppiumServiceBuilder().WithIPAddress("127.0.0.1").UsingPort(4723).WithAppiumJS(new FileInfo("C:/Users/theC/node_modules/appium/build/lib/main.js"));
                AppiumServiceBuilder serverBuilder = new AppiumServiceBuilder().WithIPAddress("192.168.1.116").UsingAnyFreePort().WithAppiumJS(new FileInfo("C:/Users/theC/node_modules/appium/build/lib/main.js"));
                server = serverBuilder.Build();
                server.Start();
                Console.WriteLine("Server started on: " + server.ServiceUrl);
                if (server.IsRunning)
                {
                    serverRetryCount = 0;
                    return true;
                }
                else
                {
                    Console.WriteLine("Server not running...");
                    if (serverRetryCount >= serverRetryLimit)
                    {
                        Console.WriteLine("Failed to start server in " + serverRetryCount + " retries");
                        return false;
                    }
                    serverRetryCount++;
                    FindOrStartAppiumServer();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error staring appium server! " + e.Message);
                if (serverRetryCount >= serverRetryLimit)
                {
                    Console.WriteLine("Failed to start server in " + serverRetryCount + " retries");
                    return false;
                }
                serverRetryCount++;
                FindOrStartAppiumServer();
            }
            return false;
        }


        //[SetUp]
        public void SetUpiOS(bool reinstall = true)
        {
            if (!initialised) InitializeParams();
            SetUpCaps(iPhone.iPhone6Plus, reinstall);
            Console.WriteLine("Staring driver...");
            FindOrStartAppiumServer();
            try
            {
                idriver = new IOSDriver<IOSElement>(server, options);
            }
            catch(WebDriverTimeoutException e)
            {
                Assert.Fail("God hates this code");
            }
            screenshotActions = new ScreenshotActions(idriver, rootPath);
            detector = new Detector(screenshotActions);
            touchInput = new Touch(idriver);
            confirmer = new Confirmer(detector, screenshotActions, touchInput, false, rootPath, saveSuccessScreens);
            Console.WriteLine("Set up complete");
        }

        [TearDown]
        public void Close()
        {
            CloseDriver();
            Console.WriteLine("Complete");
        }

        [Test]
        public void BasicTest()
        {
            SetUpiOS(true);
            try
            {
                Console.WriteLine("Starting test");
                Console.WriteLine("Screen: " + idriver.Manage().Window.Size.ToString());
                Console.WriteLine("Context: " + DriverContext() + " time: " + DateTime.Now);
                System.Threading.Thread.Sleep(2000);
                Console.WriteLine("Context: " + DriverContext() + " time: " + DateTime.Now);
                System.Threading.Thread.Sleep(2000);
                Console.WriteLine("Ending test");
            }
            catch (Exception e)
            {
                Console.WriteLine("Error msg: " + e.Message);
                Assert.Fail();
            }
        }

        [Test]
        public void GoIntoTour()
        {
            SetUpiOS(true);
            screenshotActions.SetScreenshotRatio(touchInput);
            Assert.AreEqual(ScreenOrientation.Landscape, idriver.Orientation);
            screenshotActions.screenshotPrefix = "iOS_GoIntoVestry70";
            bool found = detector.SeekTemplate(Element.NoButton, 10, 1000, true, saveSuccessScreens);
            Assert.IsTrue(found, "iOS: NoButton from VRModeAsk screen was not found!");
            touchInput.TapAtPoint(detector.centerPoint);   //for some reason this screen requries two taps
            touchInput.TapAtPoint(detector.centerPoint);
            found = detector.SeekTemplate(Element.SkipButton, 10, 1000, true, saveSuccessScreens);
            Assert.IsTrue(found, "iOS: Skip login button was not found!");
            touchInput.TapAtPoint(detector.centerPoint);
            found = confirmer.ConfirmHUD();
            Assert.IsTrue(found, "HUD was not found!");
            confirmer.SkipTutorial();
            found = detector.SeekTemplate(Element.GyroLockButton, 3, 1000, true, saveSuccessScreens);
            Assert.IsTrue(found, "GyroLock button not found!");
            touchInput.TapAtPoint(detector.centerPoint);
            gyroButtonPoint = detector.centerPoint;
            bool downloading = confirmer.WaitForDownload(gyroButtonPoint);
            Assert.IsFalse(downloading, "Downloading still in progress (timeout)");
            System.Threading.Thread.Sleep(500);
            found = detector.SeekThumbnail(ThumbnailTemplate.iOS_Vestry70, 3, 1000, saveSuccessScreens);
            Assert.IsTrue(found, "Thumbnail of 70Vestry tour was not found");
            touchInput.TapAtPoint(detector.centerPoint);
            System.Threading.Thread.Sleep(1500);
            found = confirmer.ConfirmVestry70();
            Assert.IsTrue(found, "70Vestry tour's landing pano not confirmed!");
            Console.WriteLine("Completed");
            Assert.Pass();
        }

        [Test]
        public void MagicCode()
        {
            SetUpiOS(true);
            screenshotActions.SetScreenshotRatio(touchInput);
            Assert.AreEqual(ScreenOrientation.Landscape, idriver.Orientation);
            screenshotActions.screenshotPrefix = "MagicCode";
            bool found = detector.SeekTemplate(Element.NoButton, 10, 1000, true, saveSuccessScreens);
            Assert.IsTrue(found, "NoButton from VRModeAsk screen was not found!");
            touchInput.TapAtPoint(detector.centerPoint);   //for some reason this screen requries two taps
            touchInput.TapAtPoint(detector.centerPoint);
            found = detector.SeekTemplate(Element.SignInMagicCode, 10, 1000, true, saveSuccessScreens);
            Assert.IsTrue(found, "MagicCode on login screen was not found!");
            touchInput.TapAtPoint(detector.centerPoint);
            found = detector.SeekTemplate(Element.KeyboardPlay, 3, 200, true, saveSuccessScreens);
            Point keyboardPlayButton = detector.centerPoint;
            Assert.IsTrue(found, "Keyboard play key was not found!");
            found = confirmer.FindAndTapCode(firstMagicCode);//"ljo0s7"
            Assert.IsTrue(found, "Passing magic code failed!");
            touchInput.TapAtPoint(keyboardPlayButton);
            System.Threading.Thread.Sleep(500);
            bool keyboardPresent = confirmer.CheckKeyboard(firstCode);
            if (keyboardPresent)
            {
                if (confirmer.greenCode)
                {
                    Console.WriteLine("Keyboard present, first magic code is correct. Waitnig for tour to load.");
                }
                else
                {
                    Assert.Fail("First magic code validation failed");
                }
            }
            else
            {
                Console.WriteLine("Keyboard not present. Assuming that first magic code has been correct.");
            }
            confirmer.greenCode = false;
            bool confirm = confirmer.ConfirmEnterFirstMagicCode();
            Assert.IsTrue(confirm, "Could not confirm Test tour!");
            System.Threading.Thread.Sleep(200);
            Point tapPoint = new Point(screenshotActions.screenshot.Width / 2, screenshotActions.screenshot.Height / 2);
            touchInput.DoubleTapAtPoint(tapPoint);
            System.Threading.Thread.Sleep(1000);
            confirm = confirmer.ConfirmHUDWithText(3, 500);
            Assert.IsTrue(confirm, "HUD was not found (text)");
            found = detector.SeekTemplate(Element.MagicCodeButton, 5, 200, true, saveSuccessScreens);
            Assert.IsTrue(found, "AddMagicCode button was not found");
            touchInput.TapAtPoint(detector.centerPoint);
            found = detector.SeekTemplate(Element.KeyboardPlay, 3, 200, true, true);
            Assert.IsTrue(found, "Keyborad play button was not found");
            keyboardPlayButton = detector.centerPoint;
            found = confirmer.FindAndTapCode(secondMagicCode); //toevg6
            Assert.IsTrue(found, "Passing magic code failed!");
            touchInput.TapAtPoint(keyboardPlayButton);
            System.Threading.Thread.Sleep(500);
            keyboardPresent = confirmer.CheckKeyboard(secondCode);
            if (keyboardPresent)
            {
                if (confirmer.greenCode)
                {
                    Console.WriteLine("Keyboard present, second magic code is correct. Waitnig for tour to load.");
                }
                else
                {
                    Assert.Fail("Second magic code validation failed");
                }
            }
            else
            {
                Console.WriteLine("Keyboard not present. Assuming that second magic code has been correct.");
            }

            confirm = confirmer.ConfirmEnterSecondMagicCode();
            Assert.IsTrue(confirm, "Could not confirm Test tour 2!");
            System.Threading.Thread.Sleep(1000);
            Console.WriteLine("Complete");
        }

        [Test]
        public void SharedExperience() 
        {
            SetUpiOS(true);
            screenshotActions.SetScreenshotRatio(touchInput);
            screenshotActions.screenshotPrefix = "iOS_SharedExperience";
            Assert.AreEqual(ScreenOrientation.Landscape, idriver.Orientation);
            bool found = detector.SeekTemplate(Element.NoButton, 10, 1000, true, saveSuccessScreens);
            Assert.IsTrue(found, "NoButton from VRModeAsk screen was not found!");
            touchInput.TapAtPoint(detector.centerPoint);   //this screen requries two taps, first is to get focus on app
            touchInput.TapAtPoint(detector.centerPoint);
            found = detector.SeekTemplate(Element.SignInMagicCode, 10, 1000, true, saveSuccessScreens);
            Assert.IsTrue(found, "MagicCode on login screen was not found!");
            touchInput.TapAtPoint(detector.centerPoint);
            found = detector.SeekTemplate(Element.KeyboardPlay, 3, 200, true, saveSuccessScreens);
            Point keyboardPlayButton = detector.centerPoint;
            Assert.IsTrue(found, "Keyboard play key was not found!");
            found = confirmer.FindAndTapCode(firstMagicCode);
            Assert.IsTrue(found, "Passing magic code failed!");
            touchInput.TapAtPoint(keyboardPlayButton);
            System.Threading.Thread.Sleep(500);
            bool keyboardPresent = confirmer.CheckKeyboard(firstCode);
            if (keyboardPresent)
            {
                if (confirmer.greenCode)
                {
                    Console.WriteLine("Keyboard present, first magic code is correct. Waitnig for tour to load.");
                }
                else
                {
                    System.Threading.Thread.Sleep(1000);
                    confirmer.CheckKeyboard(firstCode);
                    if (keyboardPresent && !confirmer.greenCode)
                    {
                        Assert.Fail("First magic code validation failed");
                    }
                }
            }
            else
            {
                Console.WriteLine("Keyboard not present. Assuming that first magic code has been correct.");
            }
            confirmer.greenCode = false;
            bool confirm = confirmer.ConfirmEnterFirstMagicCode();
            Assert.IsTrue(confirm, "Could not confirm Test Tour!");
            //Show HUD
            Point screenCenter = new Point(screenshotActions.screenshot.Width / 2, screenshotActions.screenshot.Height / 2);
            System.Threading.Thread.Sleep(200);
            Console.WriteLine("Show HUD");
            Point tapPoint = screenCenter;
            touchInput.DoubleTapAtPoint(tapPoint);
            System.Threading.Thread.Sleep(1000);
            confirm = confirmer.ConfirmHUDWithText(3, 500);
            Assert.IsTrue(confirm, "HUD was not found (text)");
            found = detector.SeekTemplate(Element.GyroLockButton, 5, 200, true, saveSuccessScreens);
            Assert.IsTrue(found, "GyroLock button not found");
            touchInput.TapAtPoint(detector.centerPoint);
            //Validate Takeover button - log if host or not
            screenshotActions.LoadNewScreenshot();
            System.Threading.Thread.Sleep(1000);
            //Hide HUD
            Console.WriteLine("Hide HUD");
            tapPoint = new Point(40, 40);
            touchInput.DoubleTapAtPoint(tapPoint);
            System.Threading.Thread.Sleep(1000);
            confirm = confirmer.ConfirmHUDWithText(3, 500);
            Assert.IsFalse(confirm, "HUD still presenet");
            //double tap to leave mark -cut screen 200x200 pix at center, double tap at center, cut screen 200x200 pix at center, compare cuts
            Mat beforeTap = screenshotActions.TakeCutScreenshot(screenCenter, 200, 200);
            touchInput.SlowDoubleTapAtPoint(screenCenter);
            Mat afterTap = screenshotActions.TakeCutScreenshot(screenCenter, 200, 200);
            Mat gray = afterTap.CvtColor(ColorConversionCodes.BGR2GRAY);
            CircleSegment[] circleSegments = Cv2.HoughCircles(gray, HoughMethods.Gradient, 10, 10, 300, 100, 5, 50);
            if (circleSegments.Length > 0)
            {
                for (int i = 0; i < circleSegments.Length; i++)
                {
                    float radius = circleSegments[i].Radius;
                    if (radius != 0)
                    {
                        Point center = new Point(circleSegments[i].Center.X, circleSegments[i].Center.Y);
                        Console.WriteLine("Found circle after tap: center [" + center + "] radius [" + radius);
                        Cv2.Circle(afterTap, center, (int)radius, Scalar.Black);
                        screenshotActions.SaveResultScreenshot(afterTap, "mark", true);
                    }
                }
            }
            else
            {
                screenshotActions.SaveResultScreenshot(afterTap, "mark", false);
                Assert.Fail("Double tap did not leave mark");
            }
            Console.WriteLine("Complete");
        }

        [Test]
        public void PirateModeCube()
        {
            EnterMagicCodePart(false, "iOS_PirateModeCube", firstMagicCode);
            bool confirm = confirmer.ConfirmEnterFirstMagicCode();
            Assert.IsTrue(confirm, "Could not confirm Test Tour!");
            Point tapPoint = new Point(screenshotActions.screenshot.Width / 2, screenshotActions.screenshot.Height / 2);
            touchInput.DoubleTapAtPoint(tapPoint);
            System.Threading.Thread.Sleep(1000);
            confirm = confirmer.ConfirmHUDWithText(3, 500);
            Assert.IsTrue(confirm, "HUD was not found (text)");
            bool found = detector.SeekTemplate(Element.GyroLockButton, 5, 200, true, saveSuccessScreens);
            Assert.IsTrue(found, "GyroLock button not found");
            touchInput.TapAtPoint(detector.centerPoint);
            confirm = confirmer.ConfirmFirstTestTourHUD();
            Assert.IsTrue(confirm, "Bad HUD screen");
        }

        [Test]
        public void PirateModeSpheric()
        {
            EnterMagicCodePart(false, "iOS_PirateModeSpheric", thirdMagicCode);
            bool confirm = confirmer.ConfirmEnterThirdMagicCode();
            Assert.IsTrue(confirm, "Could not confirm Test Tour!");
            Point tapPoint = new Point(screenshotActions.screenshot.Width / 2, screenshotActions.screenshot.Height / 2);
            touchInput.DoubleTapAtPoint(tapPoint);
            System.Threading.Thread.Sleep(1000);
            confirm = confirmer.ConfirmHUDWithText(3, 500);
            Assert.IsTrue(confirm, "HUD was not found (text)");
            bool found = detector.SeekTemplate(Element.GyroLockButton, 5, 200, true, saveSuccessScreens);
            Assert.IsTrue(found, "GyroLock button not found");
            touchInput.TapAtPoint(detector.centerPoint);
            confirm = confirmer.ConfirmThirdTestTourHUD();
            Assert.IsTrue(confirm, "Bad HUD screen");
        }

        [Test]
        public void VRModeCube()
        {
            EnterMagicCodePart(true, "iOS_VRModeCube", firstMagicCode);
            bool confirmed = false;
            for(int i = 0; i < 10; i++)
            {
                confirmed = confirmer.ConfirmVRModeTestTour(false);
                if(confirmed)
                {
                    break;
                }
                Console.WriteLine("Attempt: " + i + " no confirm" );
                System.Threading.Thread.Sleep(1000);
            }
            if (!confirmed)
            {
                screenshotActions.SaveResultScreenshot(screenshotActions.screenshot, "TestTour_VRhistogram", false);
            }
            Assert.IsTrue(confirmed, "VRMode in TestTour not confirmed");
            Point tapPoint = new Point(screenshotActions.screenshot.Width / 4, screenshotActions.screenshot.Height / 2);
            touchInput.DoubleTapAtPoint(tapPoint);
            System.Threading.Thread.Sleep(1000);
            confirmed = confirmer.ConfirmVRModeTestTour(true);
            if (!confirmed)
            {
                screenshotActions.SaveResultScreenshot(screenshotActions.screenshot, "TestTour_VRhistogram_HUD", false);
            }
            Assert.IsTrue(confirmed, "VRMode in TestTour not confirmed");
        }

        [Test]
        public void VRModeSpheric()
        {
            EnterMagicCodePart(true, "iOS_VRModeSpheric", thirdMagicCode);
            bool confirmed = false;
            for (int i = 0; i < 10; i++)
            {
                confirmed = confirmer.ConfirmVRModeTestTour3(false);
                if (confirmed)
                {
                    break;
                }
                Console.WriteLine("Attempt: " + i + " no confirm");
                System.Threading.Thread.Sleep(1000);
            }
            if (!confirmed)
            {
                screenshotActions.SaveResultScreenshot(screenshotActions.screenshot, "TestTour3_VRhistogram", false);
            }
            Assert.IsTrue(confirmed, "VRMode in Test Tour 3 not confirmed");
            Point tapPoint = new Point(screenshotActions.screenshot.Width / 4, screenshotActions.screenshot.Height / 2);
            touchInput.DoubleTapAtPoint(tapPoint);
            System.Threading.Thread.Sleep(1000);
            confirmed = confirmer.ConfirmVRModeTestTour3(true);
            if (!confirmed)
            {
                screenshotActions.SaveResultScreenshot(screenshotActions.screenshot, "TestTour3_VRhistogram_HUD", false);
            }
            Assert.IsTrue(confirmed, "HUD VRMode in TestTour not confirmed");
        }

        [Test]
        public void Resize()
        {
            EnterMagicCodePart(false, "iOS_Resize", fourthMagicCode);
            bool confirmed = false;
            for (int i = 0; i < 10; i++)
            {
                confirmed = confirmer.ConfirmVRModeTestTour3(false);
                if (confirmed)
                {
                    break;
                }
                Console.WriteLine("Attempt: " + i + " no confirm");
                System.Threading.Thread.Sleep(1000);
            }
            if (!confirmed)
            {
                screenshotActions.SaveResultScreenshot(screenshotActions.screenshot, "Tour8k", false);
            }
            Assert.IsTrue(confirmed, "Tour8k not confirmed");

        }

        //------------------------------------------------------------------------------------------------------------------------------------------------------
        void CloseDriver()
        {
            try
            {
                Console.WriteLine("Attempting to close app");
                idriver.CloseApp();
                idriver.Quit();
                errorCount = 0;
            }
            catch (Exception e)
            {
                Console.WriteLine("Consecutive Error: " + errorCount + " Error message: " + e.Message);
                if (errorCount < consecutiveErrorLimit)
                {
                    CloseDriver();
                }
                else
                {
                    Assert.Fail("Could not close the driver");
                }
            }

        }

        string DriverContext()
        {
            string context = "";
            try
            {
                context = idriver.Context;
                errorCount = 0;
            }
            catch (Exception e)
            {
                Console.WriteLine("Getting Context - Consecutive Error: " + errorCount + " Error message: " + e.Message);
                if (errorCount < consecutiveErrorLimit)
                {
                    context = DriverContext();
                }
                else
                {
                    Assert.Fail("Could not fetch driver context");
                }
            }
            return context;
        }

        void EnterMagicCodePart(bool VRMode, string screenshotPrefix, Key[] magicCode)
        {
            SetUpiOS(true);
            screenshotActions.screenshotPrefix = screenshotPrefix;
            screenshotActions.SetScreenshotRatio(touchInput);
            Assert.AreEqual(ScreenOrientation.Landscape, idriver.Orientation);
            Element answer = VRMode ? Element.YesButton : Element.NoButton;
            bool found = detector.SeekTemplate(answer, 10, 1000, true, saveSuccessScreens);
            Assert.IsTrue(found, answer + " from VRModeAsk screen was not found!");
            touchInput.TapAtPoint(detector.centerPoint);   //for some reason this screen requries two taps
            touchInput.TapAtPoint(detector.centerPoint);
            found = detector.SeekTemplate(Element.SignInMagicCode, 10, 1000, true, saveSuccessScreens);
            Assert.IsTrue(found, "MagicCode on login screen was not found!");
            touchInput.TapAtPoint(detector.centerPoint);
            found = detector.SeekTemplate(Element.KeyboardPlay, 3, 200, true, saveSuccessScreens);
            Point keyboardPlayButton = detector.centerPoint;
            Assert.IsTrue(found, "Keyboard play key was not found!");
            found = confirmer.FindAndTapCode(magicCode);
            Assert.IsTrue(found, "Passing magic code failed!");
            touchInput.TapAtPoint(keyboardPlayButton);
            System.Threading.Thread.Sleep(500);
            bool keyboardPresent = confirmer.CheckKeyboard(firstCode);
            if (keyboardPresent)
            {
                if (confirmer.greenCode)
                { Console.WriteLine("Keyboard present, first magic code is correct. Waitnig for tour to load."); }
                else
                { Assert.Fail("First magic code validation failed"); }
            }
            else
            { Console.WriteLine("Keyboard not present. Assuming that first magic code has been correct."); }
            confirmer.greenCode = false;
        }

        void SetUpCaps(iPhone iphone, bool reinstallApp)
        {
            Console.WriteLine("Setting up caps...");
            options = new AppiumOptions();
            options.AddAdditionalCapability("automationName", "XCUITest");
            options.AddAdditionalCapability("platformName", "iOS");
            options.AddAdditionalCapability("xcodeOrgId", "9B259JMCA9");
            options.AddAdditionalCapability("xcodeSigningId", "iPhone Developer");
            options.AddAdditionalCapability("shouldUseSingletonTestManager", false);
            options.AddAdditionalCapability("useNewWDA", true);
            options.AddAdditionalCapability("wdaLaunchTimeout", 999999999);
            options.AddAdditionalCapability("wdaConnectionTimeout", 999999999);
            options.AddAdditionalCapability("wdaStartupRetries", 3);
            options.AddAdditionalCapability("newCommandTimeout", 80);
            switch ((int)iphone)
            {
                case 0:
                    options.AddAdditionalCapability("udid", "bdc8a593bef30818df6ef3c410c9f5f332d18909");
                    options.AddAdditionalCapability("deviceName", "dariuszthecip6id2");
                    options.AddAdditionalCapability("platformVersion", "12.2");
                    break;
                case 1:
                    options.AddAdditionalCapability("udid", "f599b2841f6a45d6b9be0c38cda427610aa7bdab");
                    options.AddAdditionalCapability("deviceName", "iPhone");
                    options.AddAdditionalCapability("platformVersion", "11.3");
                    break;
                case 2:
                    options.AddAdditionalCapability("udid", "e06ca7466009f4661082b490a8a997fc27245159");
                    options.AddAdditionalCapability("deviceName", "dariusz@thec-ip6+");
                    options.AddAdditionalCapability("platformVersion", "12.2");
                    break;
            }

            if (reinstallApp)
            {
                options.AddAdditionalCapability("app", "/Users/lukasz/Library/Developer/Xcode/DerivedData/Unity-iPhone-gftlbihiohmvlcbwrdiylqpkcnnl/Build/Products/Release-iphoneos/CBD.app");
            }
            else
            {
                options.AddAdditionalCapability("bundleId", "co.theConstruct.theViewer.CBD");
            }
        }

        iPhone ParseTargtDevice(string text)
        {
            iPhone iphone;
            if (!Enum.TryParse<iPhone>(text, out iphone))
            {
                iphone = iPhone.non_recogised;
            }
            return iphone;
        }
    }

}