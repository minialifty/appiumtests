﻿using System;
using System.IO;
using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.Service;


namespace appiumtests
{
    public class AndroidMagicLinkTest
    {
        Phone phone;
        string rootPath;
        public bool saveSuccessScreens = true;
        public int warpSteps = 10;

        int serverRetryCount = 0;
        int serverRetryLimit = 3;

        string magicLink = "https://fp23h.app.goo.gl/WUgf1VZZZ3XyT6oB8";
        string activity = "com.unity3d.player.UnityPlayerActivity";

        ///Appium variables
        AppiumLocalService server;
        AndroidDriver<AndroidElement> driver;
        AppiumOptions cap;

        ScreenshotActions screenshotActions;
        Detector detector;
        Touch touchInput;
        Confirmer confirmer;

        bool initialised = false;

        public void InitializeParams()
        {
            var target = TestContext.Parameters.Get("Device");
            phone = ParseTargtDevice(target);
            rootPath = TestContext.Parameters.Get("Rootpath");
            var warpSteps = TestContext.Parameters.Get("WarpSteps");
            Console.WriteLine("Targeted device: " + phone + " Root path: " + rootPath);
            if (!int.TryParse(warpSteps, out this.warpSteps) || warpSteps.Equals(""))
            {
                this.warpSteps = 10;
                Console.WriteLine("Warp steps set to default: " + this.warpSteps);
            }
            else
            {
                Console.WriteLine("Warp steps set to: " + warpSteps);
            }
            Assert.AreNotEqual(Phone.non_recogised, phone, "Target device was not recogised");
            Assert.IsFalse((!Directory.Exists(rootPath) || !Directory.Exists(rootPath + "AppiumTestsOutput/") || !Directory.Exists(rootPath + "AppiumTests/")), "Project path setup is incorrect");
            initialised = true;
        }

        public void SetDebugParams()
        {
            phone = Phone.SmGalaxyS7edge;
            rootPath = "D:/praktyki/";
        }

        public bool FindOrStartAppiumServer()
        {
            if (server != null && server.IsRunning)
            {
                serverRetryCount = 0;
                return true;
            }
            try
            {

                Console.WriteLine("Building new server service");
                //AppiumServiceBuilder serverBuilder = new AppiumServiceBuilder().WithIPAddress("127.0.0.1").UsingPort(4723).WithAppiumJS(new FileInfo("C:/Users/theC/node_modules/appium/build/lib/main.js"));
                AppiumServiceBuilder serverBuilder = new AppiumServiceBuilder().WithIPAddress("127.0.0.1").UsingAnyFreePort().WithAppiumJS(new FileInfo("C:/Users/theC/node_modules/appium/build/lib/main.js"));
                server = serverBuilder.Build();
                server.Start();
                Console.WriteLine("Server started on: " + server.ServiceUrl);
                if (server.IsRunning)
                {
                    serverRetryCount = 0;
                    return true;
                }
                else
                {
                    Console.WriteLine("Server not running...");
                    if (serverRetryCount >= serverRetryLimit)
                    {
                        Console.WriteLine("Failed to start server in " + serverRetryCount + " retries");
                        return false;
                    }
                    serverRetryCount++;
                    FindOrStartAppiumServer();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error staring appium server! " + e.Message);
                if (serverRetryCount >= serverRetryLimit)
                {
                    Console.WriteLine("Failed to start server in " + serverRetryCount + " retries");
                    return false;
                }
                serverRetryCount++;
                FindOrStartAppiumServer();
            }
            return false;
        }


        public void SetUp(bool chrome, bool reset)
        {
            if (!initialised) { InitializeParams(); }
            //SetDebugParams();
            //Set up capabilities
            cap = new AppiumOptions();
            cap.AddAdditionalCapability("automationName", "uiautomator2");
            cap.AddAdditionalCapability("platformName", "Android");
            cap.AddAdditionalCapability("newCommandTimeout", "60");
            cap.AddAdditionalCapability("autoLaunch", false);
            cap.AddAdditionalCapability("skipUnlock", true);

            if(phone == Phone.SmGalaxyS7edge)
            {
                cap.AddAdditionalCapability("deviceName", "Samsung Galaxy S7 edge");
                cap.AddAdditionalCapability("udid", "e4381d5e");
                cap.AddAdditionalCapability("platformVersion", "8.0");
            }
            else
            {
                if(phone == Phone.SmGalaxyS6)
                {
                    cap.AddAdditionalCapability("deviceName", "Galaxy S6");
                    cap.AddAdditionalCapability("udid", "06157df6b8718307");
                    cap.AddAdditionalCapability("platformVersion", "7.0");
                }
            }

            if(chrome)
            {
                Console.WriteLine("Starting Chrome session.");
                cap.AddAdditionalCapability("browserName", "Chrome");
            }
            else
            {
                Console.WriteLine("Starting theViewer session.");
                cap.AddAdditionalCapability("appPackage", "co.theConstruct.theViewer.CBD");
                cap.AddAdditionalCapability("appActivity", "co.theconstructutils.viewerplugin.ViewerActivity");
            }
            if(reset)
            {
                Console.WriteLine("Capability noReset: false. Will reset app on start.");
                cap.AddAdditionalCapability("noReset", "false");
            }
            else
            {
                Console.WriteLine("Capability noReset: true. Won't reset app on start.");
                cap.AddAdditionalCapability("noReset", "true");
            }
            FindOrStartAppiumServer();
            try
            {
                driver = new AndroidDriver<AndroidElement>(server, cap);
                if(driver==null)
                {
                    Console.WriteLine("Driver returned null!");
                    Assert.Fail("Driver is null on start!");
                }
                string sessionID = driver.SessionId.ToString();
                Console.WriteLine("Session id: " + sessionID);
            }
            catch(Exception e)
            {
                Console.WriteLine("Error staring driver: " + e.Message);
            }

            //Set up test helpers
            screenshotActions = new ScreenshotActions(driver, rootPath);
            detector = new Detector(screenshotActions);
            touchInput = new Touch(driver);
            confirmer = new Confirmer(detector, screenshotActions, touchInput, true, rootPath, saveSuccessScreens);
        }

        [TearDown]
        public void Close()
        {
            Console.WriteLine("Closing app");
            driver.CloseApp();
            driver.PressKeyCode(AndroidKeyCode.Keycode_HOME);
            driver.PressKeyCode(AndroidKeyCode.Keycode_APP_SWITCH);
            System.Threading.Thread.Sleep(1000);
            try
            {
                AndroidElement closeApps = driver.FindElementByAccessibilityId("Close all"); //accessid Close all class android.widget.Button text CLOSE ALL
                closeApps.Click();
            }
            catch(Exception e)
            {
                Console.WriteLine("Closing all apps failed!");
                driver.PressKeyCode(AndroidKeyCode.Keycode_APP_SWITCH);
            }
            driver.Quit();
        }


        [Test]
        public void MagicLink()
        {
            SetUp(false, false);
            screenshotActions.screenshotPrefix = "MagicLink";
            UnlockPhone();
            //Start session with TheViewer app - Reset theViewer app
            driver.ResetApp();
            screenshotActions.SetScreenshotRatio(touchInput);
            GrantAllows();
            Console.WriteLine("Reseted theViewer app.");
            Close();
            System.Threading.Thread.Sleep(2000);
            //Start Chrome session - Go to magic link
            SetUp(true, true);
            Console.WriteLine("Is browser: " + driver.IsBrowser);
            Console.WriteLine("Context: " + driver.Context);
            driver.Url = magicLink;
            bool found = SeekActivity(activity, 20, 500);
            Assert.IsTrue(found, "Not expected activity");
            Console.WriteLine("Found theViewer activity.");
            Close();
            System.Threading.Thread.Sleep(2000);
            //Start session with theViewer app - check id magic link is stacked
            SetUp(false, false);
            driver.LaunchApp();
            System.Threading.Thread.Sleep(1000);
            screenshotActions.SetScreenshotRatio(touchInput);
            found = detector.SeekTemplate(Element.NoButton, 10, 1000, true, saveSuccessScreens);
            Assert.IsTrue(found, "NoButton from VRModeAsk screen was not found!");
            touchInput.TapAtPoint(detector.centerPoint);   
            touchInput.TapAtPoint(detector.centerPoint);
            bool confirm = confirmer.ConfirmEnterFirstMagicCode();
            Assert.IsTrue(confirm, "Could not confirm Test Tour!");
            Console.WriteLine("Context: " + driver.Context + " Activity: " + driver.CurrentActivity);
            System.Threading.Thread.Sleep(2000);
        }




        //----------------------------------NON-TEST-METHODS------------------------------------------------------------------------------

        /// <summary>
        /// Checks if phone is locked and performs unlock swipe if yes
        /// </summary>
        void UnlockPhone()
        {
            if (driver.IsLocked())
            {
                driver.Unlock();
                System.Threading.Thread.Sleep(30);
                Console.WriteLine("Device unlock context: " + driver.CurrentActivity);
                touchInput.UnlockSwipe();
                if (phone == Phone.SmGalaxyS7edge)
                {
                    System.Threading.Thread.Sleep(30);
                    UnlockCode();
                }
            }
            else
            {
                Console.WriteLine("Device already unlocked - context: " + driver.CurrentActivity);
            }
        }



        /// <summary>
        /// Seeks Allow button and presses five times
        /// </summary>
        /// <returns></returns>
        bool GrantAllows()
        {
            Console.WriteLine("Granting allows...");
            bool found = detector.SeekTemplate(Element.AllowButton, 10, 1000, true,saveSuccessScreens ,true, 0.05f);
            Console.WriteLine("Allows found: " + found);
            if (found)
            {
                Console.WriteLine("Found allow button");
                for (int i = 0; i < 5; i++)
                {
                    touchInput.TapAtPoint(detector.centerPoint);
                }
            }
            return found;
        }

        /// <summary>
        /// Checks if drivers current activity equals activity passed in parameter
        /// </summary>
        /// <param name="activity"></param>
        /// <param name="retries"></param>
        /// <param name="wait"></param>
        /// <returns></returns>
        bool SeekActivity(string activity, int retries, int wait)
        {
            for(int i = 0; i < retries; i++)
            {
                if(driver.CurrentActivity.Equals(activity))
                {
                    return true;
                }
                System.Threading.Thread.Sleep(wait);
            }
            Console.WriteLine("Not expected activity (" + activity + ")" + driver.CurrentActivity);
            return false;
        }

        void UnlockCode()
        {
            for (int i = 1; i < 7; i++)
            {
                AndroidElement codeKey = driver.FindElementByAccessibilityId(i.ToString());
                codeKey.Click();
                System.Threading.Thread.Sleep(30);
            }
            AndroidElement ok = driver.FindElementByAccessibilityId("OK");
            ok.Click();
        }

        Phone ParseTargtDevice(string text)
        {
            Phone phone;
            if (!Enum.TryParse<Phone>(text, out phone))
            {
                phone = Phone.non_recogised;
            }
            return phone;
        }
    }
}
