﻿using System;
using NUnit.Framework;
using OpenCvSharp;
using Tesseract;
using OpenCvSharp.Extensions;

namespace appiumtests
{
    /*
    public class UnitTest3
    {

        string templatesPath = "D:/praktyki/AppiumTestsOutput/templates/";
        string stubsPath = "D:/praktyki/AppiumTestsOutput/StubTests/stubs/";
        string resultsPath = "D:/praktyki/AppiumTestsOutput/StubTests/results/";

        Size screenSize;

        TesseractEngine engine;
        BitmapToPixConverter converterBitPix;

       [SetUp]
        public void SetUp()
        {
            engine = new TesseractEngine("D:/praktyki/AppiumTests/appiumtests/appiumtests/tessdata", "eng");
            converterBitPix = new BitmapToPixConverter();
        }

        //--------------TESTS-----------------------------
        [Test]
        public void FindDownloadLabelOnStub()
        {
            bool downloading = false;
            Mat im_screenshot = LoadStub(Stub.CaveHUDCrooked);
            Mat filter_screenshot = FilterNonWhite(im_screenshot);
            System.Drawing.Bitmap bitmap_screenshot = BitmapConverter.ToBitmap(filter_screenshot);
            Tesseract.Rect[] slices = SliceFour();
            string foundText = SearchScreenshotForText(bitmap_screenshot);
            Console.WriteLine("Found text: " + foundText);
            if(ConfirmHUDWithText(foundText))
            {
                Console.WriteLine("HUD confirmed");
                downloading = FindDownloadingLabel(foundText);
            }
            else
            {
                Console.WriteLine("HUD not found");
            }
            Assert.IsTrue(downloading);
        }


        [Test]
        public void ReadAllText()
        {
            Mat im_screenshot = LoadStub(Stub.CaveHUDFiltered);
            Mat filter_screenshot = FilterNonWhite(im_screenshot);
            filter_screenshot = filter_screenshot.CvtColor(ColorConversionCodes.BGR2GRAY);
            System.Drawing.Bitmap bitmap_screenshot = BitmapConverter.ToBitmap(filter_screenshot);
            string foundText = SearchScreenshotForText(bitmap_screenshot, new Tesseract.Rect(0,0,im_screenshot.Size().Width, im_screenshot.Size().Height/2));
            Console.WriteLine("I. Found text: " + foundText);
            foundText = SearchScreenshotForText(bitmap_screenshot, new Tesseract.Rect(0, im_screenshot.Size().Height / 2, im_screenshot.Size().Width, im_screenshot.Size().Height / 2));
            Console.WriteLine("II. Found text: " + foundText);
            Cv2.ImShow("o",filter_screenshot);
            Cv2.WaitKey();
        }

        [Test]
        public void RedKeyboard()
        {
            Mat im_screenshot = LoadStub(Stub.MagicKeyboardGreen);
            Mat hsv_image = im_screenshot.CvtColor(ColorConversionCodes.BGR2HSV);
            Mat filter_red_screenshot = FilterRed(hsv_image);
            im_screenshot = LoadStub(Stub.MagicKeyboardGreen);
            hsv_image = im_screenshot.CvtColor(ColorConversionCodes.BGR2HSV);
            Mat filter_green_screenshot = FilterGreen(hsv_image);
        }

        [Test]
        public void ReadAll()
        {
            Mat screenshot = LoadStub(Stub.NotReco);
            screenshot = screenshot.CvtColor(ColorConversionCodes.BGRA2GRAY);
            Cv2.BitwiseNot(screenshot, screenshot);
            System.Drawing.Bitmap bitmap_screenshot = BitmapConverter.ToBitmap(screenshot[new OpenCvSharp.Rect(0, 0, screenshot.Size().Width, screenshot.Size().Height / 2)]);
            Cv2.ImShow("roi", screenshot[new OpenCvSharp.Rect(0, 0, screenshot.Size().Width, screenshot.Size().Height / 2)]);
            Cv2.WaitKey();
            string foundText = SearchScreenshotForText(bitmap_screenshot);
            Console.WriteLine("I. Found text: " + foundText);
        }

        [Test]
        public void RedVestryKeyboardTest()
        {
            Mat screenshot = LoadStub(Stub.RedVestryKeyboard);
            FilterRed(screenshot);
        }

        [Test]
        public void CutTest()
        {
            Mat screenshot = LoadStub(Stub.RedVestryKeyboard);
            CutScreen(new Point(screenshot.Width / 2, screenshot.Height / 2), 200, 200, screenshot);
        }

        [Test]
        public void HistTest()
        {
            Mat screenshot = LoadStub(Stub.RedTour);
            Mat temp = LoadStub(Stub.Red);
            HistogramCompare(screenshot, temp);
        }

        [Test]
        public void CircleDetection()
        {
            Mat screenshot = LoadStub(Stub.MarkVisible);
            CircleDetect(screenshot);
        }

        //------------NON-TEST_METHODS--------------------

        Mat LoadStub(Stub stub)
        {
            Mat im_stub = Cv2.ImRead(stubsPath + "stub_" + (int)stub + ".png");
            screenSize = im_stub.Size();
            Console.WriteLine("Loaded stub: " + stub);
            return im_stub;
        }

        string SearchScreenshotForText(System.Drawing.Bitmap screenshot, Tesseract.Rect roi)
        {
            Pix img = converterBitPix.Convert(screenshot);
            Page page = engine.Process(img, roi);
            string text = page.GetText();
            page.Dispose();
            return text;
        }

        string SearchScreenshotForText(System.Drawing.Bitmap screenshot)
        {
            Pix img = converterBitPix.Convert(screenshot);
            Page page = engine.Process(img, PageSegMode.SingleBlock);
            string text = page.GetText();
            return text;
        }

        Tesseract.Rect[] SliceFour()
        {
            int half_width = screenSize.Width / 2;
            int half_height = screenSize.Height / 2;
            Tesseract.Rect[] slices = new Tesseract.Rect[4];
            slices[0] = new Tesseract.Rect(0, 0, half_width, half_height); //top left corner
            slices[1] = new Tesseract.Rect(half_width, 0, half_width, half_height);
            slices[2] = new Tesseract.Rect(0, half_height, half_width, half_height);
            slices[3] = new Tesseract.Rect(half_width, half_height, half_width, half_height);
            return slices;
        }

        Mat FilterNonWhite(Mat image)
        {
            Mat result = image.Threshold(200, 255, ThresholdTypes.Tozero);
            return result;
        }

        Mat FilterRed(Mat image)
        {
            Mat hsv_image = image.CvtColor(ColorConversionCodes.BGR2HSV);
            Mat result = hsv_image.InRange(new Scalar(0,100,100), new Scalar(10,255,255));
            Cv2.ImShow("red_filter", result);
            Cv2.WaitKey();
            return result;
        }


        Mat FilterGreen(Mat image)
        {
            Mat hsv_image = image.CvtColor(ColorConversionCodes.BGR2HSV);
            Mat result = hsv_image.InRange(new Scalar(10, 100, 100), new Scalar(150, 255, 255));
            Cv2.ImShow("green_filter", result);
            Cv2.WaitKey();
            return result;
        }


        bool ConfirmHUDWithText(string foundText)
        {
            if(foundText.Contains("theVIEWER") || foundText.Contains("VIEWER"))
            {
                if(foundText.Contains("VR Tours") || foundText.Contains("VRTours"))
                {
                    if (foundText.Contains("Actions") || foundText.Contains("Actlons"))
                        return true;
                }
            }
            return false;
        }


        bool FindDownloadingLabel(string foundText)
        {
            if (foundText.Contains("Downloading") || foundText.Contains("Downioading"))
            {
                return true;
            }
            return false;
        }

        Mat CutScreen(Point centerPoint, int width, int height, Mat screenshot)
        {
            Mat cutout = screenshot[centerPoint.Y - height / 2, centerPoint.Y + height / 2, centerPoint.X - width / 2, centerPoint.X + width / 2];
            Cv2.ImShow("screen", screenshot);
            Cv2.ImShow("cutout", cutout);
            Cv2.WaitKey();
            return cutout;
        }


        public Mat Histogram(Mat screenshot)
        {
            Rangef[] ranges = { new Rangef(0, 179) };
            int hbins = 30;
            int[] channels = { 0 }; //only hue channel
            int[] histSize = { hbins };
            Mat screenshot_hsv = screenshot.CvtColor(ColorConversionCodes.BGR2HSV);
            Mat[] img = { screenshot_hsv };
            Mat h_hist = new Mat();
            Cv2.CalcHist(img, channels , null, h_hist, 1, histSize, ranges);
            double minVal;
            double maxVal;
            Point minLoc;
            Point maxLoc;
            Cv2.MinMaxLoc(h_hist, out minVal, out maxVal, out minLoc, out maxLoc);
            Console.WriteLine("Size: " + h_hist.Size());
            Console.WriteLine("Histogram: min " + minVal + "[" + minLoc + "] max " + maxVal + "[" + maxLoc + "]");
            for(int i = 0; i < h_hist.Rows; i++)
            {
                Console.WriteLine("Bucket " + i + ": " + h_hist.At<float>(0,i));
            }
            return h_hist;
        }


        public bool HistogramCompare(Mat screenshot, Mat template)
        {
            Mat screenHist = Histogram(screenshot);
            Mat templateHist = Histogram(template);
            double result = Cv2.CompareHist(screenHist, templateHist, HistCompMethods.Bhattacharyya);
            Console.WriteLine("Hist Comparsion result = " + result);
            return false;
        }




        public void CircleDetect(Mat afterTap)
        {
            Mat gray = afterTap.CvtColor(ColorConversionCodes.BGR2GRAY);
            //gray = FilterNonWhite(gray);
            Mat edges = new Mat();
            Cv2.Canny(gray, edges, 300, 300);
            CircleSegment[] circleSegments = Cv2.HoughCircles(gray, HoughMethods.Gradient, 10, 10, 300, 100, 5, 50);
            if (circleSegments.Length > 0)
            {
                for (int i = 0; i < circleSegments.Length; i++)
                {
                    Point center = new Point(circleSegments[i].Center.X, circleSegments[i].Center.Y);
                    float radius = circleSegments[0].Radius;
                    Console.WriteLine("Found circle after tap: center [" + center + "] radius [" + radius +"]");
                    Cv2.Circle(afterTap, center, (int)radius, Scalar.Black);
                }
                Cv2.ImShow("e", edges);
                Cv2.ImShow("v", gray);
                Cv2.ImShow("c", afterTap);
                Cv2.WaitKey();
            }

        }
      



   




        //-----------HELPER_ENUMS-------------------------
        enum Stub
        {
            VRModeAsk = 0,
            VRModeAskDirty = 1,
            CaveHUDFull = 2,
            CaveEmpty = 3,
            SplashScreen = 4,
            LoginPickScreen = 5,
            CaveHUDCrooked = 6,
            VestryLandingPano = 7,
            CaveHUDFiltered = 8,
            MagicKeyboardEmpty = 9,
            MagicKeyboardRed = 10,
            MagicKeyboardGreen = 11,
            NotReco         = 12,
            RedVestryKeyboard = 13,
            RedTour = 14,
            Red = 15,
            CyanTour = 16,
            MarkVisible = 17
        }
    }
    */
}
